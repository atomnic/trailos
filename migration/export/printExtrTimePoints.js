var json2xls = require('json2xls');
var fs = require('fs');
var mongoose= require('mongoose');
var request= require('request');
var _=require('underscore');


var xat="";
var xkey="admin";
var password="boris2023";
var json2=[];

request({ 
    url: "http://localhost:8001/authenticate",
    method: "POST",
    json: true,
    body: {"username":xkey,"password":password}
  },
  function(error, response, body)  {
    if (error) {
        console.log(error);
        return;
    }
    xat = body.token;
    request({ 
        url: "http://192.168.1.200:8001/pub/trailos/events/WTOC2015/competitions/PreO/runs/Day1/ranking?category=PARA",
        method: "GET",
        json: true,
        headers: {
          'x-key': xkey,
          'x-access-token': xat
        }
      },  
      function(error, response, body2)  {
        //console.log(body2);
        json2=_.map(body2, function(docc){
            var newOne={};
            newOne.Name=docc.competitor.firstName;
            newOne.Surname=docc.competitor.lastName;
            newOne.Time=docc.time;
            newOne.Points=docc.points;
            return newOne;
        });

        var xls = json2xls(json2);
        fs.writeFileSync('dataWTOC2015_PreO_Day1_Times_Points_PARA.xlsx', xls, 'binary');
      }
    )
  }
)
