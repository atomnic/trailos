var json2xls = require('json2xls');
var fs = require('fs');
var mongoose= require('mongoose');
var request= require('request');
var _=require('underscore');


var xat="";
var xkey="admin";
var password="system";
var json2=[];

request({ 
    url: "http://localhost:8001/authenticate",
    method: "POST",
    json: true,
    body: {"username":xkey,"password":password}
  },
  function(error, response, body)  {
    if (error) {
        console.log(error);
        return;
    }
    xat = body.token;
    request({ 
        url: "http://localhost:8001/trailos/runs/Day1/competitors",
        method: "GET",
        json: true,
        headers: {
          'x-key': xkey,
          'x-access-token': xat
        }
      },  
      function(error, response, body2)  {
        //console.log(body2);
        json2=_.map(body2, function(docc){
            var newOne={};
            newOne.Event="Day1";
            newOne["Start number"]=docc.cmpCmrs.startNo;
            newOne.Name=docc.competitor.firstName;
            newOne.Surname=docc.competitor.lastName;
            newOne.Country=docc.competitor.country.nameInt;
            newOne["Country (short)"]=docc.competitor.country.localCode;
            var hrs=Math.floor(docc.genStartMin/60.0);
            var min=docc.genStartMin-60*hrs;
//console.log(min);
            var hrs=10+hrs;
            var minStr=min.toString();
            if (min<=9) minStr="0"+minStr;
            newOne["Start time"]= hrs+":"+minStr; //docc.genStartMin;
            min=docc.genStartMin;
            minStr=min.toString();
            if (min<=9) minStr="0"+minStr;
            newOne["Start minute"]=minStr;
            newOne.SI=docc.competitor.siChipNo;
            newOne.Class=docc.category.name;
            newOne.Team="";
            newOne.Group="A";
            newOne.Pusher="Pusher";

            return newOne;
        });

        var xls = json2xls(json2);
        fs.writeFileSync('dataWTOC2015_PreO_Day1_v2.xlsx', xls, 'binary');
      }
    )
  }
)
