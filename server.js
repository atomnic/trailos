var fs = require ('fs');

var privateKey = fs.readFileSync('cert/TrailOS.key');
var certificate = fs.readFileSync('cert/TrailOS.crt'); 
var options={key: privateKey, cert: certificate};

var express = require('express'),
	https= require('https'),
	http= require('http'),
    bodyParser = require('body-parser'),
    multer  = require('multer'),    
	auth = require('./server/security/auth.js'),
	states= require("./server/dao/states.js"),
	mongoo= require("./server/dao/mongodb.js"),
    app =  express(); // express();

https.createServer(options, app).listen(443);

console.log(__dirname);
app.use(express.static(__dirname + '/webcontent'));
app.use(bodyParser.json());
app.set('views', __dirname + '/webcontent');
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);
var zip = require('express-zip');
var FTPS= require('ftps');
var dateFormat = require('dateformat');

conf=require('./server/conf.js'),
console.log(conf.source),
source = require(conf.source),
uploadDone=false;

// TODO some conf, put it in config in future
var autoFtpOfPureHtmlReports=true;
var ftp={};
/*
ftp.server="www.tockacrta.com";
ftp.protocol="sftp";
ftp.port="22";
ftp.username="tockacrt";
ftp.password="2lv00Dt2lD";
ftp.dir="public_ftp";
*/
ftp.server="wtoc_rez@ftp.vihor.hr";
ftp.protocol="ftp";
ftp.port="21";
ftp.username="wtoc_rez";
ftp.password="4YpU6$_x@6zn";
ftp.dir=".";

function doFtp(fileName, options, callback){
	var exec = require('child_process').exec;
	var cd=new Date();
	var name=cd.getTime();
	exec("cat ftpIt.txt > ftp_"+name+".txt", function (error, stdout, stderr){
		exec("echo put "+fileName+" >> ftp_"+name+".txt", function (error, stdout, stderr){
			exec("lftp -f ftp_"+name+".txt", function (error, stdout, stderr){ 
				callback(error, null);
			});		
		});	
	});
	
	
	
	/*
	var ftps = new FTPS({
		host: options.server, // required 
	  	username: options.username, // required 
	  	password: options.password // required 
	  	// protocol: options.protocol, // optional, values : 'ftp', 'sftp', 'ftps',... default is 'ftp' 
	  	// protocol is added on beginning of host, ex : sftp://domain.com in this case 
	  	// port: options.port // optional 
	  	// port is added to the end of the host, ex: sftp://domain.com:22 in this case 
	});
	// Do some amazing things 
	ftps.addFile(fileName).exec(callback); */
}

// TODO ensure authorization for admins
app.all('/*', function(req, res, next) {
  	// CORS headers
  	res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  	// Set custom headers for CORS
  	res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
  	if (req.method == 'OPTIONS') {
    	res.status(200).end();
  	} else {
    	next();
	}
});

app.all('/trailos/*', [require('./server/security/validateRequest.js')]);   // TODO producta=:org find better solution for potential multitenancy

app.use(multer({ 
			dest: './uploads/',
			onFileUploadComplete: function(file, req, res) {
			console.log('onFileUploadComplete');
			uploadDone=true;
			// source.postFile(file, req, res);
		}	
	})
);

/***** DEPRECATED *****/
app.get('/reports/zip/:eventId/:cmpId/:runId', function(req, res) {
	var fs=require('fs');
	var service=require('./server/service/rankingService.js');
	// TODO check if runId is from cmpId and cmpId from eventId
	// TODO put this generating parts in separate module
	var category=req.query.category;
	delete req.query.category;
	service.forRun(res, req.params.runId, category, req.query, function(results){
		var template=fs.readFileSync('./webcontent/reports/rankingStageTemplate.html', 'utf8');
		// TODO save images

		var title="Preliminary: "; //{{run.code}}&nbsp;{{run.startTime | date:  'dd.MM.yyyy'}}&nbsp;{{printQuery.category}}
		var data={title: title, rows:""};
		var rows="";
		var zipFiles=[
			{ path: './webcontent/reports/resources/css/bootstrap.css', name: 'resources/css/bootstrap.css' },
			{ path: './webcontent/reports/resources/css/style.css', name: 'resources/css/style.css' }
		];
		for (var idx=0; idx<results.length; idx++) {
			var row=results[idx];
			if (!row.startNo) row.startNo="";
			rows=rows+"<tr>";
			rows=rows+"<td>";
			rows=rows+"</td>";	
			rows=rows+"<td style='padding-left:30px'>"+row.rank+".</td>";
			rows=rows+"<td style='padding-left:30px'>"+row.fullName+"</td>";
			rows=rows+"<td style='padding-left:30px'>"+row.startNo+"</td>";
			rows=rows+"<td style='padding-left:30px'>"+row.category+"</td>";
			rows=rows+"<td style='padding-left:30px'>"+row.country+"</td>";
			rows=rows+"<td><img src='./resources/flags/"+row.countryIso2.toLowerCase()+".png' width='31px' /></td>";
			rows=rows+"<td style='padding-left:30px; text-align: right;'>"+row.time+"</td>";
			rows=rows+"<td style='padding-left:30px; text-align: right;'>"+row.points+"</td>";
			rows=rows+"</tr>";
			zipFiles.push({ path: './webcontent/reports/resources/flags/'+row.countryIso2.toLowerCase()+'.png', 
					name: 'resources/flags/'+row.countryIso2.toLowerCase()+'.png' });
		}
		data.rows=rows;

		template=template.replace("<%= title %>", data.title);
		template=template.replace("<%= rows %>", data.rows);
		var cd=new Date();
		var name=cd.getTime();

		fs.writeFileSync('./webcontent/reports/rankingStage'+name+'.html', template);
		zipFiles.push({ path: './webcontent/reports/rankingStage'+name+'.html', name: 'stageName.html' });
		res.zip(zipFiles);
	});
});
/***********************************************/

app.get('/reports/:eventId/:cmpId/:runId', function(req, res) {
	var fs=require('fs');
	var service=require('./server/service/rankingService.js');
	// TODO check if runId is from cmpId and cmpId from eventId
	// TODO put this generating parts in separate module
	var category=req.query.category;
	delete req.query.category;
	var final=false;
	if (req.query._final) {
		final=true;
		delete req.query._final;
	}
	service.forRun(res, req.params.runId, category, req.query, function(results){


		// TODO save images
		var fst=results[0];
		var run=fst.run;
		var template;
		if (run.type==="PreO") {
			template=fs.readFileSync('./webcontent/reports/rankingStageTemplate.html', 'utf8');
		} else {
			template=fs.readFileSync('./webcontent/reports/rankingStageTemplateTempO.html', 'utf8');
		}
		var date=dateFormat(run.startTime, "dd.mm.yyyy HH:MM:ss");
		var reportAt=dateFormat(new Date(), "dd.mm.yyyy HH:MM:ss");
		var title="";
		var subTitle="";
		var fileNameLoc="";
		if (!final) {
			title="Preliminary results: "+run.name+ " "+ date;
			if (run.type==='PreO') title=title+" Class: "+category;
			subTitle="Printed: "+reportAt; //{{run.code}}&nbsp;{{run.startTime | date:  'dd.MM.yyyy'}}&nbsp;{{printQuery.category}}
			fileNameLoc="preliminary";
		} else {
			title="Final results: "+run.name;
			if (run.type==='PreO') title=title+" Class: "+category;;
			subTitle=date;
			fileNameLoc="final";
		}
		var data={title: title, rows:""};
		var rows="";
		
		for (var idx=0; idx<results.length; idx++) {
			var row=results[idx];
			if (row.rank) {
				rows=rows+"<tr>";
				rows=rows+"<td>";
				rows=rows+"</td>";	
				if (!row.status) {
					rows=rows+"<td style='padding-left:30px'>"+row.rank+".</td>";
				} else {
					rows=rows+"<td style='padding-left:30px'>"+row.status+"</td>";
				}
				rows=rows+"<td style='padding-left:30px'>"+row.competitor.fullName+"</td>";
				rows=rows+"<td style='padding-left:30px'>"+row.competitor.country.nameInt+"</td>";
				rows=rows+"<td><img src='./resources/flags/"+row.competitor.country.iso2.toLowerCase()+".png' width='31px' /></td>";
				rows=rows+"<td style='padding-left:30px; text-align: right;'>"+row.time+"</td>";

				if (run.type==="PreO") rows=rows+"<td style='padding-left:30px; text-align: right;'>"+row.points+"</td>";
				rows=rows+"</tr>";
			}
		}
		data.rows=rows;
		data.subTitle=subTitle;
		template=template.replace("<%= title %>", data.title);
		template=template.replace("<%= subTitle %>", data.subTitle);
		template=template.replace("<%= rows %>", data.rows);
		var name=run.code;
		if (run.type==='PreO') name=name+"_"+category;
console.log(run.type+" "+run.name);
		fs.writeFileSync('./webcontent/reports/WTOC2015_'+fileNameLoc+'_'+name+'.html', template);
		if (autoFtpOfPureHtmlReports) {
			doFtp('./webcontent/reports/WTOC2015_'+fileNameLoc+'_'+name+'.html', ftp, function(err, data) {
				if (!err) {
					res.send("OK");
				} else {
					res.sendStatus(500);
				}
			});
		} 
		
	});
});

app.get('/reports/detailed/:eventId/:cmpId/:runId', function(req, res) {
	var fs=require('fs');
	var service=require('./server/service/rankingService.js');
	// TODO check if runId is from cmpId and cmpId from eventId
	// TODO put this generating parts in separate module
	var category=req.query.category;
	delete req.query.category;
	var team;
	if (req.query._team) team=true; 
	var runService=require('./server/service/runService.js');
	service.forRunDetailed(res, req.params.runId, category, req.query, function(ret){
		var template=fs.readFileSync('./webcontent/reports/rankingStageDetailedTemplate.html', 'utf8');
		// TODO save images

		var title=ret.run.name+" "+dateFormat(ret.run.startTime, 'dd.mm.yyyy. HH:MM');
		if (team) title=title+" Team event ";
		if (req.query._hideClass || ret.run.type==='TempO' ) {
			delete req.query._hideClass;
		} else {
			title=title+" Class:"+category;
		}
		var subTitle="";
		var data={title: title, rows:""};
		var rows="";

		var results=ret.data;
		for (var idx=0; idx<results.length; idx++) {
			var row=results[idx];
			rows=rows+"<tr>";
			for (var idx2=0;idx2<row.length; idx2++ ) {
				var e=row[idx2];
				rows=rows+"<td colspan="+e.colspan;
				if (e.clazz) {
					rows=rows+" class='"+e.clazz+"'>";
				} else {
					rows=rows+">";
				}
				rows=rows+e.label+"</td>";
			}
			rows=rows+"</tr>";
		}
		data.rows=rows;

		template=template.replace("<%= title %>", data.title);
		template=template.replace("<%= subTitle %>", data.suBitle);
		template=template.replace("<%= rows %>", data.rows);
		var name=ret.run.code;

		if (team) name=name+"_team";
		if (ret.run.type==='PreO') name=name+"_"+category;

		fs.writeFileSync('./webcontent/reports/WTOC2015_detailed_'+name+'.html', template);
		if (autoFtpOfPureHtmlReports) {
			doFtp('./webcontent/reports/WTOC2015_detailed_'+name+'.html', ftp, function(err, data) {
				if (!err) {
					res.send("OK");
				} else {
					res.sendStatus(500);
				}
			});
		} else {
			res.send("OK");
		}
		
	});
});

app.get('/reports/team/:eventId', function(req, res) {
	var fs=require('fs');
	var service=require('./server/service/rankingService.js');
	service.forTeam(res, req.params.eventId, function(ret){
		var template=fs.readFileSync('./webcontent/reports/rankingTeamTemplate.html', 'utf8');
		// TODO save images

		var title="Team event ";
		if (req.query._title) title=title+req.query._title;
		if (req.query._final) title=title+"<br/>Final results"
		else title=title+"<br/>Preliminary results"

		var subTitle="";
		var data={title: title, rows:""};
		var rows="";
//console.log(ret.data);
		var results=ret.data;
		for (var idx=0; idx<results.length; idx++) {
			var row=results[idx];
			rows=rows+"<tr>";
			for (var idx2=0;idx2<row.length; idx2++ ) {
				var e=row[idx2];
				if (e.colspan) rows=rows+"<td colspan="+e.colspan;
				if (e.rowspan) rows=rows+"<td rowspan="+e.rowspan;
				if (e.clazz) {
					rows=rows+" class='"+e.clazz+"'>";
				} else {
					rows=rows+">";
				}
				rows=rows+e.label+"</td>";
			}
			rows=rows+"</tr>";
		}
		data.rows=rows;

		template=template.replace("<%= title %>", data.title);
		template=template.replace("<%= subTitle %>", data.suBitle);
		template=template.replace("<%= rows %>", data.rows);

		fs.writeFileSync('./webcontent/reports/WTOC2015_team_event.html', template);
		if (autoFtpOfPureHtmlReports) {
			doFtp('./webcontent/reports/WTOC2015_team_event.html', ftp, function(err, data) {
				if (!err) {
					res.send("OK");
				} else {
					res.sendStatus(500);
				}
			});
		} else {
			res.send("OK");
		}
		
	});
});

app.get('/reports/:eventId/:cmpId', function(req, res) {
	var fs=require('fs');
	var service=require('./server/service/rankingService.js');
	var competitionService=require('./server/service/competitionService.js');
	var runService=require('./server/service/runService.js');
	// TODO check if runId is from cmpId and cmpId from eventId
	// TODO put this generating parts in separate module
	var category=req.query.category;
	delete req.query.category;
	var cmpQuery={};
	if (mongoose.Types.ObjectId.isValid(req.params.cmpId)) {
		cmpQuery._id=req.params.cmpId;
	} else {
		cmpQuery.code=req.params.cmpId;
	}
	competitionService.findByEvent(res, req.params.eventId, cmpQuery, function(res0, docs){
		
		service.forCompetition(res, docs[0]._id, category, req.query, function(res2, results){
			if (!results) res2.send(500);
			var template=fs.readFileSync('./webcontent/reports/rankingCmpTemplate.html', 'utf8');
			// TODO save images

			var title="Preliminary: "; //{{run.code}}&nbsp;{{run.startTime | date:  'dd.MM.yyyy'}}&nbsp;{{printQuery.category}}
			var data={title: title, rows:""};
			var rows="";

			for (var idx=0; idx<results.length; idx++) {
				var row=results[idx];
				if (row.rank) {
					rows=rows+"<tr>";
					rows=rows+"<td>";
					rows=rows+"</td>";	
					rows=rows+"<td style='padding-left:30px'>"+row.rank+".</td>";
					rows=rows+"<td style='padding-left:30px'>"+row.competitor.fullName+"</td>";
					rows=rows+"<td style='padding-left:30px'>"+row.competitor.country.localCode+"</td>";
					rows=rows+"<td><img src='./resources/flags/"+row.competitor.country.iso2.toLowerCase()+".png' width='31px' /></td>";
					
					rows=rows+"<td style='padding-left:30px; text-align: right;'>"+row.runCmrs[0].points+"</td>";
					rows=rows+"<td style='padding-left:30px; text-align: right;'>"+row.runCmrs[0].time+"</td>";
					var t=0;
					var p=0;
					if (row.runCmrs[1]) {
						t=row.runCmrs[1].time;
						p=row.runCmrs[1].points;
					}
					rows=rows+"<td style='padding-left:30px; text-align: right;'>"+p+"</td>";
					rows=rows+"<td style='padding-left:30px; text-align: right;'>"+t+"</td>";


					rows=rows+"<td style='padding-left:30px; text-align: right;'>"+row.points+"</td>";
					rows=rows+"<td style='padding-left:30px; text-align: right;'>"+row.time+"</td>";
					rows=rows+"</tr>";
				}
			}
			data.rows=rows;

			template=template.replace("<%= title %>", data.title);
			template=template.replace("<%= rows %>", data.rows);
			var cd=new Date();
			var name=cd.getTime();

			fs.writeFileSync('./webcontent/reports/rankingCmp'+name+'.html', template);
			if (autoFtpOfPureHtmlReports) {
				doFtp('./webcontent/reports/rankingCmp'+name+'.html', ftp, function(err, data) {
					if (!err) {
						res.send("OK");
					} else {
						res.sendStatus(500);
					}
				});
			}
		});
	})
});

app.get('/', function (req, res) {
	  res.render('index.html', {});
	});

// custom logic first

// authentication
app.post('/authenticate', auth.login);

/*
app.post('/trailos/users', function(req, res) {
    var usersService=require('./server/service/usersService.js');
    usersService.newUser(req,res);
});
*/

app.get('/trailos/states/:type', function (req, res) {	
	var s=require('./server/dao/states.js');
	var states=s[req.params.type];
	res.json(states);
})

app.post('/trailos/subjects/:id/images', function (req, res) {	
	if (uploadDone) {	
		var subjectsService=require('./server/service/subjectsService.js');
		subjectsService.saveImage(res, req.files, req.params.id);
		uploadDone=false;
	}
	else {
		console.log("UplaodNotDone");		
	}
});

app.get('/pub/trailos/countrys/:id/flagImg', function (req, res) {	
	var countrysService=require('./server/service/countrysService.js');
	countrysService.getFlagImg(res, req.params.id);
});

app.post('/trailos/countrys/:id/flagImg', function (req, res) {	
	if (uploadDone) {	
		var countrysService=require('./server/service/countrysService.js');
		countrysService.saveImage(res, req.files, req.params.id);
		uploadDone=false;
	}
	else {
		console.log("UplaodNotDone");		
	}
});

app.get('/trailos/subjects/operations', function (req, res) {	
	var subjectsService=require('./server/service/subjectsService.js');
	subjectsService.getSubjectOperations(res, req.query);
});

app.post('/trailos/files', function (req, res) {  // in res.query is folder stored exp res.query= folder:Predlošci/Patenti
	if (uploadDone) {	
		var filesService=require('./server/service/filesService.js');
		filesService.saveFile(res, req.files, req.query.folder);
		uploadDone=false;
	}
	else {
		console.log("UplaodNotDone");		
	}
});

app.post('/trailos/folders', function (req, res) {  // in res.query is folder stored exp res.query= folder:Predlošci/Patenti
	var filesService=require('./server/service/filesService.js');
	filesService.saveFolder(res, req.query.newFolder, req.query.folder);
});

app.get('/trailos/events', function(req, res) {
	var torEventService=require('./server/service/eventService.js');
	torEventService.findByParents(res, req.query);
})
/* Competitions */
app.get('/trailos/events/:id/competitions', function(req, res) {
	var competitionService=require('./server/service/competitionService.js');
	competitionService.findByEvent(res, req.params.id, req.query);
});
app.get('/trailos/events/:id/competitions/:cmpCode', function(req, res) {
	var competitionService=require('./server/service/competitionService.js');
	if (req.params.cmpCode) {
		req.query.code=req.params.cmpCode;
	}
	competitionService.findByEvent(res, req.params.id, req.query);
});
app.post('/trailos/events/:eventId/competitions', function(req, res) {
	var competitionService=require('./server/service/competitionService.js');
	req.body.event=req.params.eventId;
	competitionService.post(req.headers['x-key'],res, req.body);
});
app.put('/trailos/events/:eventId/competitions/:competitionId', function(req, res) {
	var competitionService=require('./server/service/competitionService.js');
	req.body.event=req.params.eventId;
	competitionService.put(req.headers['x-key'], res, req.body, req.params.competitionId);
});

/* Competition competitors */
app.get('/trailos/events/:id/competitions/:cmpId/competitors', function(req, res) {
	var cmpCmrService=require('./server/service/cmpCmrService.js');
	cmpCmrService.findByCompetition(res, req.params.cmpId, req.query);
});
app.get('/trailos/events/:id/competitions/:cmpId/competitors/:cmrId', function(req, res) {
	var cmpCmrService=require('./server/service/cmpCmrService.js');
	if (req.params.cmrId) {
		req.query.competitor=req.params.cmrId;
	}
	cmpCmrService.findByCompetition(res, req.params.cmpId, req.query);
});
app.post('/trailos/events/:eventId/competitions/:cmpId/competitors', function(req, res) {
	var cmpCmrService=require('./server/service/cmpCmrService.js');
	req.body.competition=req.params.cmpId;
	cmpCmrService.post(req.headers['x-key'],res, req.body);
});
app.put('/trailos/events/:eventId/competitions/:cmpId/competitors/:cmrId', function(req, res) {
console.log("AKA");
	var cmpCmrService=require('./server/service/cmpCmrService.js');
	cmpCmrService.put(req.headers['x-key'], res, req.body, req.params.cmrId);
});

/* Runs/Stages */
app.get('/trailos/events/:id/competitions/:cmpId/runs', function(req, res) {
	var runService=require('./server/service/runService.js');
	runService.findByCompetition(res, req.params.cmpId, req.query);
});
app.get('/trailos/events/:id/competitions/:cmpId/runs/:runCode', function(req, res) {
	var runService=require('./server/service/runService.js');
	if (req.params.runCode) {
		req.query.code=req.params.runCode;
	}
	runService.findByCompetition(res, req.params.cmpId, req.query);
});
app.post('/trailos/events/:eventId/competitions/:cmpId/runs', function(req, res) {
	var runService=require('./server/service/runService.js');
	req.body.competition=req.params.cmpId;
	runService.post(req.headers['x-key'],res, req.body);
});
app.put('/trailos/events/:eventId/competitions/:cmpId/runs/:runId', function(req, res) {
	var runService=require('./server/service/runService.js');
	req.body.competition=req.params.cmpId;
	runService.put(req.headers['x-key'],res, req.body, req.params.runId);
});


/* Run tasks */
app.get('/trailos/runs/:id/tasks', function(req, res) {
	var taskService=require('./server/service/taskService.js');
	taskService.findByRun(res, req.params.id, req.query, false);
});
app.get('/trailos/admin/runs/:id/tasks', function(req, res) {
	var taskService=require('./server/service/taskService.js');
	taskService.findByRun(res, req.params.id, req.query, true);
});
app.post('/trailos/admin/runs/:id/tasks', function(req, res) {
	var taskService=require('./server/service/taskService.js');
	req.body.run=req.params.id;
	taskService.post(req.headers['x-key'],res, req.body);
});
app.put('/trailos/admin/runs/:id/tasks/:tskId', function(req, res) {
	var taskService=require('./server/service/taskService.js');
	req.body.run=req.params.id;
	taskService.put(req.headers['x-key'],res, req.body, req.params.tskId);
});

/* Run competitors */
app.get('/trailos/runs/:id/competitors', function(req, res) {
	var service=require('./server/service/runCmrService.js');
	service.findByRun(res, req.params.id, req.query);
});
app.get('/trailos/runs/:id/competitors/:cmrId', function(req, res) {
	var service=require('./server/service/runCmrService.js');
	if (req.params.cmrId) {
		req.query.competitor=req.params.cmrId;
	}
	service.findByRun(res, req.params.id, req.query);
});
app.post('/trailos/runs/:id/competitors', function(req, res) {
	var service=require('./server/service/runCmrService.js');
	req.body.run=req.params.id;
	service.post(req.headers['x-key'],res, req.body);
});
app.put('/trailos/runs/:id/competitors/:cmrId', function(req, res) {
	var service=require('./server/service/runCmrService.js');
	req.body.run=req.params.id;
	service.put(req.headers['x-key'],res, req.body, req.params.cmrId);
});
app.put('/trailos/runs/:id/competitors/:cmrId/times', function(req, res) {
	var service=require('./server/service/runCmrService.js');
	req.body.run=req.params.id;
	service.putTimesOnly(req.headers['x-key'],res, req.body, req.params.cmrId);
});
app.put('/trailos/runs/:id/competitors/:cmrId/solutions', function(req, res) {
	var service=require('./server/service/runCmrService.js');
	req.body.run=req.params.id;
	service.putSolutionsOnly(req.headers['x-key'],res, req.body, req.params.cmrId);
});
app.put('/trailos/runs/:id/competitors/:cmrId/timesSolutions', function(req, res) {
	var service=require('./server/service/runCmrService.js');
	req.body.run=req.params.id;
	service.putSolutionsAndTimesOnly(req.headers['x-key'],res, req.body, req.params.cmrId);
});

// RANKINGS, only data
app.get('/pub/trailos/events/:eventId/competitions/:cmpId/runs/:runId/ranking', function(req, res){
	var service=require('./server/service/rankingService.js');
	// TODO check if runId is from cmpId and cmpId from eventId
	var category=req.query.category;
	delete req.query.category;
	service.forRun(res, req.params.runId, category, req.query, function(docs){
		res.send(docs);
	});
});
app.get('/pub/trailos/events/:eventId/competitions/:cmpId/ranking', function(req, res){
	var service=require('./server/service/rankingService.js');
	// TODO check if runId is from cmpId and cmpId from eventId
	var category=req.query.category;
	delete req.query.category;
	service.forCompetition(res, req.params.cmpId, category, req.query, function(res2, docs){
		res2.send(docs);
	});
});

app.get('/trailos/:source', source.getList);
app.get('/trailos/:source/:id', source.getItem);


app.put('/trailos/:source/:id', source.putItem);
app.delete('/trailos/:source/:id', source.deleteItem);
app.delete('/trailos/:source/:id/:sub/:subId', source.deleteItem);
//
app.post('/:org/:source', source.postItem);

// Not in use
app.post('/trailos/:source/:id/:subs/:id1/:subs1', source.postItem);
var uploadDone=false;


// usually we have images in sub document sotred in binarys model
app.get('/trailos/:source/:id/:subsource/:fileId/binary', source.getFile);


// When no route is reached by now, response as 404
app.use(function(req, res, next) {
	var err = new Error('Not Found');
  	err.status = 404;
  	next(err);
});

function ensureAuthorized(req, res, next) {
    var bearerToken;
    var bearerHeader = req.headers["authorization"];
    if (typeof bearerHeader !== 'undefined') {
        var bearer = bearerHeader.split(" ");
        bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    } else {
        res.sendStatus(403);
    }
}

// Create an HTTP service.
// http.createServer(app).listen(80);
// Create an HTTPS service identical to the HTTP service.


// app.listen(8001);
