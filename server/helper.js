exports.wildcard=function(query) {
	for (var key in query) {
		if (query[key].indexOf("*")>=0) {
			query[key]={"$regex":query[key].replace("*",".*") ,"$options":"i"};
		}
	}	
}