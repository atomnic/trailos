
var mongoo = require('./dao/mongodb.js'); // mongoo - some kinde of mix between mongodb and mongoose


var queryToPaging=function(query){
	var pagign={};
	if (query.offset) {
		paging.offset=query.offset;
		delete query.offset;
	} else {
		paging.offset=0;
	}
	if (query.limit) {
		paging.limit=query.limit;
		delete query.limit;
	} else {
		paging.limit=500;
	}
	return paging;
}

exports.getList = function (req, res) {
	var paging={offset:req.query.offset, limit:req.query.limit};
	delete req.query.offset;
	delete req.query.limit;
	mongoo.get(res, req.params.source, req.query, paging);  
}

exports.getItem = function (req, res) {

	var paging=queryToPaging(res.query);
	res.query.run=res.params.id;
	mongoo.get(res, 'tasks', res.query, paging);
	mongoo.get(res, req.params.source, {"_id": req.params.id});	
}

exports.putItem = function (req, res) {
	var user=req.headers['x-key'];
	mongoo.update(res, req.body, req.params.source, req.params.id, user);
}

exports.postItem = function (req, res) {	
	// var source = require('./dao/'+req.params.source);
	var user=req.headers['x-key'];
	if (req.params.subs) {
		if (req.params.subs1) {
			mongoo.insertSub(res, req.body, req.params.source, req.params.subs, req.params.subs1,  req.params.id1, user);
		} else {
			mongoo.insertSub(res, req.body, req.params.source, null, req.params.subs,  req.params.id, user);
		}
	} else {
		mongoo.insert(res, req.body, req.params.source, user);
	}
}

exports.deleteItem = function (req, res) {
	if (req.params.sub) {
		mongoo.deleteSub(res, req.params.source, req.params.id, req.params.sub, req.params.subId);
	} else {
		mongoo.delete(res, req.params.source, req.params.id);
	}
}

exports.postFile = function (file, req, res) {	
	mongoo.saveFile(res, file, req.params.source, req.params.id);
}


exports.getFile=function (req, res) {
	mongoo.getFile(res, req.params.source,req.params.subsource, req.params.id, req.params.fileId);	
}