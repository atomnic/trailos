/**
 * New node file
 * schema and models ....
 */
var mongoose=require('mongoose');
Schema = mongoose.Schema;


var ChangesSubSchema= new Schema (
		{ 
			path: String,
			value: Schema.Types.Mixed,
			command:String,
			date: Date,
			user: String,
			initials: String
		}
);

var HistorySchema=new Schema(
	{
		original: Schema.Types.Mixed,
		changes:[ChangesSubSchema]
	}
);

/*
HistorySchema.statics.createHistory=function(path, ids, key, value) {
	this.
}
*/
mongoose.model('historys', HistorySchema);
