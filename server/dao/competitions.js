var mongodb=require('./mongodb.js');
var mongoose = require("mongoose");
var types = require("./types.js");
var types = require("./events.js");
var types = require("./competitors.js");
var types = require("./categorys.js");
var states= require("./states.js");

var CompetitionSchema=new Schema(
	{
		code:String, 
		name:String,
		nameInt:String,
		description: String,
		startDate: Date,
		endDate: Date,
		createdBy:String,
		dateCreated: Date,
		event:{type:Schema.Types.ObjectId, ref:'events'},
		status: { type: String, enum: states.event }
		// status:{type:Schema.Types.ObjectId, ref:'types'}
	}
);

CompetitionSchema.populates=function() {
	return "";
}

mongoose.model('competitions', CompetitionSchema);
