/**
 * New node file
 * schema and models ....
 */
var mongodb=require('mongodb');
var mongoose = require("mongoose");
var binarys=require('./binarys');


var FilesSchema=new Schema(
	{
		name: String,
		fileType: { type: Number, min: 0, max: 1 }, // 0-file 1-folder
		binaryId:{type:Schema.Types.ObjectId, ref:'binarys'},
		files: [{type:Schema.Types.ObjectId, ref:'files'}],
		folders: [{type:Schema.Types.ObjectId, ref:'files'}], // no automatic populates 
		big:Boolean
	}
);       


FilesSchema.populates=function () {
	return "folders files";
};

FilesSchema.slices=function() {
	return "folders"; // TODO resolver slices for files t
};

FilesSchema.sliceSize=function() {
	return [0,10]; 
};

mongoose.model('files', FilesSchema);
