/**
 * New node file
 * schema and models ....
 */
var mongodb=require('mongodb');
var mongoose = require("mongoose");
var countrys = require("./countrys.js");


var ClubSchema=new Schema(
		{			
			code:String,
			name:String,
			country: {type:Schema.Types.ObjectId, ref:'countrys'},
			createdBy: String,
			dateCreated: Date
		}
);

ClubSchema.populates=function() {
	return 'country';
}

mongoose.model('clubs', ClubSchema);
