/**
 * New node file
 * schema and models ....
 */
var mongodb=require('mongodb');
var mongoose = require("mongoose");
var types = require("./types.js");
var states= require("./states.js");
var categorys = require("./categorys.js");


var TcSubSchema= new Schema({
	no: String,
	solution: { type: String, enum: states.solution },
	status: { type: String, enum: states.task }
});

var TaskSchema=new Schema(
	{			
		//type:{type:Schema.Types.ObjectId, ref:'types'},  // CP- control point, ST - station
		type: { type: String, enum: states.taskTypes },
		no:String,
		orderNo: Number, // no to order by
		solution: { type: String, enum: states.solution },
		timeControls: [TcSubSchema],
		status: { type: String, enum: states.task },
		// status: {type:Schema.Types.ObjectId, ref:'types'},
		categorys: [{type:Schema.Types.ObjectId, ref:'categorys'}],
		run:{type:Schema.Types.ObjectId, ref:'runs'},
		team: Boolean,
		createdBy: String,
		dateCreated: Date
	}
);

TaskSchema.populates=function() {
	return "categorys";
}

mongoose.model('tasks', TaskSchema);
