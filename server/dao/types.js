/**
 * New node file
 * schema and models ....
 */
var mongodb=require('mongodb');
var mongoose = require("mongoose");

var TypeSchema=new Schema(
		{			
			code:String,
			name:String,
			domain:String,
			description: String,
			createdBy: String,
			dateCreated: Date
		}
);

TypeSchema.index({ name: 1, domain: 2}, { unique: true });

mongoose.model('types', TypeSchema);
