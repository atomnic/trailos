/**
 * New node file
 * schema and models ....
 */
var mongodb=require('./mongodb.js');
var mongoose = require("mongoose");

var CountrySchema=new Schema(
		{
			nameInt:String, 
			nameLocal:String, 
			iso2:String, 
			iso3:String,
			localCode:String,
			createdBy: String,
			dateCreated: Date,
			migrationKey: Schema.Types.Mixed,
			flagImg:{type:Schema.Types.ObjectId, ref:'binarys'}
		}
);

CountrySchema.slices=function() {
	return "countrys"; // TODO resolver slices for files t
};
/*
CountrySchema.populates=function () {
	return 'flagImg';
} 
*/

mongoose.model('countrys', CountrySchema);

