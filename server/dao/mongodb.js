/**
 * New node file
 * schema and models ....
 */
mongoose = require('mongoose');


var dbConfig= require('./dbconfig.js');
require('./historys.js');
// dbConfig.url url to database 

var sliceSize=25;

db = mongoose.connect(dbConfig.url);
Schema = mongoose.Schema;

function checkSlices(schema) {
	if ((typeof(schema.slices) === typeof(Function) && schema.slices()!==null && schema.slices()!=="")) {
		return schema.slices();
	} else {
		return "";
	}
}

function checkSliceSize(schema) {
	if ((typeof(schema.sliceSize) === typeof(Function) && schema.sliceSize()!==null && schema.sliceSize()!=="")) {
		return schema.sliceSize();
	} else {
		return [0,100];
	}
}

function checkPopulates(schema) {
	return (typeof(schema.populates) === typeof(Function) && schema.populates()!==null && schema.populates()!=="");
}

function depopulateOne(doc, schema) {
	if (checkPopulates(schema)) {	
		var populates=schema.populates().split(" ");
		for (var i=0; i<populates.length; i++) {
			if (doc[populates[i]] && doc[populates[i]]._id) {
				doc[populates[i]]=doc[populates[i]]._id;
			} 		
		}
		return true;
	} 
	return false;
}



exports.saveFile=function saveFile(res, file, modelName, id ) {
	require('./binarys.js');
	var binarys=mongoose.model('binarys');
	var source = require('./'+modelName);
	var model=mongoose.model(modelName);
	model.findById(id, function (err, doc){
		source.saveBinarys(res, doc, file);
	});
}

exports.getFile=function getFile(res, modelName, subName, id, fileId ) {
	require('./binarys.js');
	var binarys=mongoose.model('binarys');
	var source = require('./'+modelName);
	var model=mongoose.model(modelName);	
	model.findOne({_id:id}).populate(subName+'.binaryId').exec( function (err, doc) {
	    // if (err) return next(err);
		var file=doc[subName].id(fileId); //.exec( function(err, file) {
			if (file.binaryId && file.binaryId!=null) {
		    	res.contentType(file.binaryId.img.contentType);
	    		res.send(file.binaryId.img.data);			
	    	} else {
	    		res.send({});
	    	}
	});
}


// TODO get rid of res, need to be done via callback(err, doc)
exports.insert=function save(res, data, modelName, user, callback) {
	delete data.id;
	var source = require('./'+modelName);
	var model=mongoose.model(modelName);
	var history=mongoose.model('historys');	
	depopulateOne(data,model.schema);
	data.createdBy=user;
	data.dateCreated=new Date();
	
	model.create(data, function (err, post) {
 	    if (err) { 
 	    	if (err.code===11000 && res) {
 	    		res.status(409).send({ error: 'Zapis sa tim ključem već postoji!' });
 	    	} else {
 	    		res.status(500).send({ error: 'Neočekivana interna pogreška!' });
 	    	}
 	    }  else { 
 	    	// object saved, status: 200
 	    	/*
 	    	history.create({'original':post,'changes':[]}, function(err, historyDoc){
 	    		if (err) {
 	    			console.log("History not saved!");
 	    			console.log(err);
 	    		} else {
 	    			console.log("History saved!");
 	    			console.log(historyDoc);
 	    		}
 	    	});
			*/
 	    	if (res) res.json(post);
 	    	if (typeof callback === "function") callback(post);
 	    }
 	  });
}

/***
Returns subcollection of document, gviven by path of nested subcollections
Works only for projected subcollections
*/
function collectionByPath(doc2, path) {
	if (!path) {
		return doc2;
	}
	var paths=path.split(".");
	var collection=doc2[paths[0]][0];
	for (var idx=1; idx<paths.length; idx++) {
		collection=collection[paths[idx]][0];
	}
	return collection;
}


// applay this to smipd project
exports.insertSub=function save(res, data, modelName, path, subName, id, user) {
	
	var source = require('./'+modelName);
	var model=mongoose.model(modelName);
	var history=mongoose.model('historys');	
	depopulateOne(data,model.schema);
	var criteria={};
	var projection={};
	var positionalUpdate={};
	var positionalSet={};
	if (path) {
		criteria[path+"._id"]=id;	
		projection[path+".$"]= 1;
		positionalUpdate[path+"._id"]=id;
		//positionalUpdate._id=doc2._id;
	} else {
		criteria["_id"]=id;
		positionalUpdate._id=id;
	}
	model.findOne(criteria, projection, function(err, doc2){
		if (doc2) {
			var doc2subcollected=doc2;
			if (path) {
				doc2subcollected=collectionByPath(doc2, path);
			}
			// workaround to get subdocument fully created sutable for inserting into history
			var subDoc=doc2subcollected[subName].create(data);

			if (path) {
				positionalSet[path+".$."+subName]=subDoc;
			} else {
				positionalSet[subName]=subDoc;
			}

			model.update(positionalUpdate, {$push:positionalSet},function (err, doc3) {
		 	    if (err) { 
		 	    	if (err.code===11000) {
		 	    		res.status(409).send({ error: 'Zapis sa tim ključem već postoji!' });
		 	    	} else {
		 	    		res.status(500).send({ error: 'Neočekivana interna pogreška!' });
		 	    	}
		 	    }  else { 
		 	    	history.findOne({'original._id':doc2._id}, function(err, historyDoc) {
		 	    		if (err) {
		 	    			console.log("History not saved!");
			 	    		console.log(err);
		 	    			return;
		 	    		}
		 	    		if (historyDoc) {
		 	    			var change={};
		 	    			if (path) {
		 	    				change.path=path+subName;
		 	    			} else {
		 	    				change.path=subName;
		 	    			}
		 	    			change.date=new Date();
		 	    			change.value=subDoc;
		 	    			change.command="INSERT";
		 	    			change.user=user;
			 	    		historyDoc.changes.push(change);
				 	    	historyDoc.save( function(err){
				 	    		if (err) {
				 	    			console.log("History not saved!");
				 	    			console.log(err);
				 	    		} else {
				 	    			console.log("History saved!");
				 	    			console.log(historyDoc);
				 	    		}
				 	    	});
				 	    }
			 	    });
		 	    	res.json(data);
	 	    	}	
 	  		});
		}
	})
}

function getContentRange(paging, docs) {
	return paging.offset+"-"+(parseInt(paging.offset)+docs.length)+'/*';
}

exports.get=function get(res, modelName, query, paging) {
	if (!paging  || !paging.offset || !paging.limit) {
		paging={offset:0, limit:500};
	} 
	var source = require('./'+modelName);
	var model=mongoose.model(modelName);	
	for (var key in query) {
		if (query[key].indexOf("*")>=0) {
			query[key]={"$regex":query[key].replace("*",".*") ,"$options":"i"};
		}
	}	
	var slices=checkSlices(model.schema);
	var sliceSize=checkSliceSize(model.schema);

	if (checkPopulates(model.schema)) {
		model		
		.find(query)
		.populate(model.schema.populates())		
		.slice(slices, sliceSize)
		.skip(paging.offset).limit(paging.limit)
		.exec(function (err, docs) {
			if (err) return res.status(500).send({ error: 'Neočekivana interna pogreška!' })
			else if (query._id) {
				if (docs.length==1) {
					res.json(docs[0]);
				} else {
					res.json({});
				}
			} else {
				res.header('Content-Range', getContentRange(paging,docs));
				res.json(docs);
			}			
		});
	} else {		
		model.find(query).skip(paging.offset).limit(paging.limit).exec(function (err, docs) {
			if (err) return res.status(500).send({ error: 'Neočekivana interna pogreška!' });
			res.header('Content-Range', getContentRange(paging,docs));
			res.json(docs);
		});
	}	
}; 

exports.delete=function(res, modelName, id) {
	// TODO zapiši history, a ako ide za migraciju obriši history
	require('./'+modelName);
	var model=mongoose.model(modelName);

	model.findById(id).exec( function(err, doc) {
		console.log(doc);
		doc.remove();
	    if (!err) {

	    	res.status(200).send("OK");
	    }
	    else {
	    	res.status(500).send({ error: 'Neočekivana interna pogreška!' });
	    }
	});
};

exports.deleteSub=function(res, modelName, id, subModelName, subId) {
	require('./'+modelName);
	var model=mongoose.model(modelName);
	model.findById(id, function(err,doc) {
		doc[subModelName].id(subId).remove();
		doc.save(function (err) {
		  if (err) return handleError(err);
		  console.log('the sub-doc was removed');
		  res.json({});
		});
	});
};
	

exports.update=function update(res, data, modelName, id, user) {
	require('./'+modelName);
	var model=mongoose.model(modelName);
	var history=mongoose.model('historys');	
	depopulateOne(data, model.schema);	
	var id=data._id;  // remember id
	delete data._id;  // remove _id, force and prevent _id update
	delete data.__v;
	/* model.findByIdAndUpdate(id , { $set: data}, {new:true} ,function (err, doc2) {
		res.json(doc2);  // Unpopulated
		//exports.get(res, modelName, {_id:id});  // so that feiled ar populated
	}); 
	*/
console.log("B");
	model.findById(id, function(err, doc2){
		var merge = require('merge'), original, cloned;
		var history=mongoose.model('historys');	
		merge(doc2, data);
		var changes=[];
		var changed=new Date();
		for (var idx=0;idx<doc2.modifiedPaths().length;idx++) {
			var change={};
			change.path= doc2.modifiedPaths()[idx];
		   	change.value=doc2[doc2.modifiedPaths()[idx]];  // TODO solve deep paths
		   	change.date=changed;
		   	change.user=user;
			changes.push(change);
		}
		doc2.save(function (err, doc3) {
		    if (err) {
		    	console.error(err);
		    } else {
		    	/*
		    	history.findOne({'original._id':doc2._id}, function(err, historyDoc) {
		    		if (historyDoc) {
			    		var newChanges=historyDoc.changes.concat(historyDoc.changes, changes);
			    		historyDoc.changes=newChanges;
			    		historyDoc.save(function(err2){
			    			if (err2) {
			    				console.log("History not saved!");
			    				console.log(err);
			    			} else {
			    				console.log("History saved!");
			    				console.log(historyDoc);
			    			}
			    		});
		    		}
				})
				*/
		    }
		    res.send(doc3);
		});
	});
}
	
