/**
 * New node file
 * schema and models ....
 */
var mongodb=require('mongodb');
var mongoose = require("mongoose");
var categorys = require("./types.js");
var countrys = require("./countrys.js");
var countrys = require("./clubs.js");
var states= require("./states.js");

var MemberSubSchema=new Schema(
	{
		no:Number, 
		competitor:{type:Schema.Types.ObjectId, ref:'competitors'}
	}
)

var CompetitorSchema=new Schema(
		{			
			firstName:String,
			lastName:String,
			midName: String,
			fullName: String,
			dateOfBirth: Date, 
			country: {type:Schema.Types.ObjectId, ref:'countrys'},   
			club: {type:Schema.Types.ObjectId, ref:'clubs'},   
			sex: String, // M-male, F-female
			gender: { type: String, enum: states.gender },
			para: Boolean,
			paraType: {type:Schema.Types.ObjectId, ref:'types'},
			type: { type: String, enum: states.competitorTypes },  // I- individula, T - team
			startNo: Number,
			siChipNo: Number,
			team: {type:Schema.Types.ObjectId, ref:'competitors'},
			members2: [MemberSubSchema],
			members:[{type:Schema.Types.ObjectId, ref:'competitors'}],
			createdBy:String,
			dateCreated: Date
		}
);

CompetitorSchema.populates=function () {
	return 'country club paraType members2.competitor team';
}

mongoose.model('competitors', CompetitorSchema);



