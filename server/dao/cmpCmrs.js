var mongodb=require('./mongodb.js');
var mongoose = require("mongoose");
var types = require("./types.js");
var types = require("./competitors.js");
require("./competitions.js");
require("./categorys.js");
require("./runCmrs.js");

var CompetitionCompetitorSchema=new Schema ({
	competition: {type:Schema.Types.ObjectId, ref:'competitions'},  
	competitor: {type:Schema.Types.ObjectId, ref:'competitors'}, 
	category:{type:Schema.Types.ObjectId, ref:'categorys'},
	para: Boolean,
	paraType: {type:Schema.Types.ObjectId, ref:'types'},
	startNo: Number, // TO be generated with start times
	points: Number,
	time: Number,
	rank:  Number,
	runCmrs:[{type:Schema.Types.ObjectId, ref:'runCmrs'}],
	group: String
});

CompetitionCompetitorSchema.index({ competitor: 1, competition: 2}, { unique: true });

CompetitionCompetitorSchema.populates=function() {
	return "category paraType competitor competitor.country runCmrs competition";
}

CompetitionCompetitorSchema.pre('save', function (next) {
  	var self=this;
  	if (typeof self.group==="undefined") self.group="A";
  	next();
});

var deepPopulate = require('mongoose-deep-populate');
CompetitionCompetitorSchema.plugin(deepPopulate); //, options /* more on options below */);

mongoose.model('cmpCmrs', CompetitionCompetitorSchema);
