var mongodb=require('./mongodb.js');
var mongoose = require("mongoose");
var types = require("./types.js");
var types = require("./runs.js");
var types = require("./cmpCmrs.js");
var types = require("./tasks.js");
var types = require("./categorys.js");
var types = require("./competitors.js");
var states= require("./states.js");
var _=require("underscore");


var SolutionSubSchema=new Schema({
	task: {type:Schema.Types.ObjectId, ref:'tasks'}, 
	time: Number,
	time2: Number,
	solution:  { type: String, enum: states.solution },
	tcSolutions:[{no:String,  solution:{ type: String, enum: states.solution }}]
});

var PitStopSchema=new Schema({
	no:Number,
	startHrs: Number,
	startMin: Number,
	startSec: Number,
	finishHrs: Number,
	finishMin: Number,
	finishSec: Number
});

var RunCompetitorSchema=new Schema ({
	run: {type:Schema.Types.ObjectId, ref:'runs'},
	cmpCmrs: {type:Schema.Types.ObjectId, ref:'cmpCmrs'}, 
	competitor: {type:Schema.Types.ObjectId, ref:'competitors'},  //RD
	category:{type:Schema.Types.ObjectId, ref:'categorys'},	//RD
	genStartTime: Date,
	getStartHrs: Number,
	genStartMin: Number,
	genStartSec: Number,
	startTime: Date,
	breaks:[PitStopSchema],
	startNo: Number,
	status: {type: String, enum: states.runCompetitor },
	solutions:[SolutionSubSchema],
	points: Number,
	time: Number,
	rank:  Number,
	status: String,
	timeOn: Number,
	penalty: Number,
	teamTime: Number,
	group: {type: String, enum: states.startGroups }
});



function defaultz(x, val) {
	return (typeof x === 'undefined') ? val : x;
}

RunCompetitorSchema.pre('save', function (next) {
  	var self=this;
	mongoose.models["cmpCmrs"].findById(self.cmpCmrs, function (err, cmpCmrDoc){
		if (typeof self.group==="undefined") self.group=cmpCmrDoc.group;
		if (!cmpCmrDoc.runCmrs) cmpCmrDoc.runCmrs=[];
		var rrr=_.find(cmpCmrDoc.runCmrs, function(ccd){
			return ccd.equals(self._id);
		})
		if (!rrr) cmpCmrDoc.runCmrs.push(self._id);
		cmpCmrDoc.markModified("runCmrs");
		cmpCmrDoc.save(function(err, d) {
			next();
		})
	});
});

RunCompetitorSchema.populates=function() {
	return "run cmpCmrs competitor category solutions.task competitor.country";
};

var deepPopulate = require('mongoose-deep-populate');
RunCompetitorSchema.plugin(deepPopulate);

mongoose.model('runCmrs', RunCompetitorSchema);
