var mongodb=require('mongodb');
var mongoose = require("mongoose");
var binarys=require('./binarys');
 
var UserSchema   = new Schema({
    username: { type: String, unique: true, required: true },
    password: { type: String, required: true},
    initials: String,
    firstName: { type: String, required: true},
    lastName: { type: String, required: true},
    description: String,
    email: String,
    token: String,
    roles:[String]   // Roles are predefined, for now only: admin, user 
});
 
UserSchema.post('update', function(doc) {
});


mongoose.model('users', UserSchema);