/**
 * New node file
 * schema and models ....
 */
var mongodb=require('./mongodb.js');
var mongoose = require("mongoose");
var types = require("./types.js");
var types = require("./competitors.js");
var types = require("./categorys.js");
var states= require("./states.js");

var EventSchema=new Schema(
	{
		code:String, // P- rijavitelj, I -izumitelj, ...
		name:String,
		nameInt: String,
		startDate: Date,
		endDate: Date,
		createdBy:String,
		dateCreated: Date,
		status: { type: String, enum: states.event }
		// status:{type:Schema.Types.ObjectId, ref:'types'}
	}
);

EventSchema.statics.findByParents = function (parents, query, callback) {
	var torTypeModel=mongoose.model('types');
  	// Prepare the query so it can be returned
  	// before asynchronous findByUsername is complete
  	var query2 = this.find(query);

  	// Find user by username
  	if (parents && parents.status) {
	  	torTypeModel.findOne({code:parents.status}, function (error, type) {
		    var scope = this;
		    var args = arguments;
		    if (error || !type) {
		      // Maintain asynchronous behavior
		      return process.nextTick(function () {
		        // Maintain scope of callback
		        callback.apply(scope, args);
		      });
		    }
		    // Update query with user id and execute
		    query2.where('status').equals(type._id).exec(callback);
		});
  	} else {
  		query2.exec(callback);
  	}

  // Return query for optional modification at runtime
  return query2;
};

EventSchema.populates=function() {
	return "";
}

mongoose.model('events', EventSchema);



