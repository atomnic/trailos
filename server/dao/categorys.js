/**
 * New node file
 * schema and models ....
 */
var mongodb=require('mongodb');
var mongoose = require("mongoose");


var CategorySchema=new Schema(
		{			
			code:String,
			name:String,
			description: String,
			createdBy: String,
			dateCreated: Date
		}
);

mongoose.model('categorys', CategorySchema);
