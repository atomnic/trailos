var mongodb=require('./mongodb.js');
var mongoose = require("mongoose");
var types = require("./categorys.js");
var types = require("./competitions.js");
var types = require("./tasks.js");
var states= require("./states.js");

var TimeLimitSubSchema=new Schema(
	{
		limit: Number,
		limitType: {type:Schema.Types.ObjectId, ref:'categorys'} // J-junior, P-para  O-open
	}
);

/*
var SolutionSubSchema=new Schema({
	task: {type:Schema.Types.ObjectId, ref:'runs.tasks'}, 
	solution: String
});


var RunCompetitorSubSchema=new Schema ({
	competitor: {type:Schema.Types.ObjectId, ref:'competitions.competitors'}, 
	startTime: Date,
	startHrs: Number,
	startMin: Number,
	starSec: Number,
	finsihTime: Date,
	finishHrs: Number,
	finishMin: Number,
	finishSec: Number,
	// status: {type:Schema.Types.ObjectId, ref:'types'},
	status: { type: String, enum: states.runCompetitor },
	solutions:[SolutionSubSchema]
});*/

var RunSchema=new Schema(
	{
		code:String, 
		name:String,
		nameInt:String,
		description: String,
		type:{ type: String, enum: states.runTypes },
		startTime: Date,
		createdBy:String,
		dateCreated: Date,
		competition:{type:Schema.Types.ObjectId, ref:'competitions'},
		// status:{type:Schema.Types.ObjectId, ref:'types'},
		status: { type: String, enum: states.event },
		runType:{type:Schema.Types.ObjectId, ref:'types'},
		timeLimits:[TimeLimitSubSchema],
		timeLimit: Number,
		timeLimitsPara: Number,
		breaks: Number
		// competitors: [RunCompetitorSubSchema],
		// tasks:[{type:Schema.Types.ObjectId, ref:'tasks'}]
	}
);


RunSchema.populates=function() {
	return "timeLimits.limitType competition competition.event";
}

var deepPopulate = require('mongoose-deep-populate');
RunSchema.plugin(deepPopulate); //, options /* more on options below */);


mongoose.model('runs', RunSchema);
