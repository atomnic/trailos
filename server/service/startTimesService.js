/**
 * New node file
 * schema and models ....
 */
var mongoose = require('mongoose');

var source = require('../dao/cmpCmrs');
var competitionsDao = require('../dao/competitions');
var runCmrsDao = require('../dao/runCmrs');
var runDao = require('../dao/runs');
var mongodb = require('../dao/mongodb');
var model=mongoose.model('cmpCmrs');
var runCmrs= mongoose.model('runCmrs');
var runs= mongoose.model('runs');
var competitions=mongoose.model('competitions');
var fs = require('fs');
var helper=require('../helper.js');
var _=require('underscore');

var dt=120;
var sd=0;

var rule=8*60;
var maxGlobal=100;

function compare(a, b, direction) {
	if (a === b) {
		return 0;	
	}	
	return a > b ? -1*direction : 1*direction;	
}

function isRuleOk(idx, docs){
	var back=idx-1;
	while (back>=0 && docs[idx].genStartSec<docs[back].genStartSec+rule && docs[idx].competitor.country.localCode!==docs[back].competitor.country.localCode) {
		back--;
	}
	if (back<0) return true;
	if (docs[idx].genStartSec>=docs[back].genStartSec+rule) return true;
	if (docs[idx].competitor.country.localCode===docs[back].competitor.country.localCode) return false;
}

function isRuleOk2(docc, docs){
	var back=docs.length-1;
	while (back>=0 && docc.genStartSec<docs[back].genStartSec+rule && docc.competitor.country.localCode!==docs[back].competitor.country.localCode) {
		back--;
	}
	if (back<0) return true;
	if (docc.genStartSec>=docs[back].genStartSec+rule) return true;
	if (docc.competitor.country.localCode===docs[back].competitor.country.localCode) return false;
}

function findByRule(idx, docs) {
	var forward=idx+1;
	while (forward<docs.length && docs[idx].genStartSec<docs[forward].genStartSec+rule && docs[idx].competitor.country.localCode!==docs[forward].competitor.country.localCode) {
		forward++;
	}
	if (forward>docs.length) return forward-1; // TODO see last TODO comment;
	if (!(docs[idx].genStartSec<docs[forward].genStartSec+rule && docs[idx].competitor.country.localCode!==docs[forward].competitor.country.localCode)) {
		return forward;
	} else {
		// TODO extend time for forward first,  list is to small
		return forward;
	}
}

function replaceIt(pos1, pos2, list) {
	var temp=list[pos1];
	var startSec1=list[pos1].genStartSec;
	var startSec2=list[pos2].genStartSec;
	list[pos1]=list[pos2];
	list[pos1].idx=pos1;
	list[pos2]=temp;
	list[pos2].idx=pos2;

	list[pos1].genStartSec=startSec1;
	list[pos2].genStartSec=startSec2;
}

function saveIt(idx, docs, callback, startNoAlg, startAt){
	if (idx<docs.length) {
		var docc=docs[idx];
		docc.genStartMin=Math.floor(docc.genStartSec/60.0);
		docc.genStartSec=docc.genStartSec-docc.genStartMin*60;
		docc.markModified("genStartSec");
		docc.markModified("genStartMin");
		docc.save(function(err, d){
			if (typeof startNoAlg !=='undefined' && startNoAlg==="byStartTimes") {
				var startNo=startAt+idx;

				model.findByIdAndUpdate( docc.cmpCmrs, { startNo:startNo}, {new:true}, function (err, d2) {
					idx++;
					saveIt(idx, docs, callback, startNoAlg, startAt);
				});

			} else {
				idx++;
				saveIt(idx, docs, callback, startNoAlg, startAt);			
			}
		});
	} else {
		callback(docs);
	}
}

function saveIt2(idx, docs, callback, startNoAlg, startAt){
	if (idx<docs.length) {
		var docc=docs[idx];
			if (typeof startNoAlg !=='undefined' && startNoAlg==="byStartTimes") {
				var startNo=startAt+idx;

				model.findByIdAndUpdate( docc.cmpCmrs, { startNo:startNo}, {new:true}, function (err, d2) {
					idx++;
					saveIt2(idx, docs, callback, startNoAlg, startAt);
				});

			} else {
				idx++;
				saveIt2(idx, docs, callback, startNoAlg, startAt);			
			}
	} else {
		callback(docs);
	}
}

function generate(run, group, delay, delta, ruleAll, startNoAlg, startAt, callback){
	dt=parseInt(delta);
	sd=parseInt(delay);
	var preQuery;
	var where={};
	var where2={};
	where.run=run;
	if (group) where.group=group;
	if (ruleAll) {
		where2.group=ruleAll
		where2.run=run;
	}
	if (typeof ruleAll!=='undefined') {
		preQuery=runCmrs.find(where2).deepPopulate("competitor competitor.country");//.exec(function(erro,docs){
	} else {
		preQuery=runCmrs.find({run:0});
	}
	preQuery.exec(function(err, docsTo) {

		runCmrs.find(where).deepPopulate("competitor competitor.country").exec(function(erro,docs){
			// 1. randomize
			var docs=_.map(docs, function(docc){
				docc.rnd=_.random(1,1000);
				return docc;
			});

			// 2. sort by random
			docs.sort(function(r1, r2) {
				var p1 = r1.rnd;
			  	var p2 = r2.rnd;		  	
			   	return compare(p1, p2,-1);		  
			});

			// 3. generate times
			for (var idx=0; idx<docs.length; idx++) {
				docs[idx].idx=idx+1;
				docs[idx].genStartSec=sd+idx*dt;
			}

			// 4. apply rule
			var global=0;
			var lastPair={p1:0,p2:0};
			for (var idx=1; idx<docs.length; idx++) {
				var start=idx;
				var replacement=false;

				while  (((ruleAll && !isRuleOk2(docs[idx], docsTo) && idx+1<docs.length)) ) {
					if (lastPair.p1!==start && lastPair.p2!==(idx+1)) {
						replaceIt(idx, idx+1, docs);
						replacement=true;
						idx++;
						lastPair.p1=start;
						lastPair.p2=idx;
					} else {
						idx++;
					}
				}

				while ((!isRuleOk(idx, docs) && idx+1<docs.length)){
					if (lastPair.p1!==start && lastPair.p2!==(idx+1)) {
						replaceIt(idx, idx+1, docs);
						replacement=true;
						idx++;
						lastPair.p1=start;
						lastPair.p2=idx;
					} else {
						idx++;
					}
				}


				if (!isRuleOk(idx, docs) || (ruleAll && !isRuleOk2(docs[idx], docsTo) )) {
					replaceIt(idx, 0, docs);
					idx=0;
					global++;

				} else if (replacement) {  // replacement for first rule
					idx=start-1;

				} else if (replacement) {

				} else {
					idx=start;
				}
				if (global==maxGlobal) {
					console.log("MAX LOOPS!");
				}
			}


			saveIt(0, docs, callback, startNoAlg, parseInt(startAt));
		})
	})

}


exports.generateStartNoOnly=function(run, group, alg, startAt, callback) {
	var where={};
	where.run=run;
	if (group) where.group=group;
	runCmrs.find(where).deepPopulate("competitor competitor.country").sort({"genStartMin":1}).exec(function(erro,docs){
		saveIt2(0, docs, callback, alg, parseInt(startAt));
	});
}

exports.generateByRank=function(run, rankRun, group, delay, delta, ruleAll, startNoAlg, startAt, callback){
	dt=parseInt(delta);
	sd=parseInt(delay);
	var whereRank={};
	var where={};
	whereRank.run=rankRun;
	where.run=run;
	if (group) whereRank.group=group;
	whereRank.rank={$gte:1};
	if (group) where.group=group;
	runCmrs.find(whereRank).sort({rank:-1}).exec(function(err, docsRank){
console.log(docsRank);
		var idx=0;
		runCmrs.find(where).exec(function(err, docs){
			var docs2=_.map(docsRank, function(doccRank) {
				var docc2=_.find(docs, function(docc){
					return docc.competitor.equals(doccRank.competitor);
				})
				if (doccRank.rank>0) {
					docc2.genStartSec=sd+idx*dt;
					idx++;
				} else {
					docc2.genStartSec=-1;// *dt;
				}
				return docc2;
			})
console.log(docs2);
			saveIt(0,docs2,callback);
		})	
	})
}

exports.generate=generate;