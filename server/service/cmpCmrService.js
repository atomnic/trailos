/**
 * New node file
 * schema and models ....
 */
var mongoose = require('mongoose');

var source = require('../dao/cmpCmrs');
var competitionsDao = require('../dao/competitions');
var competitorsDao = require('../dao/competitors');
var runCmrsDao = require('../dao/runCmrs');
var runDao = require('../dao/runs');
var mongodb = require('../dao/mongodb');
var model=mongoose.model('cmpCmrs');
var runCmrs= mongoose.model('runCmrs');
var runs= mongoose.model('runs');
var competitions=mongoose.model('competitions');
var competitors=mongoose.model('competitors');
var fs = require('fs');
var helper=require('../helper.js');
var _=require('underscore');

var sliceSize=10;

Schema = mongoose.Schema;


model.schema.post('remove', function (doc) {
	runCmrs.remove({cmpCmrs:doc._id}).exec();
})

model.schema.post('save', function (docc) {
	for (var idx=0; idx<docc.runCmrs.length; idx++) {
		var runCmr=docc.runCmrs[idx];
		//mongodb.update(null, runCmr, 'runCmrs', "");
//console.log(docc);
		runCmrs.findByIdAndUpdate(runCmr, {$set:{ category:docc.category, group:docc.group}},{new:true}, function(err, docc2){
//console.log(docc2);
		});
	}
})


function findByCompetitionCallback(res, err, docs) {
	if (err) {
		console.log(err);
		res.status(500).send(err);
	} else {
		res.json(docs);
	}
};

exports.findByCompetition=function(res, competition, query) {
	helper.wildcard(query);
	var compQuery;
	if (mongoose.Types.ObjectId.isValid(competition))  {
		compQuery=competitions.findById(competition);
	} else {
		compQuery=competitions.findOne({"code":competition});
	}

	var countryQuery=false;
	if (query.country) {
		// IT must be ID
		countryQuery=competitors.find({"country" : query.country});
	}

		compQuery.exec(function(err, doc){
			if (doc) {
				query.competition=doc._id;
				if (countryQuery) {
					countryQuery.exec(function(err, docs0) {
						var ids = docs0.map(function(doc) { return doc._id; });
						query.competitor={$in:ids};
						delete query.country;
						model.find(query).deepPopulate(model.schema.populates()).exec(function(err, docs){
							findByCompetitionCallback(res, err,docs);
						});
					});
				} else {
					model.find(query).deepPopulate(model.schema.populates()).exec(function(err, docs){
						findByCompetitionCallback(res, err,docs);
					});
				}
			} else  {
				res.status(500).send(err);
			}
		});
	
}

function afterDelete(doc){
	// not in use done via post - delete
}

function afterInsert(doc){
	runs.find({competition:doc.competition}).exec( function(err, docs) {
		for (var idx=0; idx<docs.length; idx++) {
			var run=docs[idx];
			var runCmr={};
			runCmr.run=run._id;
			runCmr.group=doc.group;
			runCmr.cmpCmrs=doc._id;
			runCmr.competitor=doc.competitor;
			runCmr.category=doc.category;
			runCmr.solutions=[];
			//runCmrs.create(runCmr);
			mongodb.insert(null, runCmr, 'runCmrs', "");
		}
	})
}

function afterPost(doc){
	runs.find({competition:doc.competition}).exec( function(err, docs) {
		for (var idx=0; idx<docs.length; idx++) {
			var run=docs[idx];
			var runCmr={};
			runCmr.run=run._id;
			runCmr.group=doc.group;
			runCmr.cmpCmrs=doc._id;
			runCmr.competitor=doc.competitor;
			runCmr.category=doc.category;
			runCmr.solutions=[];
			//runCmrs.create(runCmr);
			mongodb.insert(null, runCmr, 'runCmrs', "");
		}
	})
}


exports.post=function(user, res, data) {
	if (mongoose.Types.ObjectId.isValid(data.competition)) {
		mongodb.insert(res, data, 'cmpCmrs',user, afterInsert);
	} else {
		competitions.findOne({code:data.competition}, function(err, doc){
			if (doc) {
				data.competition=doc._id;
				mongodb.insert(res, data, 'cmpCmrs', user, afterInsert);
			} else  {
				res.status(500).send(err);
			}
		});
	}
}

exports.put=function(user, res, data, id) {
	if (mongoose.Types.ObjectId.isValid(data.competition._id)) {
		mongodb.update(res, data, 'cmpCmrs', id, user);
	} else {
		competitions.findOne({code:data.competition}, function(err, doc){
			if (doc) {
				data.competition=doc._id;
				mongodb.update(res, data, 'cmpCmrs',id,  user);
			} else  {
				res.status(500).send(err);
			}
		});
	}
}

