/**
 * New node file
 * schema and models ....
 */
var mongoose = require('mongoose');

var source = require('../dao/runCmrs');
var runsDao = require('../dao/runs');
require('../dao/runs');
require('../dao/categorys');
require('../dao/competitions');
require('../dao/cmpCmrs');
require('../dao/tasks');
require('../dao/competitors');
var categorys=mongoose.model('categorys');
var tasks=mongoose.model('tasks');
var countrys=mongoose.model('countrys');
var mongodb = require('../dao/mongodb');
var model=mongoose.model('runCmrs');
var runs=mongoose.model('runs');
var competitors=mongoose.model('competitors');
var competitions=mongoose.model("competitions");
var cmpCmrs=mongoose.model("cmpCmrs");

var fs = require('fs');
var helper=require('../helper.js');
var _=require('underscore');

Schema = mongoose.Schema;

// Ranking model: rank, competitor, country (flag), club, category, points, time
var penaltyTimeTcPreO=60; // TODO check for penaltiy time for TC
var penaltyTimeTcTempO=30;
var timeLimitPenalty=300.0; // In sec

// DE HARDOCED IT
var timeLimitPublic=140;
var paraInOpenTimeLimitBonus=30;


function saveIt(idx, docs, callback){
	if (idx<docs.length) {
		var docc=docs[idx];
		docc.markModified("points");
		docc.markModified("time");
		docc.markModified("penalty");
		docc.markModified("timeOn");
		docc.markModified("teamTime");
		docc.markModified("rank");
		docc.save(function(err,d){
			idx++;
			saveIt(idx, docs, callback);
		});
	} else {
		callback(docs);
	}
}

function compare(a, b, direction) {
	if (a === b) {
		return 0;	
	}	
	return a > b ? -1*direction : 1*direction;	
}

function findByCompetitionCallback(res, err, docs, category, query, callback) {

}

function findByRunCallback(res, err, docs, category, query, callback, team) {
	if (err) {
		console.log(err);
		res.status(500).send(err);
	} else {
		// var ranks=[];
		for (var idx=0;idx<docs.length; idx++){
			var runCmr=docs[idx];
			delete runCmr.rank;
			var rank={points:0, time:9999};
			if ((runCmr.run.type==="PreO" //&& runCmr.breaks.length==runCmr.run.breaks+1 && runCmr.breaks[runCmr.run.breaks].finishMin!=0
				&& (!category || (category && category===runCmr.category.code)))
				||
				(runCmr.run.type==="TempO" //&& runCmr.breaks[0].finishMin!=0
				&& (!category || (category && category===runCmr.category.code)))
				) {
				//var points=0;
				var time=0;
				var penaltyTc=penaltyTimeTcTempO;		// TODO no need to get penelty for every competitor
				if (runCmr.run.type==="PreO") {
					penaltyTc=penaltyTimeTcPreO;
				}
				//
				// 1. get points for CP
				rank=_.reduce(runCmr.solutions,function(overall,solution){
					if (solution.task.type==='CP' 
						&& solution.solution===solution.task.solution 
							&& solution.task.status==='regular') {
						overall.points=overall.points+1;
					} else if ((solution.task.type==='ST' && solution.task.status==='regular')) {
						var time=0;
						if (solution.time || solution.time>0) time=solution.time;
						if (solution.time2 || solution.time2>0) time=time+solution.time2;
						if (solution.time && solution.time2) time=Math.round(((time/2.0)+ 0.00001) * 100) / 100;
						if (!solution.task.team) overall.time=overall.time+time;
						else overall.teamTime=time;

						var tcpoints=_.reduce(solution.tcSolutions,function(oa, tc){
							if (tc.solution===_.findWhere(solution.task.timeControls,{no:tc.no, status:'regular'} ).solution
								|| (typeof tc.solution==="undefined" || tc.solution==="")) {
								oa=oa+1;
							};
							return oa;
						},0);
						var regularTcs=_.reduce(solution.task.timeControls,function(f, o) {
							if (o.status==='regular') {
								f=f+1;
							}
							return f;
						},0);
						if (tcpoints<regularTcs ) {
							if (!solution.task.team) {
								overall.time=overall.time+(parseInt(regularTcs)-parseInt(tcpoints))*parseInt(penaltyTc);
							} else {
								overall.teamTime=overall.teamTime+(parseInt(regularTcs)-parseInt(tcpoints))*parseInt(penaltyTc);
							}
						}  
					}

					return overall;
					// TODO check penalty on time limits
				},{points:0, time:0, penalty:0, timeOn:0, teamTime:0});

				if (runCmr.run.type==="PreO") {
					// calculate breaks
					var timeOnTrail=_.reduce(runCmr.breaks, function(timeOn, break0){
						var hrs=break0.finishHrs-break0.startHrs;
						var min=break0.finishMin-break0.startMin;
						var sec=break0.finishSec-break0.startSec;
						var total=hrs*60*60+min*60+sec;
						timeOn=timeOn+total;
						return timeOn;
					}, 0);	
					rank.timeOn=timeOnTrail;
					var timeLimit=_.find(runCmr.run.timeLimits, function(tl) {return tl.limitType.equals(runCmr.category._id);});
					var timeLimitLimit;
					if (typeof timeLimit!=="undefined") timeLimitLimit=timeLimit.limit;
					if (runCmr.category.code==="PUBLIC") timeLimitLimit=timeLimitPublic;
					if (runCmr.competitor.para && runCmr.category.code==="OPEN") timeLimitLimit=timeLimitLimit+paraInOpenTimeLimitBonus;
// TODO DEHARDCODE IT (PUBLIC AND OPEN)
console.log(runCmr.category.code+ " TIME LIMIT: "+timeLimitLimit*60);
console.log(runCmr.competitor.fullName+" TIME ON TRAIL: "+timeOnTrail);
					if (timeOnTrail>timeLimitLimit*60) { // limit is given in minutes
						var p=(timeOnTrail-timeLimitLimit*60)/timeLimitPenalty;
						var penaltyPoints=Math.ceil(p);
						// Integer penalty=(int)penaltyD;
console.log(runCmr.competitor.fullName+" PENALTY: "+penaltyPoints);
console.log("-----------------------------");
						rank.points=rank.points-penaltyPoints;
						rank.penalty=penaltyPoints;
					}
				}
				
				runCmr.points=rank.points;
				runCmr.time=rank.time;
				runCmr.timeOn=rank.timeOn;
				runCmr.penalty=rank.penalty;
				runCmr.teamTime=rank.teamTime;
				runCmr.rank=1; // TO be calculated
				if (runCmr.time===0) {
					//runCmr.time=9999;
					runCmr.rank=0;
				} 

				var r=_.reduce(runCmr.solutions, function(s,l){
				if (l.task.type==="CP" && typeof l.solution!=="undefined") {
						s=s+1;
					}
					// TODO count TCs if (row.type==="ST" && typeof l.solution!=="undefined") {
					//	s=s+1;
					//}
					return s;
				}, 0);
				
 				//if (r<26) {
 				//	runCmr.rank=0;
 				//}
 				//if (runCmr.competitor.fullName==="Ahmed Saad") runCmr.rank=0;
 				if (typeof runCmr.status!=="undefined" && runCmr.status!=="") {
 					runCmr.rank=1;
 					//runCmr.rank=runCmr.status;
 					//runCmr.time=10000;
 					//console.log(runCmr.status);
 					//console.log(runCmr.rank);
 				}
			} else {
			 	runCmr.rank=0;
			}
		}		

		docs.sort(function(r1, r2) {
			if (typeof r1.status!=="undefined" && r1.status!=="")  return 1;
			if (typeof r2.status!=="undefined" && r2.status!=="")  return -1;
			var p1 = r1.points;
		  	var p2 = r2.points;
		  	var t1 = r1.time;
		  	var t2 = r2.time;
		  	if (typeof p1!=='undefined' && typeof p2!=="undefined" && p1 !== p2) {
		    	return compare(p1, p2,1);
		  	} else if (t2 && t1) {
		  		return compare(r1.time, r2.time,-1);
		  	} else return -1;
		});

		var nor=1;
		docs=_.map(docs, function(docc){
			if (((category && docc.category.code===category) || !category) && (docc.rank>0 || docc.rank==="DSQ")) {
				if (docc.rank>0) {
					docc.rank=nor;
					nor++;
				}
			}
			return docc;
		})

		saveIt(0,docs, function (docs){
			docs=_.filter(docs, function(docc){
					return docc.rank>0;
			});
			if (category) {
				docs=_.filter(docs, function(docc){
					return docc.category.code===category;
				});
			}
			if (query) {
				docs=_.filter(docs, function (docc){
					var r=true;
					if (query.competitor) {
						r=r&& (docc.competitor._id.equals(query.competitor));
					}
					if (query.country) {
						r=r&& (docc.competitor.country._id.equals(query.country));	
					}
					return r;
				});	
			}
			callback(docs);
		});

	}
};



exports.forRun=function(res, run, category, query, callback) {
	//helper.wildcard(query);
	
	var runQuery={};
	var categoryQuery;
	if (!category) {
		res.sendStatus(204);
		return;
	}
	var team=false;

	if (query._team) {
		team=query._team;
		delete query._team;
	} 
	if (mongoose.Types.ObjectId.isValid(category)){
		categoryQuery=categorys.findById(category);
	} else {
		categoryQuery=categorys.findOne({code:category});
	}
	if (mongoose.Types.ObjectId.isValid(run)) {
		runQuery=runs.findById(run);
	} else {
		runQuery=runs.findOne({code:run});
	}
	categoryQuery.exec(function(err, cat) {
		if (err) {
			if (res) res.status(500).send(err);
			return;
		} // TODO respond with error like wrong category
		
		runQuery.exec(function(err, run){
			if (run) {
				model.find({run:run._id, category:cat._id}).deepPopulate(model.schema.populates()).exec(function(err, docs){
					// Do some populates
					// docs=_.map(docs, function(runCmr){
					//	runCmr.run=run;// run is full populated
					//	return runCmr;
					//})
					if (cat) cat=cat.code;
					if (team) {
						docs=_.filter(docs, function(d){
							if (d.competitor.team) {
								return true;
							}
							return false;
						})
					}
					findByRunCallback(res, err, docs, cat, query, callback, team);
				})
			} else  {
				if (res) res.status(500).send(err);
			}
		});
	});
}

exports.forCompetition=function(res, competition, category, query, callback){
	if (!category) {
		res.sendStatus(204);
		return;
	}
	var cmpQuery={};
	var runQuery;
	var categoryQuery;
	if (mongoose.Types.ObjectId.isValid(category)){
		categoryQuery=categorys.findById(category);
	} else {
		categoryQuery=categorys.findOne({code:category});
	}
	if (mongoose.Types.ObjectId.isValid(competition) || typeof competition ==="object") {
		cmpQuery=competitions.findById(competition);
	} else {
		cmpQuery=competitions.findOne({code:competition});
	}
	categoryQuery.exec(function(err, cat){
		if (err) {
			if (res) res.status(500).send(err);
			return;
		} // TODO respond with error like wrong category
		cmpQuery.exec(function(err, cmp){
			if (cmp) {
				cmpCmrs.find({competition:cmp._id, category:cat._id}).deepPopulate(cmpCmrs.schema.populates()).exec(function(err, docs){
					if (cat) cat=cat.code;

					docs=_.map(docs, function(dcc){

						var points=_.reduce(dcc.runCmrs, function(all,rrr){
							if (typeof rrr.points!=="undefined") {
								all=all+rrr.points;
							}
							return all;
						},0);
						var time=_.reduce(dcc.runCmrs, function(all,rrr){
							if (typeof rrr.time!=="undefined") {
								all=all+rrr.time;
							}
							return all;
						},0);

						if (time>0) {
							dcc.rank=1; // to be calculated
							dcc.time=time;
					  	    dcc.points=points;
					  	} else {
							dcc.rank=0;
							dcc.time=0;
					  	    dcc.points=0;
						}
						return dcc;					
					})

					docs.sort(function(r1, r2) {
						var p1 = r1.points;
					  	var p2 = r2.points;
					  	var t1 = r1.time;
					  	var t2 = r2.time;
					  	if (typeof p1!=='undefined' && typeof p2!=="undefined" && p1 !== p2) {
					    	return compare(p1, p2,1);
					  	} else if (t2 && t1) {
					  		return compare(r1.time, r2.time,-1);
					  	} else return -1;
					});

					var nor=1;
					docs=_.map(docs, function(docc){
						if (((category && docc.category.code===category) || !category) && (docc.rank>0)) {
							docc.rank=nor;
							nor++;

						}
						return docc;
					})

					saveIt(0,docs, function (docs){
						docs=_.filter(docs, function(docc){
								return docc.rank>0;
						});
						if (category) {
							docs=_.filter(docs, function(docc){
								return docc.category.code===category;
							});
						}
						if (query) {
							docs=_.filter(docs, function (docc){
								var r=true;
								if (query.competitor) {
									r=r&& (docc.competitor._id.equals(query.competitor));
								}
								if (query.country) {
									r=r&& (docc.competitor.country._id.equals(query.country));	
								}
								return r;
							});	
						}
						callback(res, docs);
					});
				})
			} else  {
				if (res) res.status(500).send(err);
			}
		});
	})
}

exports.forTeam=function(res, event, callback) {
	// 1. uzmi timove
	// za svaki team zbroji points i times+ teamTime
	// TODO for given main event, find(competition.event:"something")
	model.find({teamTime: {$gt:0}}).deepPopulate("competitor competitor.country").exec(function(err, data){
	
		var docs=[];
		docs=_.reduce(data, function(o, d) {
			var team;
			var c=_.findWhere(o,{iso2:d.competitor.country.iso2});
			if (!c) {
				team={};
				team.country=d.competitor.country.nameInt;
				team.iso2=d.competitor.country.iso2
				team.points=0;
				team.time=0;
				team.competitors=[];
				o.push(team);
			} else {
				team=c;
			};
			team.points=team.points+d.points;
			//team.time=team.time+d.time;
			team.time=team.time+d.teamTime;
			team.competitors.push({fullName:d.competitor.fullName, time:d.teamTime, points:d.points});
			//
			return o;
		}, docs);
		//
		docs.sort(function(r1, r2) {
			if (typeof r1.status!=="undefined" && r1.status!=="")  return 1;
			
			var p1 = r1.points;
		  	var p2 = r2.points;
		  	var t1 = r1.time;
		  	var t2 = r2.time;
		  	if (typeof p1!=='undefined' && typeof p2!=="undefined" && p1 !== p2) {
		    	return compare(p1, p2,1);
		  	} else if (t2 && t1) {
		  		return compare(r1.time, r2.time,-1);
		  	} else return -1;
		});
		for (var idx=0; idx<docs.length; idx++) {
			docs[idx].competitors.sort(function(r1, r2) {
				if (typeof r1.status!=="undefined" && r1.status!=="")  return 1;
				
				var p1 = r1.points;
			  	var p2 = r2.points;
			  	var t1 = r1.time;
			  	var t2 = r2.time;
			  	if (typeof p1!=='undefined' && typeof p2!=="undefined" && p1 !== p2) {
			    	return compare(p1, p2,1);
			  	} else if (t2 && t1) {
			  		return compare(r1.time, r2.time,-1);
			  	} else return -1;
			});
		}

		var list=[];
		var header1=[];
		var header2=[];
		header1.push({colspan:4, label:""});
		header1.push({colspan:2, label:"Result"});
		header1.push({colspan:2, label:"Team result"});
		list.push(header1);

		header2.push({colspan:1, label:""});
		header2.push({colspan:1, label:"Name"});
		header2.push({colspan:2, label:"Team"});
		header2.push({colspan:1, label:"Points"});
		header2.push({colspan:1, label:"Sec"});
		header2.push({colspan:1, label:"Points"});
		header2.push({colspan:1, label:"Sec"});
		list.push(header2);

		var pos=1;
		for (var idx=0; idx<docs.length; idx++) {
			for (var cid=0; cid<docs[idx].competitors.length; cid++) {
				var newLine=[];
				if (cid===0) {
					var l={};
					l.rowspan=docs[idx].competitors.length;
					l.label=pos;
					l.colspan=1;
					pos++;
					newLine.push(l);
				};
				newLine.push({label:docs[idx].competitors[cid].fullName, colspan:1});
				if (cid===0) {
					var l0={};
					l0.rowspan=docs[idx].competitors.length;
					l0.colspan=1;
					l0.label="<img  style='vertical-align:middle;margin-right:2px' src='./resources/flags/"+docs[idx].iso2.toLowerCase()+".png' width='32px'/>"
					newLine.push(l0);
					var l={};
					l.rowspan=docs[idx].competitors.length;
					l.colspan=1;
					l.label=docs[idx].country;
					newLine.push(l);
				};
				newLine.push({label:docs[idx].competitors[cid].points, colspan:1});
				newLine.push({label:docs[idx].competitors[cid].time, colspan:1});
				if (cid===0) {
					var l0={};
					l0.rowspan=docs[idx].competitors.length;
					l0.colspan=1;
					l0.label=docs[idx].points;
					newLine.push(l0);
					var l={};
					l.rowspan=docs[idx].competitors.length;
					l.colspan=1;
					l.label=docs[idx].time;
					newLine.push(l);
				};
				list.push(newLine);
			}
		}
		callback({data:list});
	})
	
}

var characters="ABCDEFXZ";

exports.forRunDetailed=function(res, run, category, query, callback) {
	if (!category) {
		res.sendStatus(204);
		return;
	}
	var team=false;
	if (query._team) team=query._team;
	var header1=[];
	var header2=[];
	var results=[{label:"", colspan:1},{label:"", colspan:1},{label:"", colspan:1}];
	var data=[];
	var footer=[];
	var footer2=[];
	var run;
	var hideTeam=query._hideTeam;
	delete query._hideTeam;
	exports.forRun(res, run, category, query, function(docs){
		if (docs && docs.length>0) {
			run=docs[0].run;

			tasks.find({run:docs[0].run._id}).sort({orderNo: 1}).exec(function(err, tsks){
				header1.push({no:0, colspan:1, label:""});
				header1.push({no:1, colspan:1, label:"Class:"+docs[0].category.name});
				header1.push({no:2, colspan:1, label:"Control", clazz:"flag"});
				//
				header2.push({no:0, colspan:1, label:""});
				header2.push({no:1, colspan:1, label:"Name"});
				header2.push({no:2, colspan:1, label:"Country/Correct&nbsp;answer", clazz:"flag"});

				// header 1 
				header1=_.reduce(tsks, function(r, tsk){
					if (tsk.status==="regular" && tsk.categorys.indexOf(docs[0].category._id)>-1) {
						if (tsk.type==="CP") {
							var h={label:tsk.no, colspan:1};
							r.push(h);
						} 
					}
					return r;
				},header1);
				//
				header1=_.reduce(tsks, function(r, tsk){
					if (tsk.status==="regular" && tsk.categorys.indexOf(docs[0].category._id)>-1 
						   && ( team  || (!team && !tsk.team) )) {
						if (tsk.type==="ST") {
							var h={colspan: tsk.timeControls.length+2, label:"Station "+tsk.no};//+" 1-"+tsk.timeControls.length, colspan: tsk.timeControls.length+2};
							r.push(h);
						} 
					}
					return r;
				},header1);
				//
				var resColSpan=1;
				if (run.type==="PreO") resColSpan=4;
				header1.push({colspan:resColSpan, label:"Result"});
				
				// header line 2 and clear results array for simplier statistics calculation
				header2=_.reduce(tsks, function(r, tsk){
					if (tsk.status==="regular" && tsk.categorys.indexOf(docs[0].category._id)>-1) {
						if (tsk.type==="CP") {
							var h={label:tsk.solution, colspan:1};
							r.push(h);
							results.push({label:tsk.solution, colspan:1});
						}
					}
					return r;
				},header2);
				//
				header2=_.reduce(tsks, function(r, tsk){
					if (tsk.status==="regular" && tsk.categorys.indexOf(docs[0].category._id)>-1 
						   && ( team  || (!team && !tsk.team) )) {
						if (tsk.type==="ST")  {
							r=_.reduce(tsk.timeControls, function(a, l){
								a.push({label:l.solution, colspan:1});
								results.push({label:l.solution, colspan:1});
								return a;
							}, r);
							r.push({label:"Sec", colspan:2})
							results.push({label:"-", colspan:1});
							results.push({label:"-", colspan:1});
						}
					}
					return r;
				},header2);
				//
				if (run.type==="PreO") {
					header2.push({colspan:1, label:"Time", clazz:"verySmall"});
					header2.push({colspan:1, label:"Pen.", clazz:"verySmall"});
					header2.push({colspan:1, label:"Points"});
				}
				header2.push({colspan:1, label:"Sec"});
				//
				data.push(header1);
				data.push(header2);

				// data linesc
				data=_.reduce(docs,function(d,dcc){
					if (dcc.rank>0 && dcc.status!=="DNS") {
						//console.log(dcc);
						var data0=[];
						if (!dcc.status) data0.push({label:dcc.rank, colspan:1})
							else {
								data0.push({label:dcc.status, colspan:1, clazz:"verySmall"});
							}
						data0.push({label:dcc.competitor.fullName.replace(" ","&nbsp;"), colspan:1});
						data0.push({label:"<img  style='vertical-align:middle;margin-right:2px' src='./resources/flags/"+dcc.competitor.country.iso2.toLowerCase()+".png' width='16px'/>"+dcc.competitor.country.nameInt.replace(" ","&nbsp;"), colspan:1, clazz:"left flag"});
						data0=_.reduce(dcc.solutions, function(r, s){
							if (s.task.status==="regular" && s.task.categorys.indexOf(docs[0].category._id)>-1) {
								if (s.task.type==="CP") {
									if (!s.solution) s.solution="X";
									var h={label:s.solution, colspan:1};
									if (s.task.solution===s.solution) {
										h.clazz="correct";
									} else {
										h.clazz="wrong";
									}
									r.push(h);
								} 
							};
							return r;
						}, data0);
						data0=_.reduce(dcc.solutions, function(r, s){
							if (s.task.status==="regular" && s.task.categorys.indexOf(docs[0].category._id)>-1 
						   		&& ( team  || (!team && !s.task.team) )) {
						 		if (s.task.type==="ST") {
									r=_.reduce(s.tcSolutions, function(a,l){
										if (!l.solution) l.solution="X";
										var h={label:l.solution, colspan:1};
										var tc=_.findWhere(s.task.timeControls, {no:l.no})
										if (l.solution===tc.solution) {
											h.clazz="correct";
										} else {
											h.clazz="wrong";
										}
										a.push(h);
										return a;
									}, r);
									if (!s.time) s.time=0;
									if (!s.time2) s.time2=0;
									r.push({label:s.time+"/"+s.time2, colspan:1, clazz:"verySmall"});
									r.push({label:(s.time+s.time2)/2, colspan:1});
								}
							};
							return r;
						}, data0);
						if (!dcc.points) dcc.points=0;
						if (!dcc.time) dcc.time=0;
						if (run.type==="PreO") {
				            var min=Math.floor(dcc.timeOn/60.0);
				            var sec=dcc.timeOn-60*min;
				            var secStr=sec.toString();
				            if (sec<10) secStr="0"+secStr;
							data0.push({label:min+":"+secStr ,colspan:1, clazz:"verySmall"});
							data0.push({label:dcc.penalty ,colspan:1, clazz:"verySmall"});
							data0.push({label:dcc.points ,colspan:1});
						}
						data0.push({label:dcc.time ,colspan:1});
						d.push(data0);
					}
					return d;
				}, data);	
				//
				
				footer=_.map(results, function(r){
					var label="-";
					if (r.label!=="-") label=0; 
					var f={label:label, colspan: r.colspan};
					return f;
				});
				footer[0]={colspan:1, label:""};
				footer[1]={colspan:1, label:"", clazz:"verySmall"};
				footer[2]={colspan:1, label:"Correct answers", clazz:"verySmall"};

				footer=_.reduce(data.slice(2), function (f, d){ // Slice 2 because headers are allready pushed
					var less=3;
					if (run.type==="PreO") less=6;
					for(var idx=3; idx<d.length-less; idx++) {
						if (characters.indexOf(results[idx].label)>=0) {
							var label=f[idx].label;
							if (d[idx].label===results[idx].label) label++;
							f[idx].label=label;
						}
					}
					return f;
				}, footer);

				footer2.push({colspan:1, label:""});
				footer2.push({colspan:1, label:"www.tockacrta.com/trailos"});
				footer2.push({colspan:1, label:"%", clazz:"verySmall"});


				for (var idx=3;idx<footer.length; idx++) {
					if (results[idx].label!=="-") {
						var label=Math.round((footer[idx].label/(data.length-2))*100);
						footer2.push({label:label, colspan:1});
					} else {
						footer2.push({label:"-", colspan:1});
					}
				}

				for (var idx=0; idx<footer.length; idx++) {
					if (footer[idx].label==="-") {
						footer.splice(idx,1);
						footer[idx]={label:"", colspan:2};
					}
				}
				footer[footer.length-1].colspan=4;

				for (var idx=0; idx<footer2.length; idx++) {
					if (footer2[idx].label==="-") {
						footer2.splice(idx,1);
						footer2[idx]={label:"", colspan:2};
					}
				}
				footer2[footer2.length-1].colspan=4;

				footer.splice(0,1);
				footer[0].colspan=2;
				data.push(footer);

				footer2.splice(0,1);
				footer2[0].colspan=2;
				data.push(footer2);
				//data.push(results);

				var ret={header1:header1, header2:header2, data:data, run:run};
				callback(ret);		
			})
		}
	})
}


