/**
 * New node file
 * schema and models ....
 */
var mongoose = require('mongoose');

var source = require('../dao/subjects');
var model=mongoose.model('subjects');
var fs = require('fs');
var helper=require('../helper.js');

var sliceSize=10;


Schema = mongoose.Schema;

exports.saveImage=function saveImage(res, file, id, subId ) {
	
	require('../dao/binarys.js');
	var binarys=mongoose.model('binarys');
	model.findById(id, function (err, doc){
		if (subId) {
			// update
			} else {
				// new			
				var newBinary=new binarys;
				console.log(file);
				newBinary.img.data = fs.readFileSync(file.fileData.path);
				newBinary.img.contentType = file.fileData.mimetype;
				newBinary.save(function(err, binary){
					if (err) {
						console.log(err);
					} else {			
						var image={ _id: mongoose.Types.ObjectId(), name: file.fileData.originalname, dateAdded:new Date(), binaryId:binary._id};			
						doc.images.push(image);
						doc.save();			
						res.json(image);
					}
				});
			}
	});
	
}

exports.getSubjectOperations=function getOpertions(res, query) {
	var merge = require('merge'), original, cloned;
	var fromDate = new Date(query.date);
	delete query.date;
	helper.wildcard(query);
	model.aggregate([
        { $unwind: "$operations" },
        { $match: 
        	merge (
        	{
            	'operations.deadlineDate': {$gt:fromDate}
        	}, 
        	query)
        },
		{ $project : { caseNo : 1, caseName: 2, operations : 3 } } 
    ], function (err, result) {
        if (err) {
            console.log(err);
            return;
        }
        console.log(result);
        res.json(result);
    });
}


exports.findByType=function findByType(res, type,  query) {
	helper.wildcard(query);
	console.log(query);	
	model.findByType(type, query, function (error, subjects) {
		res.json(subjects);
	}).populate(model.populates());
}