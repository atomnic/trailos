/**
 * New node file
 * schema and models ....
 */
var mongoose = require('mongoose');

var source = require('../dao/runs');
var cmps = require('../dao/competitions');
require('../dao/categorys');
var categorys=mongoose.model('categorys');
var mongodb = require('../dao/mongodb');
var model=mongoose.model('runs');
var competitions=mongoose.model('competitions');
var fs = require('fs');
var helper=require('../helper.js');
var _=require('underscore');

var sliceSize=10;

Schema = mongoose.Schema;

function fincByCompetitionCallback(res, err, docs, callback) {
	if (err) {
		if (res) res.status(500).send(err);
	} else {
		if (res && (typeof callback === "function")) {
			callback(res, docs);
		} else {
			res.json(docs);
		}
	}
};

exports.findByCompetition=function(res, competition, query, callback) {
	helper.wildcard(query);
	var modelQuery;
	if (mongoose.Types.ObjectId.isValid(competition))  {
		query.competition=competition;
		model.find(query).deepPopulate(model.schema.populates()).exec(function(err, docs){
			fincByCompetitionCallback(res, err, docs, callback);
		});
	} else {
		competitions.findOne({code:competition}, function(err, doc){
			if (doc) {
				query.competition=doc._id;
				model.find(query).deepPopulate(model.schema.populates()).exec(function(err, docs){
					fincByCompetitionCallback(res, err, docs, callback);
				})
			} else  {
				if (res) res.status(500).send(err);
			}
		});
	}
}

exports.post=function(user, res, data) {
				// custom depopulate 2nd level
				/*
				if (data.timeLimits) {
					data.timeLimits=_.map(data.timeLimits, function(tl){
						tl.limitType=tl.limitType._id;
					})
				}*/
	if (mongoose.Types.ObjectId.isValid(data.competition)) {
		mongodb.insert(res, data, 'runs', user);
	} else {
		competitions.findOne({code:data.competition}, function(err, doc){
			if (doc) {
				data.competition=doc._id;
				mongodb.insert(res, data, 'runs', user);
			} else  {
				res.status(500).send(err);
			}
		});
	}
}

exports.put=function(user, res, data, id) {
					// custom depopulate 2nd level
					/*
				if (data.timeLimits) {
					data.timeLimits=_.map(data.timeLimits, function(tl){
						tl.limitType=tl.limitType._id;
					})
				}*/
	if (mongoose.Types.ObjectId.isValid(data.competition)) {
		data._id=id;
		mongodb.update(res, data, 'runs', id, user);
	} else {
		competitions.findOne({code:data.competition}, function(err, doc){
			if (doc) {
				data.competition=doc._id;
				data._id=id;
				mongodb.update(res, data, 'runs', id, user);
			} else  {
				res.status(500).send(err);
			}
		});
	}
}