/**
 * New node file
 * schema and models ....
 */
var mongoose = require('mongoose');

var source = require('../dao/runCmrs');
var runsDao = require('../dao/runs');
require("../dao/categorys");
var categorys=mongoose.model('categorys');
var mongodb = require('../dao/mongodb');
var model=mongoose.model('runCmrs');
var runs=mongoose.model('runs');
var fs = require('fs');
var helper=require('../helper.js');
var _=require('underscore');
var sts=require('./startTimesService.js')

var sliceSize=10;

Schema = mongoose.Schema;

function findByRunCallback(res, err, docs) {
	if (err) {
		console.log(err);
		res.status(500).send(err);
	} else {
							docs=_.map(docs, function(d){
								d.solutions=_.map(d.solutions, function(s){
									s.task.solution="*";
									s.task.timeControls=_.map(s.task.timeControls, function(t){
										t.solution="*";
										return t;
									})
									return s;
								})
								return d;
							});
		res.json(docs);
	}
};

exports.findByRun=function(res, run, query) {
	helper.wildcard(query);
	var modelQuery;

	var runQuery;
	var categoryQuery;
	if (typeof query.category!=='undefined') {
		if (mongoose.Types.ObjectId.isValid(query.category)) {
			categoryQuery=categorys.findById(query.category);
		} else {
			categoryQuery=categorys.findOne({code:query.category}); // TODO and competition, and event
		}	
	} else {
		categoryQuery=categorys.findById(0); // empty select 
	}	
	if (mongoose.Types.ObjectId.isValid(run)) {
		runQuery=runs.findById(run);
	} else {
		runQuery=runs.findOne({code:run}); // TODO and competition, and event
	}
	categoryQuery.exec(function(err, cat){
		runQuery.exec(function(err, doc){
			if (doc) {
				query.run=doc._id;
				if (typeof cat!=='undefined') query.category=cat._id;
				if (query._generate && typeof query._generateStartNoOnly==='undefined' && typeof query._basedOnRun==='undefined') {
					delete query._generate;					
					sts.generate(doc._id, query.group, query._delay, query._delta, query._checkGroup, query._startNoAlg, 
							query._startAt,function(docs0){
						delete query._delay;
						delete query._delta;
						delete query._checkGroup;
						delete query._startNoAlg;
						delete query._startAt;
						model.find(query).deepPopulate(model.schema.populates()).sort({"genStartMin":1}).exec(function(err, docs){
							findByRunCallback(res, err,docs);
						});
					});
				} else if (query._generate && typeof query._generateStartNoOnly==='undefined' && typeof query._basedOnRun!=='undefined') {
					delete query._generate;		
					var runQueryRank;			
					if (mongoose.Types.ObjectId.isValid(query._basedOnRun)) {
						runQueryRank=runs.findById(query._basedOnRun);
					} else {
						runQueryRank=runs.findOne({code:query._basedOnRun}); // TODO and competition, and event
					}
					delete query._basedOnRun;
					runQueryRank.exec(function(err, docRank){
						sts.generateByRank(doc._id, docRank._id, query.group, query._delay, query._delta, query._checkGroup, query._startNoAlg, 
							query._startAt,function(docs0){
							delete query._delay;
							delete query._delta;
							delete query._checkGroup;
							delete query._startNoAlg;
							delete query._startAt;
							//query.genStartMin={$gte:0};
							query.genStartSec={$gte:0};
							model.find(query).deepPopulate(model.schema.populates()).sort({"genStartMin":1}).exec(function(err, docs){
								findByRunCallback(res, err,docs);
							});
						});
					});
				} else if (query._generateStartNoOnly) {
					delete query._generateStartNoOnly;
					var alg=query._startNoAlg;
					var startAt=query._startAt;
					delete query._startNoAlg;
					delete query._startAt;
					sts.generateStartNoOnly(doc._id, query.group, alg, startAt,  function(docs0) {
						model.find(query).deepPopulate(model.schema.populates()).sort({"genStartMin":1}).exec(function(err, docs){
							findByRunCallback(res, err,docs);
						});
					})
				} else {
					model.find(query).deepPopulate(model.schema.populates()).sort({"genStartMin":1}).exec(function(err, docs){
						findByRunCallback(res, err,docs);
					});					
				}
			} else  {
				res.status(500).send(err);
			}
		});
	});
}

exports.post=function(user, res, data) {
	if (mongoose.Types.ObjectId.isValid(data.run)) {
		mongodb.insert(res, data, 'runCmrs',user);
	} else {
		runs.findOne({code:data.run}, function(err, doc){
			if (doc) {
				data.run=doc._id;
				mongodb.insert(res, data, 'runCmrs', user);
			} else  {
				res.status(500).send(err);
			}
		});
	}
}

exports.put=function(user, res, data, id) {
	if (mongoose.Types.ObjectId.isValid(data.run)) {
		mongodb.update(res, data, 'runCmrs', id, user);
	} else {
		runs.findOne({code:data.run}, function(err, doc){
			if (doc) {
				data.run=doc._id;
				mongodb.update(res, data, 'runCmrs', id,  user);
			} else  {
				res.status(500).send(err);
			}
		});
	}
}

exports.putTimesOnly=function(user, res, data, id) {
	if (mongoose.Types.ObjectId.isValid(data.run)) {
		// mongodb.update(res, data, 'runCmrs', id, user);
		model.findByIdAndUpdate(id,{breaks:data.breaks},{new:true}, function(err, docc){
			res.send(docc);
		});
	} else {
		runs.findOne({code:data.run}, function(err, doc){
			if (doc) {
				data.run=doc._id;
				// mongodb.update(res, data, 'runCmrs', id,  user);
				model.findByIdAndUpdate(id,{breaks:data.breaks},{new:true}, function(err, docc){
					res.send(docc);
				});
			} else  {
				res.status(500).send(err);
			}
		});
	}
}

exports.putSolutionsOnly=function(user, res, data, id) {
	if (mongoose.Types.ObjectId.isValid(data.run)) {
		// mongodb.update(res, data, 'runCmrs', id, user);
		model.findByIdAndUpdate(id,{solutions:data.solutions},{new:true}, function(err, docc){
			res.send(docc);
		});
	} else {
		runs.findOne({code:data.run}, function(err, doc){
			if (doc) {
				data.run=doc._id;
				// mongodb.update(res, data, 'runCmrs', id,  user);
				model.findByIdAndUpdate(id,{solutions:data.solutions},{new:true}, function(err, docc){
					res.send(docc);
				});
			} else  {
				res.status(500).send(err);
			}
		});
	}
}

exports.putSolutionsAndTimesOnly=function(user, res, data, id) {
	if (mongoose.Types.ObjectId.isValid(data.run)) {
		// mongodb.update(res, data, 'runCmrs', id, user);
		model.findByIdAndUpdate(id,{solutions:data.solutions, breaks:data.breaks},{new:true}, function(err, docc){
			res.send(docc);
		});
	} else {
		runs.findOne({code:data.run}, function(err, doc){
			if (doc) {
				data.run=doc._id;
				// mongodb.update(res, data, 'runCmrs', id,  user);
				model.findByIdAndUpdate(id,{solutions:data.solutions, breaks:data.breaks},{new:true}, function(err, docc){
					res.send(docc);
				});
			} else  {
				res.status(500).send(err);
			}
		});
	}
}

