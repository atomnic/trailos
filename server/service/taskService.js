/**
 * New node file
 * schema and models ....
 */
var mongoose = require('mongoose');

var source = require('../dao/runs');
var mongodb = require('../dao/mongodb');
var model=mongoose.model('tasks');
var runs=mongoose.model('runs');
var fs = require('fs');
var helper=require('../helper.js');
var _=require('underscore');

var sliceSize=10;

Schema = mongoose.Schema;

function fincByRunCallback(res, err, docs, admin) {
	if (err) {
		console.log(err);
		res.status(500).send(err);
	} else {
		if (!admin) {
			docs=_.map(docs, function(d){
				d.solution="*";
				d.timeControls=_.map(d.timeControls, function(t){
					t.solution="*";
					return t;
				})
				return d;
			});
		}
		res.json(docs);
	}
};

exports.findByRun=function(res, run, query, admin) {
	helper.wildcard(query);
	var modelQuery;
	if (mongoose.Types.ObjectId.isValid(run))  {
		query.run=run;
		model.find(query).populate(model.schema.populates()).sort({orderNo:1}).exec(function(err, docs){
			fincByRunCallback(res, err,docs, admin);
		});
	} else {
		runs.findOne({code:run}, function(err, doc){
			if (doc) {
				query.run=doc._id;
				model.find(query).populate(model.schema.populates()).sort({orderNo:1}).exec(function(err, docs){
					fincByRunCallback(res, err,docs, admin);
				})
			} else  {
				res.status(500).send(err);
			}
		});
	}
}

exports.post=function(user, res, data) {
	if (mongoose.Types.ObjectId.isValid(data.run)) {
		mongodb.insert(res, data, 'tasks',user);
	} else {
		runs.findOne({code:data.run}, function(err, doc){
			if (doc) {
				data.run=doc._id;
				mongodb.insert(res, data, 'tasks', user);
			} else  {
				res.status(500).send(err);
			}
		});
	}
}

exports.put=function(user, res, data, id) {
	if (mongoose.Types.ObjectId.isValid(data.run)) {
		data._id=id; // force id
		mongodb.update(res, data, 'tasks', id,  user);
	} else {
		runs.findOne({code:data.run}, function(err, doc){
			if (doc) {
				data.run=doc._id;
				data._id=id; // force id
				mongodb.update(res, data, 'tasks', id,  user);
			} else  {
				res.status(500).send(err);
			}
		});
	}
}

