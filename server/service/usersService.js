var mongoose = require('mongoose');

var source = require('../dao/users');
var model=mongoose.model('users');
var jwt = require("jsonwebtoken");

exports.authenticate=function authenticate(req, res) {
	model.findOne({username: req.body.username, password: req.body.password}, function(err, user) {
        if (err) {
            res.json({
                type: false,
                data: "Error occured: " + err
            });
        } else {
            if (user) {
            	user.token = jwt.sign(user.username, process.env.JWT_SECRET);
				user.save(function(err, user1) {
					res.json({
						type: true,
						data: user1,
						token: user1.token
					});
				});
            } else {
                res.json({
                    type: false,
                    data: "Incorrect username/password"
                });    
            }
        }
    });
}

exports.newUser=function addUser(req, res) {
	model.findOne({username: req.body.email, password: req.body.password}, function(err, user) {
        if (err) {
            res.json({
                type: false,
                data: "Error occured: " + err
            });
        } else {
            if (user) {
                res.json({
                    type: false,
                    data: "User already exists!"
                });
            } else {
                var newUser = new model();
                newUser.username = req.body.username;
                newUser.email = req.body.email;
                newUser.firstName=req.body.firstName;
                newUser.lastName=req.body.lastName;
                newUser.password = req.body.password;
                // TODO generate initials
                newUser.save(function(err, user) {
					res.json({
						type: true,
						data: user
					});
                })
            }
        }
    });
}