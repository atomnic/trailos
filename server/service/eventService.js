/**
 * New node file
 * schema and models ....
 */
var mongoose = require('mongoose');

var source = require('../dao/events');
var model=mongoose.model('events');
var fs = require('fs');
var helper=require('../helper.js');
var _=require('underscore');

var sliceSize=10;


Schema = mongoose.Schema;

exports.findByParents=function findByParents(res,  query) {
	helper.wildcard(query);
	var parents=_.pick(query, function(value, key, object){
		return (key.indexOf('parents.')==0);
	});

	var realQuery=_.omit(query, function(value, key, object){
		return (key.indexOf('parents')>=0);
	});

	var parentsJSON={};
	for (var idx=0; idx<Object.keys(parents).length; idx++) {
		var keySplits=Object.keys(parents)[idx].split('.');
		if (keySplits.length>1) {
			parentsJSON[keySplits[1]]=parents[Object.keys(parents)[idx]];
		}
	};


	
	model.findByParents(parentsJSON, realQuery, function (error, subjects) {
		res.json(subjects);
	}).populate(model.schema.populates());
}