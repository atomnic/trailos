/**
 * New node file
 * schema and models ....
 */
var mongoose = require('mongoose');

var source = require('../dao/competitions');
var mongodb = require('../dao/mongodb');
var model=mongoose.model('competitions');
var events=mongoose.model('events');
var fs = require('fs');
var helper=require('../helper.js');
var _=require('underscore');

var sliceSize=10;

Schema = mongoose.Schema;

function fincByEventCallback(res, err, docs, callback) {
	if (err) {
		if (res) res.status(500).send(err);
	} else {
		if (res && (typeof callback === "function")) {
			callback(res, docs);
		} else {
			res.json(docs);
		}
	}
};

exports.findByEvent=function(res, event, query, callback) {
	helper.wildcard(query);	
	var modelQuery;
	if (mongoose.Types.ObjectId.isValid(event))  {
		query.event=event;
		model.find(query).populate(model.schema.populates()).exec(function(err, docs){
			fincByEventCallback(res, err, docs, callback);
		});
	} else {
		events.findOne({code:event}, function(err, doc){
			if (doc) {
				query.event=doc._id;
				model.find(query).populate(model.schema.populates()).exec(function(err, docs){
					fincByEventCallback(res, err, docs, callback);
				})
			} else  {
				if (res) res.status(500).send(err);
			}
		});
	}
}

exports.post=function(user, res, data) {
	if (mongoose.Types.ObjectId.isValid(data.event)) {
		mongodb.insert(res, data, 'competitions',user);
	} else {
		events.findOne({code:data.event}, function(err, doc){
			if (doc) {
				data.event=doc._id;
				mongodb.insert(res, data, 'competitions', user);
			} else  {
				res.status(500).send(err);
			}
		});
	}
}

exports.put=function(user, res, data, id) {
	if (mongoose.Types.ObjectId.isValid(data.event)) {
		data._id=id; // force id
		mongodb.update(res, data, 'competitions', id,  user);
	} else {
		events.findOne({code:data.event}, function(err, doc){
			if (doc) {
				data.event=doc._id;
				data._id=id; // force id
				mongodb.update(res, data, 'competitions', id,  user);
			} else  {
				res.status(500).send(err);
			}
		});
	}
}

