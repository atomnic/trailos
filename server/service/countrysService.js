/**
 * New node file
 * schema and models ....
 */
var mongoose = require('mongoose');

var source = require('../dao/countrys');
var model=mongoose.model('countrys');
var fs = require('fs');
var helper=require('../helper.js');

var sliceSize=10;


Schema = mongoose.Schema;

exports.saveImage=function saveImage(res, file, id ) {
	
	require('../dao/binarys.js');
	var binarys=mongoose.model('binarys');
	model.findById(id, function (err, doc){		
		var newBinary=new binarys;
		newBinary.img.data = fs.readFileSync(file.fileData.path);
		newBinary.img.contentType = file.fileData.mimetype;
		newBinary.save(function(err, binary){
			if (err) {
				console.log(err);
			} else {			
				binarys.remove(doc.flagImg);
				doc.flagImg=binary._id;
				doc.save();			
				res.json(doc);
			}
		});
	});
	
}

exports.getFlagImg=function(res, id) {
	require('../dao/binarys.js');
	var binarys=mongoose.model('binarys');
	// Read only country _id and populate flagImg
	model.find({_id:id},'flagImg').populate('flagImg').exec(function (err, result) {
        if (err) {
            console.log(err);
            return;
        }
        var file=result[0].flagImg;
		if (file && file.img ) {
		   	res.contentType(file.img.contentType);
	    	res.send(file.img.data);			
	    } else {
	    	res.send({});
	   	}
    });
}

