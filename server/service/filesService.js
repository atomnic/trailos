/**
 * New node file
 * schema and models ....
 */
var mongoose = require('mongoose');

var source = require('../dao/files');
var model=mongoose.model('files');
var fs = require('fs');
var us=require('underscore');


var sliceSize=2;


Schema = mongoose.Schema;

function findNext( doc, folders, idx, binary, res, file, folder) {
	model.findById(doc._id).populate('folders','name').exec(function(err, doc0){
		if (doc0) {
			if (idx<folders.length) {
				var doc2=us.findWhere(doc0.folders, {'name':folders[idx]});
				idx++;
				findNext(doc2, folders, idx, binary, res, file, folder);			
			} else {			
				if (file) {					
					model.create({ name: file.fileData.originalname, fileType:0, binaryId:binary._id, big:false}, function (err, post) {				
						doc0.files.push(post._id);
						doc0.save();			
						res.json(post);
					});
				} else if (folder) {				
					doc0.folders.push(folder._id);
					doc0.save();
					res.json(folder);
				}
			}
		}
	});
}

exports.saveFile=function saveFile(res, file, folder ) {
	
	require('../dao/binarys.js');
	var binarys=mongoose.model('binarys');
	//model.findById(id, function (err, doc){
	var folders=folder.split("/");
	var doc;
	// new			
	var newBinary=new binarys;
	newBinary.img.data = fs.readFileSync(file.fileData.path);
	newBinary.img.contentType = file.fileData.mimetype;
	newBinary.save(function(err, binary){
		if (err) {
			console.log(err);
		} else {
			model.find({name:folders[0]}).populate('folders','name').exec(function (err, docs){
				if (docs) {							
					var doc2=us.findWhere(docs[0].folders, {'name':folders[1]});								
					findNext(doc2, folders, 2, binary, res, file);
				}
			});
		}
	});
}

exports.saveFolder=function saveFolder(res, newFolderName, folder ) {
					
	require('../dao/binarys.js');
	var binarys=mongoose.model('binarys');
	//model.findById(id, function (err, doc){
	var folders=folder.split("/");
	var doc;
	console.log(folders);
	// new			
	var newFolder=new model; // model == files
	newFolder.name=newFolderName;
	newFolder.type=1;
	newFolder.folders=[];
	newFolder.files=[];
	newFolder.big=false;
	newFolder.save(function(err, binary){
		if (err) {
			console.log(err);
		} else {
			model.find({name:folders[0]}).populate('folders','name').exec(function (err, docs){
				if (docs) {
					var doc2=us.findWhere(docs[0].folders, {'name':folders[1]});								
					findNext(doc2, folders, 2, binary, res, false, newFolder);
				}
			});
		}
	});								
};	
	
	
	