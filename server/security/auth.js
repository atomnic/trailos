var jwt = require('jwt-simple');
var mongoose = require('mongoose');
var source = require('../dao/users');
var model=mongoose.model('users');

var auth = {
	login: function(req, res) {
		var username = req.body.username || '';
		var password = req.body.password || '';
		if (username == '' || password == '') {
			res.status(401);
			res.json({
				"status": 401,
				"message": "Invalid credentials"
			});
			return;
		}

		// Fire a query to your DB and check if the credentials are valid
		var dbUserObj = auth.validate(res, username, password);

	},

	validate: function(res, username, password) {
		// spoofing the DB response for simplicity
		model.findOne({username:username, password:password}, function(err, user){
			if (!user) { // If authentication fails, we send a 401 back
					res.status(401);
					res.json({
						"status": 401,
						"message": "Invalid credentials"
					});
				return;
			}
			res.json(genToken(user));
		})
	},

	validateUser: function(res, username, callback) {
		model.findOne({username:username},callback);
  	}
}

// private method
function genToken(user) {
	var expires = expiresIn(1); 
	var token = jwt.encode({
			exp: expires
		}, require('../config/secret')()
	);  
	return {
		token: token,
		expires: expires,
		user: user
	};
}

function expiresIn(numDays) {
	var date = new Date();
	return date.setDate(date.getDate() + numDays);
}

module.exports = auth;