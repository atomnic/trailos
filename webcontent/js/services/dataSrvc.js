smipdMain.service('dataSrvc', function($http, underscore, messageSrvc) {

    var storageUrl="/trailos/";

    this.deleteCollection=function(collectionName, id){
        var url=storageUrl+collectionName+"/"+id;
        return $http.delete(url);
    }

    this.getStates=function(type){
        return $http.get(storageUrl+"states/"+type);
    }

    this.getStates2=function(type){
        return this.getStates(type).then(
            function(response){
                return response.data;
            }, function(error){
                return [];
            }
        );
    }

    this.getCollection=function(collectionName, filters, offset, limit){
        if (offset || offset==0) {
            var offset={property:"offset", value:offset, wildCard:false};
            filters.push(offset);
        }
        if (limit || limit==0) {
            var limit={property:"limit", value:limit, wildCard:false};
            filters.push(limit);
        }
        var filter=underscore.reduce(filters, function(query, element){
            var value=element.value;
            if (element.wildCard) {
                value=value+"*";
            }
            query=query+"&"+element.property+"="+value;
            return query;
        }, "");
        var filter=filter.replace("&","?");
        var url=storageUrl+collectionName+filter;
        return $http.get(url);
    }

this.getCollection2=function(collectionName, filter, offset, limit, options){
        if (offset || offset==0) {
            filter.offset=offset;
        }
        if (limit || limit==0) {
            filter.limit=limit;
        }
        var query="";
        for(var idx=0; idx<Object.keys(filter).length; idx++){
            var key=Object.keys(filter)[idx];
            query=query+"&"+key+"="+filter[key];
            if (options && options[key]) {
                if (options[key].wildCard) {
                    query=query+"*";
                }   
            } 
        }
        var query=query.replace("&","?");
        var url=storageUrl+collectionName+query;
        return $http.get(url);
    }

this.getCollection3=function(collectionName, filter, offset, limit, options){
        if (offset || offset==0) {
            filter.offset=offset;
        }
        if (limit || limit==0) {
            filter.limit=limit;
        }
        var query="";
        for(var idx=0; idx<Object.keys(filter).length; idx++){
            var key=Object.keys(filter)[idx];
            query=query+"&"+key+"="+filter[key];
            if (options && options[key]) {
                if (options[key].wildCard) {
                    query=query+"*";
                }   
            } 
        }
        var query=query.replace("&","?");
        var url=storageUrl+collectionName+query;
        return $http.get(url).then(
            function(response){
                return response.data;
            }, function(error) {
                messageSrvc.growlIt("e103", collectionName);
                console.log("Unabl to read competitors");
            }
        );
    }

    this.deleteConfirm=function(message, collection, id, clientCollection, index, callback) {
        bootbox.confirm(message, function(result) {
            if (result) {
                if (id) {       
                    dataSrvc.deleteCollection(collection, id).then(
                        function(response) {
                            if (clientCollection) {
                                for (var idx=0; idx < clientCollection.length; idx++) {
                                    if (clientCollection[idx]._id==id) {
                                        clientCollection.splice(parseInt(idx),1);
                                        //$scope.$apply();
                                        if (callback)
                                            callback();
                                        break;
                                    }
                                };
                            }
                        },
                        function(error){
                            messageSrvc.growlIt("e105", collection);
                        }
                    );
                } else {
                    if (clientCollection && (index || index==0)) {
                        clientCollection=clientCollection.splice(parseInt(index),1);
                        //$scope.$apply();
                        if (callback)
                            callback();
                    }
                }
            };
        });
    }
})