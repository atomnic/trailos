smipdMain.service('messageSrvc', 
    function(growl) {
    	// 100-199 errors
    	// 200-299 warnings
    	// 300-399 infos
    	// 400-500 questions - not sutable for growl
    	var messages={
    		"e000": "Message % is not defined!",
    		"e100": "Unexpected error! %", // TODO separate with , and just add new for exanple i201: Information abotu something
            "e198": "Server error: %",
            "e199": "Unexpected error on server! %",
    		"e101": "Passwords doesn't match!",            
    		"e102": "User with username % already exists!",
            "e103": "Can not read from collection %!",
            "e104": "Can not save competitor!",
            "e105": "Can not delete row from %!",
            "e198": "User has no rights on accessed resource!",
            "i300": "Saved....",
            "i301": "Sent....",
            "w100": "Wrong character, only: A B C D E F Z or X allowed!"
    	};

        this.getMessage=function() {  // parameters via special arguments array
            if (arguments.length>0) {
                var code=arguments[0];
                var message=messages[code];
                if (message) {
                    for(var idx=1; idx<arguments.length; idx++) {
                        message=message.replace('%', arguments[idx]);
                    }
                    return {code:code , message:message} ; //+": "+message;
                } else {
                    message=messages['e000'];
                    var newCode='e000';
                    message=message.replace('%',code);
                    return {code: newCode, message: message};
                }
            }
        }

        this.growlIt=function() {
            var message=this.getMessage.apply(this, arguments);
            if (message.code.charAt(0)=='e') {
                return growl.error(message.code+": "+message.message);

            } else if (message.code.charAt(0)=='w') {
                return growl.warning(message.code+": "+message.message);

            } else if (message.code.charAt(0)=='i') {
                return growl.info(message.message);

            } else if (message.code.charAt(0)=='q') {
                return growl.error(message.message);
            }
        }

        this.silentInfo=function(text) {
            return growl.info(text);
        }
    }
)