/**
 * New node file
 * It can be refactored based on ng-model $(element).attr('ng-model')
 */
smipdMain.directive('passwordValidate', function(messageSrvc, $http) {

  	function link(scope, element) {
		var error=false;
		var controllerScope=scope;
    	element.bind('change', function() {
    		if (error) {
		   		error.destroy();
			}
    		if (scope.editRow.password && scope.password2 && scope.editRow.password!=scope.password2) {
    			messageSrvc.growlIt("e101");
   		   		$(element).parent().addClass('has-error');
   		   		$(element).focus();
			} else {			    		
				$(element).parent().removeClass('has-error');	
    		}
    	}); 
  };
  return {
	  restrict: "EA",	  
	  link: link,
	  scope: false
  };
});