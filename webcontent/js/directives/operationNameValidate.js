/**
 * New node file
 * It can be refactored based on ng-model $(element).attr('ng-model')
 */
smipdMain.directive('operationNameValidate', function(growl, $http) {
  // var error;
  // var controllerScope;
  function link(scope, element) {
	var error=false;
	var controllerScope=scope;
    element.bind('keyup', function() {
    	var value=element[0].value.toUpperCase(); 
    	$http.get("/producta/operations?name="+value+"*").then(
				function (response){
					if (response.data.length>0) {
						if (error) {
			    			error.destroy();
						}
			    		$(element).parent().removeClass('has-error');	
			    		controllerScope.editRow.operationId=response.data[0];
					} else {
						// TODO if instrunction doesn't exists copy value into separete field (not yet defined)
					}
				},
				function (error) {
					console.error(error);
				});		 
    }); 
  };
  return {
	  restrict: "EA",	  
	  link: link,
	  scope: false
  };
});