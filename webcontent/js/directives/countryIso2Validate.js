/**
 * New node file
 * It can be refactored based on ng-model $(element).attr('ng-model')
 */
smipdMain.directive('countryIso2Validate', function(growl, $http) {
  // var error;
  // var controllerScope;
  function link(scope, element) {
	var error=false;
	var controllerScope=scope;
    element.bind('change', function() {
    	var value=element[0].value.toUpperCase(); 
    	$http.get("/producta/countrys?iso2="+value).then(
				function (response){
					if (response.data.length>0) {
						if (error) {
			    			error.destroy();
						}
			    		$(element).parent().removeClass('has-error');	
			    		controllerScope.editRow.countryId=response.data[0];
					} else {
						error=growl.error("Nispravan unos države!");
			    		$(element).parent().addClass('has-error');
			    		delete controllerScope.editRow.countryId._id;
					}
				},
				function (error) {
					console.error(error);
				});		 
    }); 
  };
  return {
	  restrict: "EA",	  
	  link: link,
	  scope: false
  };
});