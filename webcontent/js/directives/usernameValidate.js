/**
 * New node file
 * It can be refactored based on ng-model $(element).attr('ng-model')
 */
smipdMain.directive('usernameValidate', function(messageSrvc, $http) {
  // var error;
  // var controllerScope;
	var storageUrl=""; // test always loads data from node.js server this should be part of some global config
	var endPoint="/producta/users";  // 
	var url=storageUrl+endPoint;

  	function link(scope, element) {
		var error=false;
		var controllerScope=scope;
    	element.bind('change', function() {
    		var value=element[0].value.toLowerCase(); 
    		$http.get(url+"?username="+value).then(
				function (response){
					if (error) {
			   			error.destroy();
					}
					if (response.data.length>0 && (!scope.editRow._id || scope.editRow._id===resposne.data[0]._id)) {
						error=messageSrvc.growlIt("e102", value);
			    		$(element).parent().addClass('has-error');
					} else {
			    		$(element).parent().removeClass('has-error');	
					}
				},
				function (error) {
					console.error(error);
				});		 
    	}); 
  };
  return {
	  restrict: "EA",	  
	  link: link,
	  scope: false
  };
});