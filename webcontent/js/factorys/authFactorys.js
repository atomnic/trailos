smipdMain.factory('authenticationFactory', function($window) {
	var auth = {
		isLogged: false,
		user:function(){ return $window.sessionStorage.user},
		userRole:function(){ return $window.sessionStorage.userRole},
		token:function(){ return $window.sessionStorage.token},
		check: function() {
			if ($window.sessionStorage.token && $window.sessionStorage.user) {
				this.isLogged = true;
			} else {
				this.isLogged = false;
				delete this.user;
			}
			return this.isLogged;
		}
	}
	return auth;
});

smipdMain.factory('userAuthFactory', function($window, $location, $http, authenticationFactory) {
	return {
		login: function(username, password, callback) {
			return $http.post('/authenticate', {	// TODO get rid of hardcoded url
				username: username,
				password: password
			}).then(function(response) {
				authenticationFactory.isLogged = true;
				//authenticationFactory.user = response.data.user.username;
				//authenticationFactory.userRole = response.data.user.roles;
				$window.sessionStorage.token = response.data.token;
				$window.sessionStorage.user = response.data.user.username; // to fetch the user details on refresh
				$window.sessionStorage.userRole = response.data.user.roles; // to fetch the user details on refresh
			});
		},
		logout: function() {
			if (authenticationFactory.isLogged) {
				authenticationFactory.isLogged = false;
				// delete authenticationFactory.user;
				// delete authenticationFactory.userRole;
				delete $window.sessionStorage.token;
				delete $window.sessionStorage.user;
				delete $window.sessionStorage.userRole;
				$location.path("login");
			}
		}
	}
});

smipdMain.factory('tokenInterceptor', function($q, $window) {
	return {
		request: function(config) {
			config.headers = config.headers || {};
			if ($window.sessionStorage.token) {
				config.headers['X-Access-Token'] = $window.sessionStorage.token;
				config.headers['X-Key'] = $window.sessionStorage.user;
				//config.headers['Content-Type'] = "application/json";
			}
			return config || $q.when(config);
		},
		response: function(response) {
			return response || $q.when(response);
		}
	};
});

smipdMain.factory('securityFactory', function() {
	return {

	}
})