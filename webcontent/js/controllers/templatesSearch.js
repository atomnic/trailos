/**
 * 
 */

smipdMain.controller("templatesSearch",function($rootScope, $scope, $http, $window, $location, $modal, $upload, underscore) {
	// init, load from server
	var storageUrl=""; // test always loads data from node.js server
	var endPoint="/producta/files";
	var url=storageUrl+endPoint;	
	
	$scope.treeOptions = {
		    nodeChildren: "children",
		    dirSelectable: true,
		    injectClasses: {
		        ul: "a1",
		        li: "a2",
		        liSelected: "a7",
		        iExpanded: "a3",
		        iCollapsed: "a4",
		        iLeaf: "a5",
		        label: "a6",
		        labelSelected: "a8"
		    }
		}
		
	
	/* $scope.dataForTheTree =
		[
		    // { "id":1, "category" : "Predlošci", "url":"", "children" : [] , "parent"}
		];
	*/
	$scope.pseudoRoot=$scope.$parent.dataForTheTree[4].children[0];	
				
	$scope.updateTree=function(parent, file, update){
		var node;
		if (!update) {
			node={id:file._id, category:file.name, file:file, children:[], parent:parent, broadcast:true, signal:"showFiles"};
			parent.children.push(node);
		} else {
			parent.file=file;
			parent.children=[];
			parent.id=file._id;
			node=parent;
			parent.broadcast=true;
			parent.signal="showFiles";
		}
		var folders=file.folders;
		for(var idx=0;idx<folders.length;idx++ ) {
			var folder=folders[idx];
			var queryUrl=url+"?_id="+folder._id;
			$http.get(queryUrl).then(
					function (response){									
						$scope.updateTree(node, response.data, false);
					},
					function (error) {
						console.error(error);
					});						
		}
		return node;
	}
	
	$scope.init=function() {
		$scope.load("Predlošci", true); // $scope.dataForTheTree[0].category)
		$window.document.title = "Predlošci";
	}
	
	$scope.load=function(nodeName, root) {
		var queryUrl=url+"?name="+nodeName;
		$http.get(queryUrl).then(
				function (response){			
					if (response.data.length>1 && root) {
						// not real root
						// TODO respond with error
						console.error(root+" is not real root!")
						return;
					}
					if (response.data.length==1 && !response.data[0].big) {
						$scope.files=underscore.filter(response.data[0].files,{fileType:0});
						$scope.updateTree($scope.pseudoRoot, response.data[0], true);
					}
				},
				function (error) {
					console.error(error);
				});		
	}

	$scope.$on('showFiles', function(event, arg){
		$scope.showFiles(arg)
	})
	
	$scope.showFiles=function(node) {
		$scope.files=node.file.files;
	};
	
	
	$scope.remove = function(){ 
		$scope.subjects.splice(this.$index, 1); 
	}
	
	
	$scope.getClass = function(path) {
		if ($location.path().indexOf(path) >= 0) {
	      return "active";
	    } else {
	      return "";
	    }
	}
	
	$(function() {
	    $( ".resizable tr th" ).resizable({
	      handles: 'e'
	    });
	  });
	
	
	$scope.searchOptions=
		[ {"value":"null",  "label":" "},
		  {"value":"name",  "label":"naziv predloška"}
		];	

	$scope.emptyPair={"key":$scope.searchOptions[0], "value":""}; // key == value from searchOptions
	
	$scope.searchPairs =
		[ {id:1, "key":$scope.searchOptions[1], "value":""},
		];

	$scope.addPair = function() {
		var id=$scope.searchPairs.length+1;
		$scope.searchPairs.push({id:id,"key":{"value":"null",  "label":" "},"value":"" });
	};
	
	    
	$scope.deletePair = function(pairId){
		$scope.searchPairs.splice(pairId,1);
	    };
	        
	
	$(function() {
	    $( "#table-css-border-1 tr th" ).resizable({
	      handles: 'e'
	    });
	  });
	
	$( "button[type=save]" ).button({icons: {primary: "ui-icon ui-icon-disk"}});
	$( "button[type=plus]" ).button({icons: {primary: "ui-icon ui-icon-plus"}}); 
	$( "button[type=search]" ).button({icons: {primary: "ui-icon ui-icon-search"}}); 
	$( "button[type=new]" ).button({icons: {primary: "ui-icon ui-icon-folder-open"}}); 
	$( "button[type=delete]" ).button({icons: {primary: "ui-icon ui-icon-trash"}});
	
	
	$scope.delete=function(id) {
		$http.delete(url+"/"+id).then(
				function (response){
					$scope.subjectsRows=response.data;
				},
				function (error) {
					console.error(error);
				});		
	}
	
	$scope.new=function() {
		$http.put(url).then(
				function (response){
					$scope.subjectsRows=response.data;
				},
				function (error) {
					console.error(error);
				});		
	}
	
	$scope.path=function() {
		var end=false;
		var path;
		// TODO open doesn't work
		var node=$scope.$parent.selectedNode;
		while (!end) {
			if (node.category) {
				if (!path) {
					path=node.category
				} else {
					path=node.category+'/'+path;
				}
			}
			if (node.parent) {					
				node=node.parent;
			} else {
				end=true;
			}
		}
		return path;
	}
	
	
	$scope.openModalFiles= function(templateUrl, forRow) {
	   	 var modalInstance = $modal.open({
	   	      templateUrl: templateUrl,
	   	      controller: 'modalUploadFilesCtrl',
	   	      resolve: {
	   	        row: function () {
	   	        	if (forRow) {
	   	        		return forRow;
	   	        	}
	   	        	else {
	   	        		$scope.newRow={};
	   	        		return $scope.newRow;
	   	        	}
	   	        },
	   	        folder: function() {
	   	        	return  $scope.path();// $scope.selectedNode;
	   	        }
	   	      }
	   	    });

	   	    modalInstance.result.then(function (result) {
	   	      if (result=='OK') {
	   	    	  if ($scope.newRow) {
	   	    		  $scope.editRow.images.push($scope.newRow);
	   	    		  $scope.save();
	   	    		  delete $scope.newRow;
	   	    	  } else {
	   	    		  $scope.save();
	   	    	  }
	   	      }
	   	    }, function () { // dialog dissmised
	   	    	$scope.load();
	   	    });
	   	  };
	
	$scope.selected=[];
	$scope.select=function(id) {
		var index=$scope.selected.indexOf(id);
		if (index<0) {
			$scope.selected.push(id);
		} else {
			$scope.selected.splice(index,1);
		};
	}
			
	
	$scope.deleteImage=function(imageId) {
		growl.warning("Nedostaje prozor sa pitanjem da li ste sigurni!");
		var method="DELETE";
		var urlForId=url;
		urlForId=url+'/'+$scope.id+'/images/'+imageId;			 
		$http({url:urlForId, method:method}).then(
			function (response){
				// $scope.editRow.images.splice(pos,1);
				$scope.load();
			},
			function (error) {
				console.error(error);
			});				
	};	
	
	$scope.showPic = function(fileId) {
		$scope.imageUrl=url+'/'+$scope.id+'/files/'+fileId;	
	};
	
	$scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
	$scope.uploadPic = function(files) {
		$scope.formUpload = true;
		if (files != null) {
			generateThumbAndUpload(files[0])
		}
	};
	
	function generateThumbAndUpload(file) {
		$scope.howToSend=1;
		$scope.errorMsg = null;
		$scope.generateThumb(file);
		if ($scope.howToSend === 1) {
			uploadUsing$upload(file);
		} else if ($scope.howToSend == 2) {
			uploadUsing$http(file);
		} else {
			uploadS3(file);
		}
	}
	$scope.picFile=null;
	$scope.generateThumb = function(file) {
		if (file != null) {
			if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
				$timeout(function() {
					var fileReader = new FileReader();
					fileReader.readAsDataURL(file);
					fileReader.onload = function(e) {
						$timeout(function() {
							file.dataUrl = e.target.result;
						});
					}
				});
			}
		}
	};
	
	function uploadUsing$upload(file) {
		file.upload = $upload.upload({
			url: '/producta/files?folder=Predlošci#Patenti',
			method: 'POST',
			headers: {
				'my-header' : 'my-header-value'
			},
			fields: {username: $scope.username},
			file: file,
			fileFormDataName: 'fileData'
		});

		file.upload.then(function(response) {
			$timeout(function() {
				file.result = response.data;
			});
			$scope.load();			
		}, function(response) {
			if (response.status > 0)
				$scope.errorMsg = response.status + ': ' + response.data;
		});

		file.upload.progress(function(evt) {
			// Math.min is to fix IE which reports 200% sometimes
			file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
		});

		file.upload.xhr(function(xhr) {
			// xhr.upload.addEventListener('abort', function(){console.log('abort complete')}, false);
		});
	}
	
	function uploadUsing$http(file) {
		var fileReader = new FileReader();
		fileReader.onload = function(e) {
			$timeout(function() {
				file.upload = $upload.http({
					url: 'https://angular-file-upload-cors-srv.appspot.com/upload' + $scope.getReqParams(),
					method: 'POST',
					headers : {
						'Content-Type': file.type
					},
					data: e.target.result
				});
			
				file.upload.then(function(response) {
					file.result = response.data;
				}, function(response) {
					if (response.status > 0)
						$scope.errorMsg = response.status + ': ' + response.data;
				});
			
				file.upload.progress(function(evt) {
					file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
				});
			}, 5000);
		};
		fileReader.readAsArrayBuffer(file);
	}	
		
	$scope.search=function() {
		var url2="";
		for (var int = 0; int < $scope.searchPairs.length; int++) {
			if ($scope.searchPairs[int].value!=="") {
				url2= url2 + "&" + $scope.searchPairs[int].key.value + "=" + $scope.searchPairs[int].value;				
			};
		}
		url2=url2.replace("&", "?");
		url2=url+url2;
		$http.get(url2).then(
				function (response){
					$scope.subjects=response.data;
				},
				function (error) {
					console.error(error);
				});
	}
	
	$scope.createNewFolder=function() {
		
	}
	
	$scope.showTemplate=function(row) {
		// TODO download binary with mimeType... check on internet
	}

})