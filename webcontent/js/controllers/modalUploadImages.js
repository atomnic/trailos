/**
 * 
 */

smipdMain.controller("modalUploadImages",function($scope, $modalInstance, subjectId, row, $upload, $http, $timeout) {

	var storageUrl=""; // test always loads data from node.js server this should be part of some global config
	var endPoint="/producta/subjects";  // 
	var url=storageUrl+endPoint;

	$scope.imageUrl
	
	$scope.editRow=row;
	$scope.forSubjectId=subjectId;

	// This thre functions are common to all modalas...	  
		$scope.open = function($event, name) {
			    $event.preventDefault();
			    $event.stopPropagation();
			    $scope[name]=true;
			  };		
		
		  $scope.ok = function () {
		    $modalInstance.close('OK');
		  };

		  $scope.cancel = function () {
		    $modalInstance.dismiss('cancel');
		  };
	
	
	
	$scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
	$scope.uploadPic = function(files) {
		$scope.formUpload = true;
		if (files != null) {
			generateThumbAndUpload(files[0])
		}
	};
	
	function generateThumbAndUpload(file) {
		$scope.howToSend=1;
		$scope.errorMsg = null;
		$scope.generateThumb(file);
		if ($scope.howToSend === 1) {
			uploadUsing$upload(file);
		} else if ($scope.howToSend == 2) {
			uploadUsing$http(file);
		} else {
			uploadS3(file);
		}
	}
	$scope.picFile=null;
	$scope.generateThumb = function(file) {
		if (file != null) {
			if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
				$timeout(function() {
					var fileReader = new FileReader();
					fileReader.readAsDataURL(file);
					fileReader.onload = function(e) {
						$timeout(function() {
							file.dataUrl = e.target.result;
						});
					}
				});
			}
		}
	};
	
	function uploadUsing$upload(file) {
		file.upload = $upload.upload({
			url: '/producta/subjects/'+$scope.forSubjectId+'/images',
			method: 'POST',
			headers: {
				'my-header' : 'my-header-value'
			},
			fields: {username: $scope.username},
			file: file,
			fileFormDataName: 'fileData'
		});

		file.upload.then(function(response) {
			$timeout(function() {
				file.result = response.data;
			});
			$scope.imageUrl=url+'/'+$scope.forSubjectId+'/images/'+response.data._id+'/binary';			
		}, function(response) {
			if (response.status > 0)
				$scope.errorMsg = response.status + ': ' + response.data;
		});

		file.upload.progress(function(evt) {
			// Math.min is to fix IE which reports 200% sometimes
			file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
		});

		file.upload.xhr(function(xhr) {
			// xhr.upload.addEventListener('abort', function(){console.log('abort complete')}, false);
		});
	}	
	
	
	
});

