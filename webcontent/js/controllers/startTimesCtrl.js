/**
 * 
 */

smipdMain.controller("startTimesCtrl",function($route, $rootScope, $scope, $http, $window, $location, $modal, authenticationFactory, messageSrvc, dataSrvc, underscore) {
	// init, load from server
	if ($route.current.params.eventCode) {
        $scope.eventCode = $route.current.params.eventCode;
    };
    if ($route.current.params.competitionCode) {
        $scope.competitionCode = $route.current.params.competitionCode;
    };

    if ($route.current.params.runCode) {
        $scope.runCode = $route.current.params.runCode;
    };

	$scope.printQuery={}
    if ($route.current.params.category) {
		// Only for print
		$scope.printQuery.category=$route.current.params.category;
	}
	if ($route.current.params.group) {
		// Only for print
		$scope.printQuery.group=$route.current.params.group;
	}
	if ($route.current.params.onlyGenerated) {
		$scope.onlyGenerated=true;
	}

	if ($route.current.params.groupStart) {  // zero start time for print
		$scope.groupStart=$route.current.params.groupStart;
	}
	var storageUrl=""; // test always loads data from node.js server
	//var endPoint="/trailos/events/"+$scope.eventCode+"/competitions/"+$scope.competitionCode+"/runs/"+$scope.runCode+"/tasks/";
	var endPoint="/trailos/runs/"+$scope.runCode+"/competitors";
	var url=storageUrl+endPoint;	

	$scope.$parent.setHideMenu(false);
	$scope.$parent.setHideTools(false);

	if ($location.path().indexOf('startTimesPrint')>0) {
		$scope.$parent.setHideMenu(true);
		$scope.$parent.setHideTools(true);
	} else {
		$scope.$parent.setHideMenu(false);
		$scope.$parent.setHideTools(false);
	}

	$scope.init=function() {
		if (authenticationFactory.check()) {
			$scope.search(false);
			$scope.load();
		} else {
			$location.path("login");
		}
	}
	
	$scope.load=function() {
		$http.get("trailos/events/"+$scope.eventCode+"/competitions/"+$scope.competitionCode+"/runs/"+$scope.runCode).then(
			function(response){
				$scope.run=response.data[0];
			}, function(error){
			messageSrvc.growlIt("e103", 'run/stage '+$scope.runCode); 
		});
	}

	
	
	$scope.getClass = function(path) {
		if ($location.path().indexOf(path) >= 0) {
	      return "active";
	    } else {
	      return "";
	    }
	}
	
	$(function() {
	    $( ".resizable tr th" ).resizable({
	      handles: 'e'
	    });
	  });
	
	
	$scope.searchOptions=
		[ 
		  {"value":"competitor",  "label":"Competitor", "typeahead":"competitors"},
		  {"value":"category",  "label":"Category", "typeahead":"categorys"},
		  {"value":"group",  "label":"Group"}
		];	

	$scope.emptyPair={"key":$scope.searchOptions[0], "value":""}; // key == value from searchOptions
	
	$scope.searchPairs =
		[];

	$scope.addPair = function() {
		var id=$scope.searchPairs.length+1;
		$scope.searchPairs.push({id:id,"key":{"value":"null",  "label":" "},"value":"" });
	};
	
	    
	$scope.deletePair = function(pairId){
		$scope.searchPairs.splice(pairId,1);
	    };
	        
	$scope.open = function($event, name) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    $scope[name]=true;
	};	

	$scope.buildUrlParams=function(generate, forRow, startNoOnly){
		var url2="";
		for (var int = 0; int < $scope.searchPairs.length; int++) {
			if ($scope.searchPairs[int].value!=="") {
				var value=$scope.searchPairs[int].value;
				if (value.code) {
					value=value.code;
				}
				else if (value._id) {
					value=value._id;
				}
				url2= url2 + "&" + $scope.searchPairs[int].key.value + "=" + value;				
			};
		}
		for (key in $scope.printQuery) {
			url2=url2 + "&" +key+"="+$scope.printQuery[key];
		}
		if (generate && forRow.startNoOnly) {
			url2=url2+"&_generateStartNoOnly=true";
			url2=url2+"&group="+forRow.group;
			if (typeof forRow.startNoAlg!=='undefined' && forRow.startNoAlg!=="") {
				url2=url2+"&_startNoAlg="+forRow.startNoAlg;
				if (typeof forRow.startAt!=='undefined' && forRow.startAt!=="") {
					url2=url2+"&_startAt="+forRow.startAt;
					
				} 
			} 
		} else if (generate) {
			url2=url2+"&_generate=true";
			url2=url2+"&_delay="+forRow.delay;
			url2=url2+"&_delta="+forRow.delta;
			url2=url2+"&group="+forRow.group;
			if (typeof forRow.checkGroup!=='undefined' && forRow.checkGroup!=="") {
				url2=url2+"&_checkGroup="+forRow.checkGroup;
			}
			if (typeof forRow.startNoAlg!=='undefined' && forRow.startNoAlg!=="") {
				url2=url2+"&_startNoAlg="+forRow.startNoAlg;
				if (typeof forRow.startAt!=='undefined' && forRow.startAt!=="") {
					url2=url2+"&_startAt="+forRow.startAt;
					
				} 
			} 
		} 
		url2=url2.replace("&", "?");
		return url2;
	}
	
	$scope.print=function() {
		var url2=$scope.buildUrlParams(false);
		var openUrl='/#/'+$scope.eventCode+'/'+$scope.competitionCode+'/'+$scope.runCode+'/startTimesPrint'+url2;
		$window.open(openUrl, '_blank');	// TODO dodaj sve prams i query parametre, pa u npr. rankingPrint koristi isti controler 
		// TODO rankingPrint.html je isti kao i rankings.html samo bez kerefeka tabova i gumbiju i slično i po potrebi prilagođavan za ispis 
		// nađi emajlove o teome kako treba izgledati ispis
	}

	$scope.save=function(editRow) {
		var method="POST";
		var urlForId=url;
		if (editRow._id) { // means old obejct
			method="PUT";
			urlForId=url+'/'+editRow._id;			
		} 
		$http({url:urlForId, method:method, data:editRow}).then(
					function (response){
						messageSrvc.growlIt("i300"); // TODO messageSrvc
						// delete $scope.editRow;
						$scope.load();
					},
					function (error) {
						messageSrvc.growlIt("i199",error.data.error); // TODO server will respons duplicate key
					});				
	}	
	
	$scope.openModalGenerate= function() {
	   	var modalInstance = $modal.open({
	   	    templateUrl: 'modalGenerate.html',
	   	 	controller: 'modalEditGenerateCtrl',
	   	      resolve: {
	   	        row: function () {
	   	        		$scope.editRow={};
	   	        		return $scope.editRow;
	   	        	}
	   	        } 
	   	    });

	   	modalInstance.result.then(function (result) {
			if (result=='OK') {
				if ($scope.editRow) { // paranoic check
					$scope.search(true, $scope.editRow);
				} 
			}
		}, function () { // dialog dissmised
			$scope.load();
		});
	};
	

	$scope.selected=[];
	$scope.select=function(id) {
		var index=$scope.selected.indexOf(id);
		if (index<0) {
			$scope.selected.push(id);
		} else {
			$scope.selected.splice(index,1);
		};
	}
		
	
	$scope.search=function(generate, forRow) {
		var url2=$scope.buildUrlParams(generate, forRow);
		url2=url+url2;
		$http.get(url2).then(
				function (response){
					if ($scope.onlyGenerated) {
						$scope.rows=underscore.filter(response.data, function(r){
							return (typeof r.genStartSec !=="undefined" && r.genStartSec>=0);
						});
					} else {
						$scope.rows=response.data;
					}
				},
				function (error) {
					console.error(error);
				});
	}

})