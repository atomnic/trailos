/**
 * 
 */

smipdMain.controller("usersSearchCtrl",function($rootScope, $scope, $http, $window, $location, $modal, growl, messageSrvc ) {
	// init, load from server
	var storageUrl=""; // test always loads data from node.js server
	var endPoint="/producta/users";
	var url=storageUrl+endPoint;	
	
	
				
	
	$scope.init=function() {
		$scope.load();
	}
	
	$(function() {
        $('.multiselect').multiselect();
    });
	
	$scope.load=function() {
		$http.get(url).then(
				function (response){
					$scope.users=response.data;			
				},
				function (error) {
					console.error(error);
				});		
	}
	
	
	$scope.remove = function(){ 
		$scope.users.splice(this.$index, 1); 
	}
	
	$(function() {
	    $( ".resizable tr th" ).resizable({
	      handles: 'e'
	    });
	  });
	
	
	$scope.getClass = function(path) {
		if ($location.path().indexOf(path) >= 0) {
	      return "active";
	    } else {
	      return "";
	    }
	}
	
	
	
	
	$scope.searchOptions=
		[ {"value":"null",  "label":" "},
		  {"value":"username",  "label":"Korisničko ime"},
		  {"value":"firstName", "label":"Ime"},
		  {"value":"lastName", "label":"Prezime"},
		  {"value":"email", "label":"E-Mail"},
		  {"value":"roles", "label":"Uloga"},
		];	

	$scope.emptyPair={"key":$scope.searchOptions[0], "value":""}; // key == value from searchOptions
	
	$scope.searchPairs =
		[ {id:1, "key":$scope.searchOptions[1], "value":""},
		 {id:2,"key":$scope.searchOptions[2], "value":""},
		 {id:3,"key":$scope.searchOptions[3], "value":""},
		 {id:4,"key":$scope.searchOptions[4], "value":""},
		 {id:5,"key":$scope.searchOptions[5], "value":""},
		];

	$scope.addPair = function() {
		var id=$scope.searchPairs.length+1;
		$scope.searchPairs.push({id:id,"key":{"value":"null",  "label":" "},"value":"" });
	};
	
	    
	$scope.deletePair = function(pairId){
		$scope.searchPairs.splice(pairId,1);
	    };
	        
	
	
	
	
	$scope.delete=function(id) {
		$http.delete(url+"/"+id).then(
				function (response){
					$scope.usersRows=response.data;
				},
				function (error) {
					console.error(error);
				});		
	}
	
	$scope.new=function() {
		$http.put(url).then(
				function (response){
					$scope.usersRows=response.data;
				},
				function (error) {
					console.error(error);
				});		
	}
	
	
	
	$scope.save=function() {
		var method="POST";
		var urlForId=url;
		if ($scope.editRow._id) { // means old obejct
			method="PUT";
			urlForId=url+'/'+$scope.editRow._id;			
		} 
		$http({url:urlForId, method:method, data:$scope.editRow}).then(
					function (response){
						growl.info("Korisnik ..."); // TODO messageSrvc
						delete $scope.editRow;
						$scope.load();
					},
					function (error) {
						growl.error(error); // TODO server will respons duplicate key
					});				
	}
	
	
	
	$scope.openModalUsers= function(templateUrl, forRow) {
	   	var modalInstance = $modal.open({
	   	      templateUrl: templateUrl,
	   	      controller: 'modalEditUserCtrl',
	   	      resolve: {
	   	        row: function () {
	   	        	if (forRow) {
	   	        		$scope.editRow=forRow;
	   	        		return $scope.editRow;
	   	        	}
	   	        	else {
	   	        		$scope.editRow={};
	   	        		return $scope.editRow;
	   	        	}
	   	        }
	   	      }
	   	    });

	   	modalInstance.result.then(function (result) {
			if (result=='OK') {
				if ($scope.editRow) { // paranoic check
					$scope.save();
				} 
			}
		}, function () { // dialog dissmised
			$scope.load();
		});
	};
	
	
	
	
	    
	   	  
	   	  
	   	  
	   	  
	   	
	
	
	$scope.selected=[];
	$scope.select=function(id) {
		var index=$scope.selected.indexOf(id);
		if (index<0) {
			$scope.selected.push(id);
		} else {
			$scope.selected.splice(index,1);
		};
	}
	
	
	
	
	$scope.search=function() {
		var url2="";
		for (var int = 0; int < $scope.searchPairs.length; int++) {
			if ($scope.searchPairs[int].value!=="") {
				url2= url2 + "&" + $scope.searchPairs[int].key.value + "=" + $scope.searchPairs[int].value;				
			};
		}
		url2=url2.replace("&", "?");
		url2=url+url2;
		$http.get(url2).then(
				function (response){
					$scope.users=response.data;
				},
				function (error) {
					console.error(error);
				});
	}

})