/**
 * 
 */

smipdMain.controller("modalEditGenerateCtrl", function ($scope, $modalInstance, $http, $document, row, underscore,  messageSrvc, dataSrvc) {
	
	$scope.editRow=row;

    dataSrvc.getStates("startGroups").then(
        function(response){
            $scope.startGroups=response.data;
        }, function(error){
            messageSrvc.growlIt("e103","start groups")
        }
    );

	$document.ready(function() {
    $('#formGenerateStartTimes').formValidation({
			framework: 'bootstrap',
	        	err: {
	            	container: 'tooltip'
	        	},
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
            fields: {
                group: {
                    validators: {
                        notEmpty: {
                            message: 'Group is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                } /*,
                delay: {
                    disabled:true,
                    validators: {
                        notEmpty: {
                            message: 'Delay is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                delta: {
                    disabled:true,
                    validators: {
                        notEmpty: {
                            message: 'Delta is mandatory'
                        }
                    }
                } */
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
            $scope.ok();
        });
});

        

// This thre functions are common to all modalas...	  
	$scope.open = function($event, name) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope[name]=true;
	};		

	$scope.validate = function () {
		$modalInstance.close('OK');
		return true;
	};
	
	$scope.ok = function () {
		$scope.validate();
	};

	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};

});