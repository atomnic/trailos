/**
 * 
 */

smipdMain.controller("modalEditCountryCtrl", function ($scope, $modalInstance, $http, $document, row, underscore,  messageSrvc ) {
	
	$scope.editRow=row;

    $scope.imageUrl='/pub/trailos/countrys/'+row._id+'/flagImg';

	$document.ready(function() {
    $('#countryEditForm').formValidation({
			framework: 'bootstrap',
	        	err: {
	            	container: 'tooltip'
	        	},
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'First name is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                iso2: {
                    validators: {
                        notEmpty: {
                            message: 'Last name is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                iso3: {
                    validators: {
                        notEmpty: {
                            message: 'Country is mandatory'   // TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
            $scope.ok();
        });
    });

        

	$scope.validate = function () {
		$modalInstance.close('OK');
		return true;
	};
	
	$scope.ok = function () {
		$scope.validate();
	};

	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};

});