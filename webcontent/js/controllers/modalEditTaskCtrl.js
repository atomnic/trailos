/**
 * 
 */

smipdMain.controller("modalEditTaskCtrl", function ($scope, $modalInstance, $http, $document, row, underscore,  messageSrvc, dataSrvc ) {
	
	$scope.editRow=row;

    $scope.taskTypes=[];
    $scope.taskStates=[];
    $scope.loadOptions=function(){
        dataSrvc.getStates('taskTypes').then(
            function(response){
                $scope.taskTypes=response.data;
            },
            function (error) {
                messageSrvc.growlIt("e103", "task types");
            }
        );
        dataSrvc.getStates('task').then(
            function(response){
                $scope.taskStates=response.data;
            },
            function (error) {
                messageSrvc.growlIt("e103", "task states");
            }
        )
    }

    $scope.loadOptions();


    $scope.getCategorys=function(value) {
        if (!value) return [];
        dataSrvc.getCollection2('categorys',{name:value+"*"},0,20).then(
            function(response){
                $scope.lastCategorys=response.data;
            }, function(error) {
                messageSrvc.growlIt("e103", 'categorys');            
            }
        );
    }

    $scope.addTimeControl=function(){
        if (!$scope.editRow.timeControls) {
            $scope.editRow.timeControls=[];
        }
        var cp={};
        cp.status='regular';
        cp.no=$scope.editRow.timeControls.length+1;
        $scope.editRow.timeControls.push(cp);
    }

    $scope.apply=function(){
        $scope.$apply();
    }

    $scope.removeTimeControl=function($index) {
        dataSrvc.deleteConfirm("Removig time contorl! Are you sure?", null, null, $scope.editRow.timeControls, $index, $scope.apply);
    }

	$document.ready(function() {
    $('#taskEditForm').formValidation({
			framework: 'bootstrap',
	        	err: {
	            	container: 'tooltip'
	        	},
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
            fields: {
                no: {
                    validators: {
                        notEmpty: {
                            message: 'Number is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                type: {
                    validators: {
                        notEmpty: {
                            message: 'Type is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'Status is mandatory'
                        }
                    }
                },
                solution: {
                    enabled:false,
                    validators: {
                        notEmpty: {
                            message: 'Solution is mandatory'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
            $scope.ok();
        });
});

        

// This thre functions are common to all modalas...	  
	$scope.open = function($event, name) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope[name]=true;
	};		
	
	$scope.ok = function () {
		$modalInstance.close('OK');
	};

	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};

});