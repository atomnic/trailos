/**
 * 
 */

smipdMain.controller("competitionSearchCtrl",function($route, $rootScope, $scope, $http, $window, $location, $modal, authenticationFactory, messageSrvc) {
	// init, load from server
	if ($route.current.params.eventCode) {
        $scope.eventCode = $route.current.params.eventCode;
    };

	var storageUrl=""; // test always loads data from node.js server
	var endPoint0='/trailos/events';
	var endPoint="/trailos/events/"+$scope.eventCode+"/competitions";
	var url=storageUrl+endPoint;	

	$scope.$parent.setHideMenu(false);
	$scope.$parent.setHideTools(false);

	$scope.init=function() {
		if (authenticationFactory.check()) {
			$scope.load();
		} else {
			$location.path("login");
		}
	}

	$scope.openCompetition=function(forRow) {
		var child={};
		child.id=forRow._id;
		child.category=forRow.name;
		child.url="/"+$scope.eventCode+"/"+forRow.code+"/runSearch";
		child.children=[];
		child.openIn="";
		$scope.$parent.selectedNode.children.push(child);
		$scope.$parent.selectedNode=child;
		$location.path(child.url);
	}
	
	$scope.load=function() {
		var url2=url; // TODO calculate the right date, taht will come in nex 30 days and older
		$http.get(url2).then(
			function(response) {
				$scope.rows=response.data;
				$scope.loadOptions();
			},
			function(error) {
				messageSrvc.growlIt('e103','competitions for event '+$scope.eventCode);
			}			
		);	
	}

	$scope.loadOptions=function(){
		$http.get('/trailos/types?domain=cmpStatus').then(
			function(response) {
				$scope.cmpStatus=response.data;
			},
			function(error) {
				messageSrvc.growlIt('e103','types for domain cmpStatus');
			}			
		);	
	}

	$scope.openModalCompetition= function(forRow) {
	   	var modalInstance = $modal.open({
	   	    templateUrl: 'modalCompetitionEdit.html',
	   	 	controller: 'modalEditCompetitionCtrl',
	   	      resolve: {
	   	        row: function () {
	   	        	if (forRow) {
	   	        		$scope.editRow=forRow;
	   	        		return $scope.editRow;
	   	        	}
	   	        	else {
	   	        		$scope.editRow={};
	   	        		return $scope.editRow;
	   	        	}
	   	        }
	   	      }
	   	    });

	   	modalInstance.result.then(function (result) {
			if (result=='OK') {
				if ($scope.editRow) { // paranoic check
					$scope.save();
				} 
			}
		}, function () { // dialog dissmised
			$scope.load();
		});
	};
	
	
	$scope.getClass = function(path) {
		if ($location.path().indexOf(path) >= 0) {
	      return "active";
	    } else {
	      return "";
	    }
	}
	
	$(function() {
	    $( ".resizable tr th" ).resizable({
	      handles: 'e'
	    });
	  });
	
	
	$scope.searchOptions=
		[ {"value":"null",  "label":" "},
		  {"value":"code",  "label":"Short name"},
		  {"value":"name", "label":"Name"}
		];	

	$scope.emptyPair={"key":$scope.searchOptions[0], "value":""}; // key == value from searchOptions
	
	$scope.searchPairs =
		[];

	$scope.addPair = function() {
		var id=$scope.searchPairs.length+1;
		$scope.searchPairs.push({id:id,"key":{"value":"null",  "label":" "},"value":"" });
	};
	
	    
	$scope.deletePair = function(pairId){
		$scope.searchPairs.splice(pairId,1);
	    };
	        
	
	$(function() {
		$( ".resizable tr th" ).resizable({
			handles: 'e'
		});
	  });
	
	$( "button[type=save]" ).button({icons: {primary: "ui-icon ui-icon-disk"}});
	$( "button[type=plus]" ).button({icons: {primary: "ui-icon ui-icon-plus"}}); 
	$( "button[type=search]" ).button({icons: {primary: "ui-icon ui-icon-search"}}); 
	$( "button[type=new]" ).button({icons: {primary: "ui-icon ui-icon-folder-open"}}); 
	$( "button[type=delete]" ).button({icons: {primary: "ui-icon ui-icon-trash"}});
	
	
	
	$scope.open = function($event, name) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    $scope[name]=true;
	};	
	

	$scope.save=function() {
		var method="POST";
		var urlForId=url;
		if ($scope.editRow._id) { // means old obejct
			method="PUT";
			urlForId=url+'/'+$scope.editRow._id;			
		} 
		$scope.editRow.event=$scope.eventId;
		$http({url:urlForId, method:method, data:$scope.editRow}).then(
					function (response){
						growl.info("Saved ..."); // TODO messageSrvc
						delete $scope.editRow;
						$scope.load();
					},
					function (error) {
						growl.error(error); // TODO server will respons duplicate key
					});				
	}	
	

	$scope.selected=[];
	$scope.select=function(id) {
		var index=$scope.selected.indexOf(id);
		if (index<0) {
			$scope.selected.push(id);
		} else {
			$scope.selected.splice(index,1);
		};
	}
		
	
	$scope.search=function() {
		// TODO put this in some service or factory
		var url2="";
		for (var int = 0; int < $scope.searchPairs.length; int++) {
			if ($scope.searchPairs[int].value!=="") {
				url2= url2 + "&" + $scope.searchPairs[int].key.value + "=" + $scope.searchPairs[int].value;				
			};
		}
		url2=url2.replace("&", "?");
		url2=url+url2;
		$http.get(url2).then(
				function (response){
					$scope.rows=response.data;
				},
				function (error) {
					console.error(error);
				});
	}

})