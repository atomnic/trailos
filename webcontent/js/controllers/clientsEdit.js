/**
 * 
 */


 



smipdMain.controller("clientsEdit",function($rootScope, $scope, $http, $window, $location, $route, $upload, $timeout, $modal, underscore, messageSrvc) {
	// init, load from server
	var storageUrl=""; // test always loads data from node.js server this should be part of some global config
	var endPoint="/producta/clients";  // 
	var url=storageUrl+endPoint;
	
	$scope.$parent.setHideMenu(true);
	$scope.$parent.setHideTools(true);

	if ($route.current.params.id) {
        $scope.id = $route.current.params.id;
      };
	
	
	$scope.editRow;

	
	$scope.load=function() {
		if ($scope.id) {
			var urlForId=url+'/'+$scope.id;
			$http.get(urlForId).then(
				function (response){
					$scope.editRow=response.data;
					$scope.loadOptions();	
				},
				function (error) {
					messageSrvc.growlIt("e100", "Učitavanje klijenta");
				}
			);		
	    }
	}
	

	$scope.loadOptions=function() {
		$http.get('/producta/categorys?domain=clientType').then(
			function(response) {
				$scope.selectClientTypes=response.data;
			},
			function(error) {
				messageSrvc.growlIt("e103","tipove osoba");
			}			
		);
		$http.get('/producta/clientTypes?restrict=true').then(
			function(response) {
				// add false to each possible client type
				$scope.allClientTypes=underscore.map(response.data, function(clientType) {clientType.value=false; return clientType;});
				// mark as true already assigned client types, 
				if ($scope.editRow.clientTypes) {
					for(var idx=0; idx<$scope.editRow.clientTypes.length; idx++) {
						var clientType=underscore.findWhere( $scope.allClientTypes, { '_id': $scope.editRow.clientTypes[idx] });
						if (clientType) {
							clientType.value=true;
						}
					}
				}
			},
				function(error) {
				messageSrvc.growlIt("e103","tipove klijenata");
			}			
		);		
	}
	
	$scope.init=function() {	
		$scope.load();		
		$window.document.title = $scope.editRow.caseNo;
	}
		
	$( "button[type=save]" ).button({icons: {primary: "ui-icon ui-icon-disk"}});
	$( "button[type=plus]" ).button({icons: {primary: "ui-icon ui-icon-plus"}}); 
	$( "button[type=search]" ).button({icons: {primary: "ui-icon ui-icon-search"}}); 
	$( "button[type=new]" ).button({icons: {primary: "ui-icon ui-icon-folder-open"}}); 
	$( "button[type=delete]" ).button({icons: {primary: "ui-icon ui-icon-trash"}});
		
       		    		    	
	$scope.getClass = function(path) {
		if ($location.path().indexOf(path) && $location.path().indexOf(path, $location.path().length - path.length) === -1) {
      		return ""
		} else {
		    return "active"
		}
	}
	
	$(function() {
	    $( ".resizable tr th" ).resizable({
	      handles: 'e'
	    });
	});
	
	
	$scope.save=function() {
		var method="POST";
		var urlForId=url;
		// reinit clienttypes
	  	$scope.editRow.clientTypes=[];
		var roles=underscore.where($scope.allClientTypes, {'value': true});
	  	$scope.editRow.clientTypes=underscore.pluck(roles, '_id');
		//
		if ($scope.id) { // means old obejct
			method="PUT";
			urlForId=url+'/'+$scope.id;			
		} 
		$http({url:urlForId, method:method, data:$scope.editRow}).then(
					function (response){
						console.info(saved);
					},
					function (error) {
						console.error(error);
					});				
	}
		  
	
	$scope.openModalDelete= function(templateUrl, forRow) {
   	 var modalInstance = $modal.open({
   	      templateUrl: templateUrl,
   	      controller: 'modalEditRowCtrl',
   	      resolve: {
   	        row: function () {
   	        	if (forRow) {
   	        		return forRow;
   	        	}
   	        	else {
   	        		$scope.newRow={};
   	        		return $scope.newRow;
   	        	}
   	        }
   	      }
   	    });

   	    modalInstance.result.then(function (result) {
   	      if (result=='OK') {
   	    	  if ($scope.newRow) {
   	    		  $scope.editRow.priorities.push($scope.newRow);
   	    		  $scope.save();
   	    		  delete $scope.newRow;
   	    	  } else {
   	    		  $scope.save();
   	    	  }
   	      }
   	    }, function () { // dialog dissmised
   	    	$scope.load();
   	    });
   	  };
	
	
   	$scope.openModalContact= function(templateUrl, forRow) {
   		$('<style>.modal-dialog { width: 1000px; }</style>').appendTo('body');
   		$(".modal-body").css("width","800px");
   		var modalInstance = $modal.open({
   	      templateUrl: templateUrl,
   	      controller: 'modalContactsSearchCtrl',
   	      resolve: {
   	        row: function () {
   	        	if (forRow) {
   	        		return forRow;
   	        	}
   	        	else {
   	        		$scope.newRow={};
   	        		return $scope.newRow;
   	        	}
   	        }
   	      }
   	    });

   	    modalInstance.result.then(function (result) {
   	      if (result=='OK') {
   	    	  if ($scope.newRow) {
   	    		  $scope.editRow.clients.push($scope.newRow);
   	    		  $scope.save();
   	    		  delete $scope.newRow;
   	    	  } else {
   	    		  $scope.save();
   	    	  }
   	      }
   	    }, function () { // dialog dissmised
   	    	$scope.load();
   	    });
   	  };
   	  

     	$scope.searchOptions=
    		[ {"value":"null",  "label":" "},
    		  {"value":"type",  "label":"tip klijenta"},
    		  {"value":"clientName", "label":"ime klijenta"},
    		  {"value":"companyName", "label":"tvrtka"},
    		  {"value":"department", "label":"odjel"},
    		  {"value":"person", "label":"osoba"},
    		  {"value":"reference", "label":"referenca"},
    		];	

    	$scope.emptyPair={"key":$scope.searchOptions[0], "value":""}; // key == value from searchOptions
    	
    	$scope.searchPairs =
    		[ {id:1, "key":$scope.searchOptions[1], "value":""},
    		 {id:2,"key":$scope.searchOptions[2], "value":""},
    		 {id:3,"key":$scope.searchOptions[3], "value":""},
    		 {id:4,"key":$scope.searchOptions[4], "value":""}
    		];

    	$scope.addPair = function() {
    		var id=$scope.searchPairs.length+1;
    		$scope.searchPairs.push({id:id,"key":{"value":"null",  "label":" "},"value":"" });
    	};
    	
    	    
    	$scope.deletePair = function(pairId){
    		$scope.searchPairs.splice(pairId,1);
    	    }; 
   	  
   	  
	
	 $(document).ready(function() {
		 $('#vrsta').multiselect();
		 $('#tip').multiselect();
		 $('#popust').multiselect();
		 $('#drzava').multiselect();
		 });
	
	
	 
	 
	 
	 $(document).on("click", ".alert3", function(e) {
		    bootbox.prompt("Unesite Admin lozinku:", function(result) {
		        if (result === "Admin") {
		        	$scope.deleteIt();
		        } else {
		        	alert("Neispravna lozinka!");
		        }
		    });
		});
	  
	 
	$scope.emailIt=function() {
		 growl.info("Funkcija generiranje e-maila još nije implementirana!");
	};
		
	$scope.copy=function() {
		growl.warning("Funkcija pametnog kopiranja još nije implementirana!");
	};	
	
	$scope.paste=function() {
		growl.error("Funkcija pametnog lijepljenja još nije implementirana!");
	};	
	
	$scope.exportIt=function() {
		growl.info("Funkcija exporta još nije implementirana!");
	};	
	
	$scope.deleteIt=function() {
		growl.warning("Funkcija brisanje je napravljena, ali nije uključena u sučelje!");
	};	
	
	
})