/**
 * 
 */

var modalEditRowCtrl=smipdMain.controller("modalEditRowCtrl", function ($scope, $modalInstance, row ) {
	
	$scope.editRow=row;

// This thre functions are common to all modalas...	  
	$scope.open = function($event, name) {
		    $event.preventDefault();
		    $event.stopPropagation();
		    $scope[name]=true;
		  };		
	
	$scope.ok = function () {
	  	$modalInstance.close('OK');
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});