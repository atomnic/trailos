/**
 * 
 */

var modalEditRowCtrl=smipdMain.controller("modalEditSolutionCtrl", function ($scope, $modalInstance, $http, $document, run, cmpCmr, underscore,  messageSrvc, dataSrvc) {
	
	$scope.editRow={};
    $scope.run=run;
    $scope.cmpCmr=cmpCmr;

    var storageUrl=""; // test always loads data from node.js server
    var endPoint="/trailos/runs/"+$scope.run._id+"/competitors";
    var url=storageUrl+endPoint;

    if ($scope.run.type==="PreO") {
        $scope.sliceIt=11;
    } else {
        $scope.sliceIt=4;
    }

    $scope.initSolutions=function() {
        $http.get("/trailos/runs/"+$scope.editRow.run._id+"/tasks?categorys="+$scope.editRow.category._id).then(
            function(response) {
                var noTeam=0;
                // TODO DE HARDCODE team event stage
                if ($scope.editRow.run.code==="Day2") noTeam=1;
                if ($scope.cmpCmr.competitor.team) noTeam=0;
                if (response.data.length-noTeam>$scope.editRow.solutions.length) {
                    $scope.editRow.solutions=underscore.map(response.data, function(task) {
                        var obj={};
                        obj.task=task;
                        if (task.type==="ST") {
                            obj.tcSolutions=[];
                            for(var idx=0; idx<task.timeControls.length; idx++) {
                                var tcs={};
                                tcs.no=task.timeControls[idx].no;
                                obj.tcSolutions.push(tcs);
                            }
                        }
                        return obj;
                    });
                    if (noTeam===1) {
                        $scope.editRow.solutions=underscore.filter($scope.editRow.solutions, function(r){
                            return (typeof r.task.team===undefined || !r.task.team);
                        })
                    } 
                }
            },
            function(error) {
                if (error.status===403) {
                    messageSrvc.growlIt('e198');
                } else {
                    messageSrvc.growlIt("e199","tasks for stage "+$scope.editRow.run.code);
                    console.log(error);
                }
            }
        );
    }

    $scope.init=function(){
        $scope.load();
    }

    $scope.load=function() {
        var url2=url; 
        $http.get(url2+"/"+$scope.cmpCmr.competitor._id).then(
            function(response) {
                if (response.data.length==1) {
                    $scope.editRow=response.data[0];
                    //if ($scope.editRow.solutions.length<0) {
                    $scope.initSolutions();
                    //}
                    if (!$scope.editRow.breaks || $scope.editRow.breaks.length==0) {
                        $scope.editRow.breaks=[];
                        var numOfBreaks=$scope.editRow.run.breaks;
                        if (!numOfBreaks) numOfBreaks=0;
                        for (var idx=0;idx<numOfBreaks+1; idx++) {
                            $scope.editRow.breaks.push({no:idx+1, startHrs:0,startMin:0,startSec:0,finishHrs:0,finishMin:0,finishSec:0});
                        }
                    }
                } else {
                    messageSrvc.growlIt("e103", 'competitor for stage '+$scope.editRow.run.code)
                }
            },
            function(error) {
                messageSrvc.growlIt('e103','competitor for stage '+$scope.editRow.run.code);
            }           
        );  
    }

    $scope.timesEneterd=false;
    $scope.numbers="0123456789";
    $scope.timesEnter=function($event){
        var pressedChar=String.fromCharCode($event.keyCode);
        if ($scope.numbers.indexOf(pressedChar)>=0) {
            $scope.timesEneterd=true;
        }
    }

    $scope.save=function(){
        var method="POST";
        var urlForId=url;
        if ($scope.editRow._id) { // means old obejct
            method="PUT";
            urlForId=url+'/'+$scope.editRow._id;   
            //
            if ($scope.timesEneterd) {
                urlForId=urlForId+"/timesSolutions";
            } else {
                urlForId=urlForId+"/solutions";
            }         
        } 
        $http({url:urlForId, method:method, data:$scope.editRow}).then(
                    function (response){
                        messageSrvc.growlIt("i300"); // TODO messageSrvc
                        // $scope.load();
                    },
                    function (error) {
                       messageSrvc.growlIt(error.data.error); // TODO server will respons duplicate key
                    });             
    }

    $scope.enterTime=function($event, row) {
        if ($scope.numbers.indexOf(pressedChar)>=0) {
            if (parseInt(row.time)>9) $.tabNext();
        }
    }

    $scope.characters="aAbBcCdDeEfFzZxX";
    
    $scope.enterSolution=function($event, $index, row, tc) {
        var pressedChar=String.fromCharCode($event.keyCode); 
        if (  $scope.characters.indexOf(pressedChar)>=0){
            if (tc) {
                tc.solution=pressedChar.toUpperCase();
            } else if (row) {
                row.solution=pressedChar.toUpperCase();
            }
            //$event.preventDefault();
            $.tabNext();
            // $('input,select,textarea')[$('input,select,textarea').index($event.target)+1].focus();
            /*
            $event.stopPropagation();
            if (tc)
            $($event.target).parent().parent().next(".row").find("input").focus();
            if (row)
                $($event.target).parent().parent().parent().parent().next(".row").find("input").focus();*/
            //$.tabNext();
        } 
        else if ($event.keyCode!=9) {
            row.solution="";
            messageSrvc.growlIt("w100");
        }
    }
    
    
	$document.ready(function() {
    $('#solutionsEditForm').formValidation({
		framework: 'bootstrap',
	       	err: {
	         	container: 'tooltip'
        	},
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        live:"disabled",
            fields: {
                /*code: {
                    validators: {
                        notEmpty: {
                            message: 'Short name is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Name is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                startDate: {
                    validators: {
                        notEmpty: {
                            message: 'Start date is mandatory'
                        }
                    }
                }*/
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            // for (var idx=0; idx<) // TODO do some validation if all tasks are entered if not warning and X for not entered
            // TODO check if times from break and from statiosn are entered to
            e.preventDefault();
            $scope.ok();
        }); 
    });

        

// This thre functions are common to all modalas...	  
	$scope.open = function($event, name) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope[name]=true;
	};		


	
	$scope.ok = function () {
        $scope.save(); 
		$modalInstance.close('OK');
	};

	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};

});