/**
 * 
 */

smipdMain.controller("modalEditClubCtrl", function ($scope, $modalInstance, $http, $document, row, underscore,  messageSrvc ) {
	
	$scope.editRow=row;

    $scope.loadOptions=function(){
        $http.get('/trailos/types?domain=cmpStatus').then(
            function(response) {
                $scope.cmpStatus=response.data;
            },
            function(error) {
                messageSrvc.growlIt('e103','types for domain cmpStatus');
            }           
        );  
    }


    $scope.getCountrys=function($viewValue){
        if (!$viewValue) {
            return [];
        }
        var filter=$viewValue+"*";
        return $http.get('/trailos/countrys?nameLocal='+filter+'&offset=0&limit=15').then(
            function(response) {
                return response.data;
            },
            function(error) {
                messageSrvc.growlIt('e103','countrys');
            }           
        );  
    }
    



    $scope.loadOptions();

	$document.ready(function() {
    $('#clubEditForm').formValidation({
			framework: 'bootstrap',
	        	err: {
	            	container: 'tooltip'
	        	},
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
            fields: {
                code: {
                    validators: {
                        notEmpty: {
                            message: 'Code/abbreviation name is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Name is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                country: {
                    validators: {
                        notEmpty: {
                            message: 'Country is mandatory'   // TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
            $scope.ok();
        });
    });

        

// This thre functions are common to all modalas...	  
	$scope.open = function($event, name) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope[name]=true;
	};		

	$scope.validate = function () {
		$modalInstance.close('OK');
		return true;
	};
	
	$scope.ok = function () {
		$scope.validate();
	};

	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};

});