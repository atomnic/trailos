/**
 * 
 */

smipdMain.controller("modalEditCompetitorCtrl", function ($scope, $modalInstance, $http, $document, row, underscore,  messageSrvc, dataSrvc ) {
	
	$scope.editRow=row;

    $scope.loadOptions=function(){
        dataSrvc.getStates('gender').then(
            function(response) {
                $scope.statesGender=response.data;
            },
            function(error) {
                messageSrvc.growlIt('e103','genders');
            }           
        );  
        dataSrvc.getStates('competitorTypes').then(
            function(response) {
                $scope.statesCompetitorType=response.data;
            },
            function(error) {
                messageSrvc.growlIt('e103','competitor types');
            }           
        );  
    }

    $scope.getCountrys=function($viewValue){
        if (!$viewValue) {
            return [];
        }
        return dataSrvc.getCollection2('countrys',{nameLocal:$viewValue},0,25,{nameLocal:{wildCard:true}}).then(
            function(response) {
                return response.data;
            },
            function(error) {
                messageSrvc.growlIt('e103','countrys');
            }           
        );  
    }

    $scope.getTeams=function($viewValue){
        if (!$viewValue) {
            return [];
        }
        return dataSrvc.getCollection2('competitors',{ type:"Team" ,fullName:$viewValue},0,25,{fullName:{wildCard:true}}).then(
            function(response) {
                return response.data;
            },
            function(error) {
                messageSrvc.growlIt('e103','countrys');
            }           
        );  
    }


    $scope.loadOptions();

	$document.ready(function() {
    $('#competitorEditForm').formValidation({
			framework: 'bootstrap',
	        	err: {
	            	container: 'tooltip'
	        	},
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
            fields: {
                firstName: {
                    validators: {
                        notEmpty: {
                            message: 'First name is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                country: {
                    validators: {
                        notEmpty: {
                            message: 'Country is mandatory'   // TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                sex: {
                    validators: {
                        notEmpty: {
                            message: 'Country is mandatory'   // TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
            $scope.editRow.fullName=$scope.editRow.firstName;
            if (typeof $scope.editRow.lastName!=="undefined" && $scope.editRow.lastName!=="") $scope.editRow.fullName=$scope.editRow.fullName+" "+$scope.editRow.lastName;
            $scope.ok();
        });
});

        

// This thre functions are common to all modalas...	  
	$scope.open = function($event, name) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope[name]=true;
	};		

	$scope.validate = function () {
		$modalInstance.close('OK');
		return true;
	};
	
	$scope.ok = function () {
		$scope.validate();
	};

	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};

});