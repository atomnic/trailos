/**
 * 
 */

smipdMain.controller("eventSearchCtrl",function($rootScope, $scope, $http, $window, $location, $modal, authenticationFactory) {
	// init, load from server
	var storageUrl=""; // test always loads data from node.js server
	var endPoint="/trailos/events";
	var url=storageUrl+endPoint;	

	$scope.$parent.setHideMenu(false);
	$scope.$parent.setHideTools(false);

	$scope.init=function() {
		if (authenticationFactory.check()) {
			$scope.load();
		} else {
			$location.path("login");
		}
	}
	
	$scope.load=function() {
		var url2=url; // TODO calculate the right date, taht will come in nex 30 days and older
		$http.get(url2).then(
				function(response) {
					$scope.rows=response.data;
				},
				function(error) {
					console.error(error);
				}			
			);		
	}

	$scope.openEvent=function(forRow) {
		var child={};
		child.id=forRow._id;
		child.category=forRow.name;
		child.url="/"+forRow.code+"/competitionSearch";
		child.children=[];
		child.openIn="";
		$scope.$parent.selectedNode.children.push(child);
		$scope.$parent.selectedNode=child;
		$location.path(child.url);
	}

	$scope.openModalEvent= function(forRow) {
	   	var modalInstance = $modal.open({
	   	    templateUrl: 'modalEventEdit.html',
	   	 	controller: 'modalEditEventCtrl',
	   	      resolve: {
	   	        row: function () {
	   	        	if (forRow) {
	   	        		$scope.editRow=forRow;
	   	        		return $scope.editRow;
	   	        	}
	   	        	else {
	   	        		$scope.editRow={};
	   	        		return $scope.editRow;
	   	        	}
	   	        }
	   	      }
	   	    });

	   	modalInstance.result.then(function (result) {
			if (result=='OK') {
				if ($scope.editRow) { // paranoic check
					$scope.save();
				} 
			}
		}, function () { // dialog dissmised
			$scope.load();
		});
	};
	
	
	$scope.getClass = function(path) {
		if ($location.path().indexOf(path) >= 0) {
	      return "active";
	    } else {
	      return "";
	    }
	}
	
	$(function() {
	    $( ".resizable tr th" ).resizable({
	      handles: 'e'
	    });
	  });
	
	
	$scope.searchOptions=
		[ {"value":"null",  "label":" "},
		  {"value":"code",  "label":"Short name"},
		  {"value":"name", "label":"Name"}
		];	

	$scope.emptyPair={"key":$scope.searchOptions[0], "value":""}; // key == value from searchOptions
	
	$scope.searchPairs =
		[];

	$scope.addPair = function() {
		var id=$scope.searchPairs.length+1;
		$scope.searchPairs.push({id:id,"key":{"value":"null",  "label":" "},"value":"" });
	};
	
	    
	$scope.deletePair = function(pairId){
		$scope.searchPairs.splice(pairId,1);
	    };
	        
	
	$(function() {
		$( ".resizable tr th" ).resizable({
			handles: 'e'
		});
	  });
	

	$scope.open = function($event, name) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    $scope[name]=true;
	};	
	

	$scope.save=function() {
		var method="POST";
		var urlForId=url;
		if ($scope.editRow._id) { // means old obejct
			method="PUT";
			urlForId=url+'/'+$scope.editRow._id;			
		} 
		$http({url:urlForId, method:method, data:$scope.editRow}).then(
					function (response){
						growl.info("Saved ..."); // TODO messageSrvc
						delete $scope.editRow;
						$scope.load();
					},
					function (error) {
						growl.error(error); // TODO server will respons duplicate key
					});				
	}	
	

	$scope.selected=[];
	$scope.select=function(id) {
		var index=$scope.selected.indexOf(id);
		if (index<0) {
			$scope.selected.push(id);
		} else {
			$scope.selected.splice(index,1);
		};
	}
		
	
	
	$scope.pathUrl = function(path) {
		if (path === "OSOBE") {
			$window.open('/#/clientsSearch/');
	    }
		if (path === "INSTRUKCIJE") {
			$window.open('/#/operationsSearch/');
	    }
	}
	
	
	$scope.search=function() {
		var url2="";
		for (var int = 0; int < $scope.searchPairs.length; int++) {
			if ($scope.searchPairs[int].value!=="") {
				url2= url2 + "&" + $scope.searchPairs[int].key.value + "=" + $scope.searchPairs[int].value;				
			};
		}
		url2=url2.replace("&", "?");
		url2=url+url2;
		$http.get(url2).then(
				function (response){
					$scope.operations=response.data;
				},
				function (error) {
					console.error(error);
				});
	}

})