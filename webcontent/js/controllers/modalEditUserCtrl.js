/**
 * 
 */

var modalEditRowCtrl=smipdMain.controller("modalEditUserCtrl", function ($scope, $modalInstance, $http, $document, row, underscore,  messageSrvc ) {
	
	$scope.editRow=row;
	$scope.password2=row.password;

	$scope.allRoles=[{'key':'admin', 'label':"Administrator", 'value':false}, {'key':'user', 'label':"Korisnik", 'value':false}];

	if ($scope.editRow.roles) {
		for(var idx=0; idx<$scope.editRow.roles.length; idx++) {
			var role=underscore.findWhere( $scope.allRoles, { 'key': $scope.editRow.roles[idx] });
			if (role) {
				role.value=true;
			}
		}
	}
	
	$document.ready(function() {
    $('#userEditForm').formValidation({
			framework: 'bootstrap',
	        	err: {
	            	container: 'tooltip'
	        	},
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
            fields: {
                username: {
                    validators: {
                        notEmpty: {
                            message: 'Korisničko ime je obavezno'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'Korisničko ime je obavezno'	// TODO rethink, use messageSrvc, generic reuqired field
                        },
                        stringLength: {
                        	min: 8,
                            max: 32,
                            message: 'The password must be less than 32 characters long but longer than 8'
                        }
                    }
                },
                firstName: {
                    validators: {
                        notEmpty: {
                            message: 'Ime je obavezno'
                        }
                    }
                },
                lastName: {
                    validators: {
                        notEmpty: {
                            message: 'Prezime je obavezno'
                        }
                    }
                }
            }
        })
});

        

// This thre functions are common to all modalas...	  
	$scope.open = function($event, name) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope[name]=true;
	};		

	$scope.validate = function () {
	  	if ($scope.editRow.password !== $scope.password2) {
	  		// growl.info(messageSrvc.getMessage("e101"));
	  		messageSrvc.growlIt("e101");
	  		return false;
	  	} else {
	  		if ($scope.editRow._id) {
	  			// fine we are updating
	  			$scope.editRow.roles=[];
	  			var roles=underscore.where($scope.allRoles, {'value': true});
	  			$scope.editRow.roles=underscore.pluck(roles, 'key');
	  			$modalInstance.close('OK');
	  			return true;
	  		} else {
		  		$http.get('/producta/users?username='+$scope.editRow.username).then(
		  			function(response) {
		  				if (response.data.length==0) {
		  					// username doesn't exists, fine continue to save
		  					// first prepare roles
	  						$scope.editRow.roles=[];
	  						var roles=underscore.where($scope.allRoles, {'value': true});
	  						$scope.editRow.roles=underscore.pluck(roles, 'key');
	  						// finito
	  						$modalInstance.close('OK');
		  					return true;
		  				} else {
		  					messageSrvc.growlIt("e102", $scope.editRow.username);
		  					return false;
		  				}
		  			},
		  			function(error) {
		  				messageSrvc.growlIt("e199", error);	// TODO messageSrvc for unexpected errors from server
		  				return false;
		  			}
		  		)
	  		}
	  	}
	};
	
	$scope.ok = function () {
		$scope.validate();
	};

	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};

});