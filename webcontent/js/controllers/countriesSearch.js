/**
 * 
 */

smipdMain.controller("countriesSearch",function($rootScope, $scope, $http, $window, $location, $modal) {
	// init, load from server
	var storageUrl=""; // test always loads data from node.js server
	var endPoint="/trailos/countrys";
	var url=storageUrl+endPoint;	
	
	$scope.paging={"offset":0, "limit":20};

	$scope.init=function() {
		$scope.load();
	}
	
	$scope.load=function() {
		$http.get(url+"?offset="+$scope.paging.offset+"&"+"limit="+$scope.paging.limit).then(
				function (response){
					$scope.countrys=response.data;	
					$scope.$parent.parseContentRange(response.headers('Content-Range'), $scope.paging);		
				},
				function (error) {
					console.error(error);
				});		
	}
	
	
	$scope.remove = function(){ 
		$scope.countrys.splice(this.$index, 1); 
	}
	
	$(function() {
	    $( ".resizable tr th" ).resizable({
	      handles: 'e'
	    });
	  });
	
	
	$scope.getClass = function(path) {
		if ($location.path().indexOf(path) >= 0) {
	      return "active";
	    } else {
	      return "";
	    }
	}
	
	
	
	
	$scope.searchOptions=
		[ 
		  {"value":"nameInt",  "label":"name"},
		  {"value":"nameLocal", "label":"local name"},
		  {"value":"localCode", "label":"IOF code"},
		  {"value":"iso3", "label":"ISO3"}
		];	

	$scope.emptyPair={"key":$scope.searchOptions[0], "value":""}; // key == value from searchOptions
	
	$scope.searchPairs =
		[ {id:0, "key":$scope.searchOptions[0], "value":""},
		 {id:1,"key":$scope.searchOptions[1], "value":""},
		 {id:2,"key":$scope.searchOptions[2], "value":""},
		 {id:3,"key":$scope.searchOptions[3], "value":""}
		];

	$scope.addPair = function() {
		var id=$scope.searchPairs.length+1;
		$scope.searchPairs.push({id:id,"key":{"value":"null",  "label":" "},"value":"" });
	};
	
	    
	$scope.deletePair = function(pairId){
		$scope.searchPairs.splice(pairId,1);
	    };
	        
	
	$(function() {
	    $( "#table-css-border-1 tr th" ).resizable({
	      handles: 'e'
	    });
	  });
	
	$scope.delete=function(id) {
		$http.delete(url+"/"+id).then(
				function (response){
					$scope.countrysRows=response.data;
				},
				function (error) {
					console.error(error);
				});		
	}
	
	$scope.new=function() {
		$http.put(url).then(
				function (response){
					$scope.countrysRows=response.data;
				},
				function (error) {
					console.error(error);
				});		
	}
	
	
	$scope.save=function() {
		var method="POST";
		var urlForId=url;
		if ($scope.editRow._id) { // means old obejct
			method="PUT";
			urlForId=url+'/'+$scope.editRow._id;			
		} 
		$scope.editRow.event=$scope.eventId;
		$http({url:urlForId, method:method, data:$scope.editRow}).then(
					function (response){
						messageSrvc.silentInfo("Pospremljeno ..."); // TODO messageSrvc
						delete $scope.editRow;
						$scope.load();
					},
					function (error) {
						messageSrvc.growlIt("e104"); // TODO server will respons duplicate key
					});				
	}	
	
	
	
	$scope.openModalCountries= function(templateUrl, forRow) {
	   	 var modalInstance = $modal.open({
	   	      templateUrl: templateUrl,
	   	      controller: 'modalEditCountryCtrl',
	   	      resolve: {
		   	      row: function () {
		   	        	if (forRow) {
		   	        		$scope.editRow=forRow;
		   	        		return $scope.editRow;
		   	        	}
		   	        	else {
		   	        		$scope.editRow={};
		   	        		return $scope.editRow;
		   	        	}
		   	        }
	   	      }
	   	    });

	   	    modalInstance.result.then(function (result) {
	   	      if (result=='OK') {
	   	    	  if ($scope.newRow) {
	   	    		  $scope.countrys.push($scope.newRow);
	   	    		  $scope.save();
	   	    		  delete $scope.newRow;
	   	    	  } else {
	   	    		  $scope.save();
	   	    	  }
	   	      }
	   	    }, function () { // dialog dissmised
	   	    	$scope.load();
	   	    });
	   	  };
	
	
	
	
	
	
	
	$scope.selected=[];
	$scope.select=function(id) {
		var index=$scope.selected.indexOf(id);
		if (index<0) {
			$scope.selected.push(id);
		} else {
			$scope.selected.splice(index,1);
		};
	}
		
	$scope.newSubject=function() {
		$window.open('/#/countriesEdit/', '_blank');
	}
	
	// #/subjectsEdit/{{row.id}}
	$scope.openSubject=function(rowId) {
		$window.open('/#/countriesEdit/'+rowId, '_blank');
	}
	
	$scope.pathUrl = function(path) {
		if (path === "OSOBE") {
			$window.open('/#/clientsSearch/');
	    }
		if (path === "INSTRUKCIJE") {
			$window.open('/#/operationsSearch/');
	    }
	}
	
	
	$scope.search=function() {
		var url2="";
		url2="?offset="+$scope.paging.offset+"&"+"limit="+$scope.paging.limit;
		for (var int = 0; int < $scope.searchPairs.length; int++) {
			if ($scope.searchPairs[int].value!=="") {
				url2= url2 + "&" + $scope.searchPairs[int].key.value + "=" + $scope.searchPairs[int].value;				
			};
		}
		//url2=url2.replace("&", "?");
		url2=url+url2;
		$http.get(url2).then(
				function (response){
					$scope.countrys=response.data;
					$scope.$parent.parseContentRange(response.headers('Content-Range'), $scope.paging);
				},
				function (error) {
					console.error(error);
				});
	}

})