/**
 * 
 */
smipdMain.controller("loginCtrl",function($rootScope, $scope, $http, $window, $location, $route, authenticationFactory, userAuthFactory, messageSrvc) {
	// init, load from server
	var storageUrl=""; // test always loads data from node.js server this should be part of some global config
	var endPoint="/trailos/users";  // 
	var url=storageUrl+endPoint;
	
	$scope.$parent.setHideMenu(true);
	$scope.$parent.setHideTools(true);

	$scope.login = function() {
		var username = $scope.username,
		password = $scope.password;
		if (username !== undefined && password !== undefined) {
			userAuthFactory.login(username, password).then(function(){
					$location.path("/");
				}
				,function(status) {
					alert('Oops something went wrong!');
				});
		} else {
			alert('Invalid credentials');
		}
	};
})