/**
 * 
 */

smipdMain.controller("modalRunCompetitionCtrl", function ($scope, $modalInstance, $http, $document, row, underscore,  messageSrvc, dataSrvc ) {
	
	$scope.editRow=row;

    $scope.loadOptions=function(){
        // $http.get('/trailos/types?domain=cmpStatus').then(
        dataSrvc.getStates('event').then(
            function(response) {
                $scope.cmpStatus=response.data;
            },
            function(error) {
                messageSrvc.growlIt('e103','types for domain cmpStatus');
            }           
        );  
    }

    $scope.loadOptions();

	$document.ready(function() {
    $('#runEditForm').formValidation({
			framework: 'bootstrap',
	        	err: {
	            	container: 'tooltip'
	        	},
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
            fields: {
                code: {
                    validators: {
                        notEmpty: {
                            message: 'Short name is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Name is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                startDate: {
                    validators: {
                        notEmpty: {
                            message: 'Start date is mandatory'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
            $scope.ok();
        });
});

        

// This thre functions are common to all modalas...	  
	$scope.open = function($event, name) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope[name]=true;
	};		

	$scope.validate = function () {
		$modalInstance.close('OK');
		return true;
	};
	
	$scope.ok = function () {
		$scope.validate();
	};

	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};

});