/**
 * 
 */

smipdMain.controller("subjectsEdit",function($rootScope, $scope, $http, $window, $location, $route, $upload, $timeout, growl, $modal, $log) {
	// init, load from server
	var storageUrl=""; // test always loads data from node.js server this should be part of some global config
	var endPoint="/producta/subjects";  // 
	var url=storageUrl+endPoint;

	$scope.$parent.setHideMenu(true);
	$scope.$parent.setHideTools(true);
	
	$scope.subjectTypeAlias;
	if ($route.current.params.id) {
        $scope.id = $route.current.params.id;      
      };	
	if ($route.current.loadedTemplateUrl.indexOf("subjects")>-1) {
		$scope.subjectTypeAlias='P';
	}  else if ($route.current.loadedTemplateUrl.indexOf("trademarks")-1) {
		$scope.subjectTypeAlias='T';
	} else if ($route.current.loadedTemplateUrl.indexOf("models")-1) {
		$scope.subjectTypeAlias='M';
	} 
	
	
	$(function() {
	    $( ".resizable tr th" ).resizable({
	      handles: 'e'
	    });
	  });
	
	
	$scope.init=function() {
		$scope.load();
		$window.document.title = $scope.editRow.caseNo;
		if ($route.current.loadedTemplateUrl.indexOf("subjectsEdit")>-1) {
			$scope.lockedUnlockedLabel="Otključaj";
			$scope.locked=false;
		} else {
			$scope.lockedUnlockedLabel="Zaključaj";
			$scope.locked=true;
		}		
	}	
	
	$scope.currentDate=new Date();
		$scope.deleteNode = function(){
	        var index = this.parentNo - 1;
	        if (this.childNo===undefined) {
	        	this.childNo="";
	        };
	        if (this.childNo.length>=0) {
	        	branch=$scope.dataForTheTree[index].children;
	        	branch.splice(this.childNo-1,1);
	        };
	        if (this.childNo.length===0){
	        	$scope.dataForTheTree.splice(this.parentNo-1,1);
	        };
	        
	    }
		
		
		
		$scope.lockedUnlockedLabel={};
		
		$scope.lockForm=function(){
	        if ($scope.locked===true) {
	        	$scope.lockedUnlockedLabel="Otključaj";
	        	$scope.locked=false;
	        } else if ($scope.locked===false) {
	        	$scope.lockedUnlockedLabel="Zaključaj";
	        	$scope.locked=true;
	        };
		}
		
		
		
		$( "button[type=save]" ).button({icons: {primary: "ui-icon ui-icon-disk"}});
		$( "button[type=plus]" ).button({icons: {primary: "ui-icon ui-icon-plus"}}); 
		$( "button[type=search]" ).button({icons: {primary: "ui-icon ui-icon-search"}}); 
		$( "button[type=new]" ).button({icons: {primary: "ui-icon ui-icon-folder-open"}}); 
		$( "button[type=delete]" ).button({icons: {primary: "ui-icon ui-icon-trash"}});
		$( "button[type=email]" ).button({icons: {primary: "ui-icon ui-icon-mail-open"}});
		$( "button[type=copy]" ).button({icons: {primary: "ui-icon  ui-icon-copy"}});
		$( "button[type=paste]" ).button({icons: {primary: "ui-icon  ui-icon-arrowreturnthick-1-s"}});
		$( "button[type=export]" ).button({icons: {primary: "ui-icon ui-icon-arrowthick-1-e"}});
		
		 $scope.addParent = function() {
			 var no=$scope.dataForTheTree.length+1;
			 $scope.dataForTheTree.push({ "id":no, "category" : "TEST", "children" : [] });
		    };
		  
		 
		$scope.addChild = function(leaf) {
			
			 var index = $scope.dataForTheTree.indexOf(leaf);
			 var no=$scope.dataForTheTree[index].children.length+1;
			 if(index!=-1){
		        $scope.dataForTheTree[index].children.push({ "id":no, "category" : "TEST", "children" : [] });
			 };
		 }
		          		    		    	
		    
		$scope.getClass = function(path) {
			if ($location.path().indexOf(path) && $location.path().indexOf(path, $location.path().length - path.length) === -1) {
		      return ""
		    } else {
		      return "active"
		    }
		}
		
		
		$scope.tools=function(selectedNode) {
			
			if (selectedNode == "PREDMETI") {
	    		window.location.href = '#/search';
	    	}
	    	if (selectedNode == "PATENTI") {
	    		window.location.href = '#/search';
	    		$location.search(temp,"temp");
	    	}
	    	if (selectedNode == "ŽIGOVI") {
	    		window.location.href = '#/search';
	    	}
	    	if (selectedNode == "OSOBE") {
	    		window.location.href = '#/search';
	    	}
		}
			
		$scope.availableOperations = ["test1","test2"];
		/**
		$scope.availableOperations = $scope.operation.operationId.name;
		**/
		
		$scope.complete=function(what, collection,query){
			// based ond what search collection (like*', eventualy restrict selection with query  
			$( "#operationsList" ).autocomplete({
				source: $scope.availableOperations
		        });
			}
	$scope.editRow={};	
	$scope.load=function() {
		if ($scope.id) {
			var urlForId=url+'/'+$scope.id;
			$http.get(urlForId).then(
					function (response){
						$scope.editRow=response.data;
						
					},
					function (error) {
						console.error(error);
					});		
	     } else {
	     	var urlForId=url+'/'+$scope.id;
			$http.get('/producta/subjectTypes?alias='+$scope.subjectTypeAlias).then(
					function (response){
						$scope.editRow.subjectTypeId=response.data[0]._id;
						
					},
					function (error) {
						console.error(error);
					});	
	     }
	}
	
	
	
	
	$scope.clientsRow=[];
	
	$scope.listLabels=function(appl){
		$scope.clientsRow = appl;
		}
	
	
	
	
	
	
	
	$scope.save=function() {
		var method="POST";
		var urlForId=url;
		if ($scope.id) { // means old obejct
			method="PUT";
			urlForId=url+'/'+$scope.id;			
		} 
		$http({url:urlForId, method:method, data:$scope.editRow}).then(
					function (response){
						console.info(saved);
						$scope.editRow=response.data;
					},
					function (error) {
						console.error(error);
					});				
	}
		  
	$scope.deleteImage=function(image) {
		var method="DELETE";
		var urlForId=url;
		urlForId=url+'/'+$scope.id+'/images/'+image._id;			 
		$http({url:urlForId, method:method}).then(
			function (response){
				// $scope.editRow.images.splice(pos,1);
				$scope.load();
			},
			function (error) {
				console.error(error);
			});				
	};
	
	$scope.deletePriority=function(priority) {
		var method="DELETE";
		var urlForId=url;
		urlForId=url+'/'+$scope.id+'/priorities/'+priority._id;			 
		$http({url:urlForId, method:method}).then(
			function (response){
				// $scope.editRow.images.splice(pos,1);
				$scope.load();
			},
			function (error) {
				console.error(error);
			});				
	};
	
	$scope.deleteApplicant=function(applicant) {
		var method="DELETE";
		var urlForId=url;
		urlForId=url+'/'+$scope.id+'/clients/'+applicant._id;			 
		$http({url:urlForId, method:method}).then(
			function (response){
				// $scope.editRow.images.splice(pos,1);
				$scope.load();
			},
			function (error) {
				console.error(error);
			});				
	};
	
	$scope.deleteOperation=function(operation) {
		var method="DELETE";
		var urlForId=url;
		urlForId=url+'/'+$scope.id+'/operations/'+operation._id;			 
		$http({url:urlForId, method:method}).then(
			function (response){
				// $scope.editRow.images.splice(pos,1);
				$scope.load();
			},
			function (error) {
				console.error(error);
			});				
	};
	
	
	
	
	
	
	
	
	
	
	$scope.showPic = function(fileId) {
		$scope.imageUrl=url+'/'+$scope.id+'/images/'+fileId+'/binary';	
	};
	
	$scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
	$scope.uploadPic = function(files) {
		$scope.formUpload = true;
		if (files != null) {
			generateThumbAndUpload(files[0])
		}
	};
	
	function generateThumbAndUpload(file) {
		$scope.howToSend=1;
		$scope.errorMsg = null;
		$scope.generateThumb(file);
		if ($scope.howToSend === 1) {
			uploadUsing$upload(file);
		} else if ($scope.howToSend == 2) {
			uploadUsing$http(file);
		} else {
			uploadS3(file);
		}
	}
	$scope.picFile=null;
	$scope.generateThumb = function(file) {
		if (file != null) {
			if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
				$timeout(function() {
					var fileReader = new FileReader();
					fileReader.readAsDataURL(file);
					fileReader.onload = function(e) {
						$timeout(function() {
							file.dataUrl = e.target.result;
						});
					}
				});
			}
		}
	};
	
	function uploadUsing$upload(file) {
		file.upload = $upload.upload({
			url: '/producta/subjects/'+$scope.editRow._id+'/images',
			method: 'POST',
			headers: {
				'my-header' : 'my-header-value'
			},
			fields: {username: $scope.username},
			file: file,
			fileFormDataName: 'fileData'
		});

		file.upload.then(function(response) {
			$timeout(function() {
				file.result = response.data;
			});
			$scope.load();			
		}, function(response) {
			if (response.status > 0)
				$scope.errorMsg = response.status + ': ' + response.data;
		});

		file.upload.progress(function(evt) {
			// Math.min is to fix IE which reports 200% sometimes
			file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
		});

		file.upload.xhr(function(xhr) {
			// xhr.upload.addEventListener('abort', function(){console.log('abort complete')}, false);
		});
	}
	
	function uploadUsing$http(file) {
		var fileReader = new FileReader();
		fileReader.onload = function(e) {
			$timeout(function() {
				file.upload = $upload.http({
					url: 'https://angular-file-upload-cors-srv.appspot.com/upload' + $scope.getReqParams(),
					method: 'POST',
					headers : {
						'Content-Type': file.type
					},
					data: e.target.result
				});
			
				file.upload.then(function(response) {
					file.result = response.data;
				}, function(response) {
					if (response.status > 0)
						$scope.errorMsg = response.status + ': ' + response.data;
				});
			
				file.upload.progress(function(evt) {
					file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
				});
			}, 5000);
		};
		fileReader.readAsArrayBuffer(file);
	}	
	
	$scope.emailIt=function() {
		growl.info("Funkcija generiranje e-maila još nije implementirana!");
	};
		
	$scope.copy=function() {
		growl.warning("Funkcija pametnog kopiranja još nije implementirana!");
	};	
	
	$scope.paste=function() {
		growl.error("Funkcija pametnog lijepljenja još nije implementirana!");
	};	
	
	$scope.exportIt=function() {
		growl.info("Funkcija exporta još nije implementirana!");
	};	
	
	$scope.deleteIt=function() {
		growl.warning("Funkcija brisanje je napravljena, ali nij euključena u sučelje!");
	};		

	$scope.prevent = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();
	  };	
	  
	$scope.open = function($event, name) {
		    $event.preventDefault();
		    $event.stopPropagation();
		    $scope[name]=true;
		  };	
		  
    
		  
		  
		  
		  $scope.openModalImages= function(templateUrl, forRow) {
			   	 var modalInstance = $modal.open({
			   	      templateUrl: templateUrl,
			   	      controller: 'modalUploadImages',
			   	      resolve: {
			   	        row: function () {
			   	        		$scope.newRow={};
			   	        		return $scope.newRow;
			   	        },
			   	        subjectId: function() {return $scope.id; }
			   	        /* tu je greška 
			   	        folder: function() {
			   	        	return  $scope.path();// $scope.selectedNode;
			   	        }*/
			   	      }
			   	    });

			   	    modalInstance.result.then(function (result) {
			   	      if (result=='OK') {
			   	    	  // if ($scope.newRow) {
			   	    	  //	  $scope.editRow.images.push($scope.newRow);
			   	    	  //	  $scope.save();
			   	    	  //	  delete $scope.newRow;
			   	    	  // } else {
			   	    	  //	  $scope.save();
			   	    	  // }
			   	    	  $scope.load();
			   	      }
			   	    }, function () { // dialog dissmised
			   	    	// TODO obriši ili nađi neki drugi način
			   	    	// $scope.load();
			   	    });
			   	  };
				  
		  
		  
		  
		  
		  
	$scope.openModalOperation= function(templateUrl, forRow) {
    	 var modalInstance = $modal.open({
    	      templateUrl: templateUrl,
    	      controller: 'modalEditRowCtrl',
    	      resolve: {
    	        row: function () {
    	        	if (forRow) {
    	        		return forRow;
    	        	}
    	        	else {
    	        		$scope.newRow={};
    	        		return $scope.newRow;
    	        	}
    	        }
    	      }
    	    });

    	    modalInstance.result.then(function (result) {
    	      if (result=='OK') {
    	    	  if ($scope.newRow) {
    	    		  $scope.editRow.operations.push($scope.newRow);
    	    		  $scope.save();
    	    		  delete $scope.newRow;
    	    	  } else {
    	    		  $scope.save();
    	    	  }
    	      }
    	    }, function () { // dialog dissmised
    	    	$scope.load();
    	    });
    	  };
    	  
    	  
    	  
    	  $scope.openModalApplicant= function(templateUrl, forRow) {
    	    	 var modalInstance = $modal.open({
    	    	      templateUrl: templateUrl,
    	    	      controller: 'modalEditRowCtrl',
    	    	      resolve: {
    	    	        row: function () {
    	    	        	if (forRow) {
    	    	        		return forRow;
    	    	        	}
    	    	        	else {
    	    	        		$scope.newRow={};
    	    	        		return $scope.newRow;
    	    	        	}
    	    	        }
    	    	      }
    	    	    });

    	    	    modalInstance.result.then(function (result) {
    	    	      if (result=='OK') {
    	    	    	  if ($scope.newRow) {
    	    	    		  $scope.editRow.clients.push($scope.newRow);
    	    	    		  $scope.save();
    	    	    		  delete $scope.newRow;
    	    	    	  } else {
    	    	    		  $scope.save();
    	    	    	  }
    	    	      }
    	    	    }, function () { // dialog dissmised
    	    	    	$scope.load();
    	    	    });
    	    	  };
    	  
    	  
    	  
    	  
    	  $scope.openModalPriorities= function(templateUrl, forRow) {
    	    	 var modalInstance = $modal.open({
    	    	      templateUrl: templateUrl,
    	    	      controller: 'modalEditRowCtrl',
    	    	      resolve: {
    	    	        row: function () {
    	    	        	if (forRow) {
    	    	        		return forRow;
    	    	        	}
    	    	        	else {
    	    	        		$scope.newRow={};
    	    	        		return $scope.newRow;
    	    	        	}
    	    	        }
    	    	      }
    	    	    });

    	    	    modalInstance.result.then(function (result) {
    	    	      if (result=='OK') {
    	    	    	  if ($scope.newRow) {
    	    	    		  $scope.editRow.priorities.push($scope.newRow);
    	    	    		  $scope.save();
    	    	    		  delete $scope.newRow;
    	    	    	  } else {
    	    	    		  $scope.save();
    	    	    	  }
    	    	      }
    	    	    }, function () { // dialog dissmised
    	    	    	$scope.load();
    	    	    });
    	    	  };
    
});

