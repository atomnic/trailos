/**
 * 
 */

smipdMain.controller("taskSearchCtrl",function($route, $rootScope, $scope, $http, $window, $location, $modal, authenticationFactory, messageSrvc, dataSrvc) {
	// init, load from server
	if ($route.current.params.eventCode) {
        $scope.eventCode = $route.current.params.eventCode;
    };
    if ($route.current.params.competitionCode) {
        $scope.competitionCode = $route.current.params.competitionCode;
    };

    if ($route.current.params.runCode) {
        $scope.runCode = $route.current.params.runCode;
    };

	var storageUrl=""; // test always loads data from node.js server
	//var endPoint="/trailos/events/"+$scope.eventCode+"/competitions/"+$scope.competitionCode+"/runs/"+$scope.runCode+"/tasks/";
	var endPoint="/trailos/admin/runs/"+$scope.runCode+"/tasks";
	var url=storageUrl+endPoint;	

	$scope.$parent.setHideMenu(false);
	$scope.$parent.setHideTools(false);

	$scope.init=function() {
		if (authenticationFactory.check()) {
			$scope.load();
		} else {
			$location.path("login");
		}
	}
	
	$scope.load=function() {
		var url2=url; 
		$http.get(url2).then(
			function(response) {
				$scope.rows=response.data;
			},
			function(error) {
				if (error.status===403) {
					messageSrvc.growlIt('e198');
				} else {
					messageSrvc.growlIt('e103','tasks for stage '+$scope.runCode);
					console.log(error);
				}
			}			
		);	
	}



	$scope.openModalTaskEdit= function(forRow) {
	   	var modalInstance = $modal.open({
	   	    templateUrl: 'modalTaskEdit.html',
	   	 	controller: 'modalEditTaskCtrl',
	   	      resolve: {
	   	        row: function () {
	   	        	if (forRow) {
	   	        		$scope.editRow=forRow;
	   	        		return $scope.editRow;
	   	        	}
	   	        	else {
	   	        		$scope.editRow={};
	   	        		return $scope.editRow;
	   	        	}
	   	        }
	   	      }
	   	    });

	   	modalInstance.result.then(function (result) {
			if (result=='OK') {
				if ($scope.editRow) { // paranoic check
					$scope.save();
				} 
			}
		}, function () { // dialog dissmised
			$scope.load();
		});
	};
	
	
	$scope.getClass = function(path) {
		if ($location.path().indexOf(path) >= 0) {
	      return "active";
	    } else {
	      return "";
	    }
	}
	
	$(function() {
	    $( ".resizable tr th" ).resizable({
	      handles: 'e'
	    });
	  });
	
	
	$scope.searchOptions=
		[ 
		  {"value":"no",  "label":"Number"},
		  {"value":"type", "label":"Type"},
		  {"value":"status", "label":"Status"}
		];	

	$scope.emptyPair={"key":$scope.searchOptions[0], "value":""}; // key == value from searchOptions
	
	$scope.searchPairs =
		[];

	$scope.addPair = function() {
		var id=$scope.searchPairs.length+1;
		$scope.searchPairs.push({id:id,"key":{"value":"null",  "label":" "},"value":"" });
	};
	
	    
	$scope.deletePair = function(pairId){
		$scope.searchPairs.splice(pairId,1);
	    };
	        
	
	$(function() {
		$( ".resizable tr th" ).resizable({
			handles: 'e'
		});
	  });
	
	
	$scope.open = function($event, name) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    $scope[name]=true;
	};	
	

	$scope.save=function() {
		var method="POST";
		var urlForId=url;
		if ($scope.editRow._id) { // means old obejct
			method="PUT";
			urlForId=url+'/'+$scope.editRow._id;			
		} 
		$scope.editRow.event=$scope.eventId;
		$http({url:urlForId, method:method, data:$scope.editRow}).then(
					function (response){
						messageSrvc.growlIt("i300"); // TODO messageSrvc
						delete $scope.editRow;
						$scope.load();
					},
					function (error) {
						growl.error(error); // TODO server will respons duplicate key
					});				
	}	
	

	$scope.selected=[];
	$scope.select=function(id) {
		var index=$scope.selected.indexOf(id);
		if (index<0) {
			$scope.selected.push(id);
		} else {
			$scope.selected.splice(index,1);
		};
	}
		
	
	$scope.search=function() {
		// TODO put this in some service or factory
		var url2="";
		for (var int = 0; int < $scope.searchPairs.length; int++) {
			if ($scope.searchPairs[int].value!=="") {
				url2= url2 + "&" + $scope.searchPairs[int].key.value + "=" + $scope.searchPairs[int].value;				
			};
		}
		url2=url2.replace("&", "?");
		url2=url+url2;
		$http.get(url2).then(
				function (response){
					$scope.rows=response.data;
				},
				function (error) {
					console.error(error);
				});
	}

})