/**
 * 
 */

smipdMain.controller("testController",function($rootScope, $scope, $http, $window, $location) {
	// init, load from server
	var storageUrl=""; // test always loads data from node.js server
	var endPoint="/producta/test";
	var url=storageUrl+endPoint;	
	
	$scope.init=function() {
		$scope.load();
	}
	
	$scope.load=function() {
		$http.get(url).then(
				function (response){
					$scope.testRows=response.data;
					var i=0;
					
				},
				function (error) {
					console.error(error);
				});		
	}
	
	$scope.delete=function(id) {
		$http.delete(url+"/"+id).then(
				function (response){
					$scope.testRows=response.data;
				},
				function (error) {
					console.error(error);
				});		
	}
	
	$scope.new=function() {
		$http.put(url).then(
				function (response){
					$scope.testRows=response.data;
				},
				function (error) {
					console.error(error);
				});		
	}
})