/**
 * 
 */

var modalUploadFilesCtrl=smipdMain.controller("modalUploadFilesCtrl", function ($scope, $modalInstance, row, folder, $upload, $http) {
	
	$scope.editRow=row;
	$scope.folder=folder;	
	$scope.files;

// This thre functions are common to all modalas...	  
	$scope.open = function($event, name) {
		    $event.preventDefault();
		    $event.stopPropagation();
		    $scope[name]=true;
	};		
	
	$scope.ok = function (fileType) {
		if (fileType===0) {
			$scope.upload($scope.files);
			$modalInstance.close('OK');
		} else if (fileType===1) {
			$scope.createFolder($scope.newFolderName);
		}
	};

	  $scope.cancel = function () {
	    $modalInstance.dismiss('cancel');	    
	  };
	  
	  $scope.prepareUpload = function(files) {
		  $scope.files=files;
	  }
	  
		$scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
		
		$scope.upload = function(files) {
			$scope.formUpload = true;
			if (files != null) {
				generateThumbAndUpload(files[0])
			}
		};
		
		function generateThumbAndUpload(file) {
			$scope.howToSend=1;
			$scope.errorMsg = null;
			$scope.generateThumb(file);
			if ($scope.howToSend === 1) {
				uploadUsing$upload(file);
			} else if ($scope.howToSend == 2) {
				uploadUsing$http(file);
			} else {
				uploadS3(file);
			}
		}
		$scope.picFile=null;
		$scope.generateThumb = function(file) {
			if (file != null) {
				if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
					$timeout(function() {
						var fileReader = new FileReader();
						fileReader.readAsDataURL(file);
						fileReader.onload = function(e) {
							$timeout(function() {
								file.dataUrl = e.target.result;
							});
						}
					});
				}
			}
		};

		$scope.createFolder=function(folderName) {
			var url='/producta/folders?folder='+folder+'&'+'newFolder='+folderName;
			$http({url:url, method:'POST', data:{}}).then(
				function(response) {
					console.log('Folder created');
				},
				function(error) {
					console.error(error);
				}
			); // TODO post data in body
		}
		
		function uploadUsing$upload(file) {
			file.upload = $upload.upload({
				url: '/producta/files?folder='+$scope.folder,
				method: 'POST',
				headers: {
					'my-header' : 'my-header-value'
				},
				fields: {username: $scope.username},
				file: file,
				fileFormDataName: 'fileData'
			});

			file.upload.then(function(response) {
				$timeout(function() {
					file.result = response.data;
				});
				$scope.load();			
			}, function(response) {
				if (response.status > 0)
					$scope.errorMsg = response.status + ': ' + response.data;
			});

			file.upload.progress(function(evt) {
				// Math.min is to fix IE which reports 200% sometimes
				file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
			});

			file.upload.xhr(function(xhr) {
				// xhr.upload.addEventListener('abort', function(){console.log('abort complete')}, false);
			});
		}
		
		function uploadUsing$http(file) {
			var fileReader = new FileReader();
			fileReader.onload = function(e) {
				$timeout(function() {
					file.upload = $upload.http({
						url: 'https://angular-file-upload-cors-srv.appspot.com/upload' + $scope.getReqParams(),
						method: 'POST',
						headers : {
							'Content-Type': file.type
						},
						data: e.target.result
					});
				
					file.upload.then(function(response) {
						file.result = response.data;
					}, function(response) {
						if (response.status > 0)
							$scope.errorMsg = response.status + ': ' + response.data;
					});
				
					file.upload.progress(function(evt) {
						file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
					});
				}, 5000);
			};
			fileReader.readAsArrayBuffer(file);
		}	
		
	  
	  
});