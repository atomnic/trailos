/**
 * 
 */

smipdMain.controller("cmpCmrEditCtrl",function($route, $rootScope, $scope, $http, $window, $document, $location, $modal, authenticationFactory, messageSrvc, dataSrvc, underscore) {
	// init, load from server
	if ($route.current.params.eventCode) {
        $scope.eventCode = $route.current.params.eventCode;
    };
    if ($route.current.params.competitionCode) {
        $scope.competitionCode = $route.current.params.competitionCode;
    };

	var storageUrl=""; // test always loads data from node.js server
	var endPoint="/trailos/events/"+$scope.eventCode+"/competitions/"+$scope.competitionCode+"/competitors";
	var url=storageUrl+endPoint;	

	$scope.$parent.setHideMenu(false);
	$scope.$parent.setHideTools(false);

	$scope.paraTypes=[];
	$scope.gategorys=[];

	$scope.init=function() {
		if (authenticationFactory.check()) {
			$scope.load();
		} else {
			$location.path("login");
		}
	}

	$scope.getCompetitors=function($viewValue) {
		if ($viewValue=="") return [];
		return dataSrvc.getCollection3('competitors',{fullName:$viewValue+"*"},0,20);
	}

	$scope.getCategorys=function() {
		    dataSrvc.getCollection2('categorys',{},0,20).then(
            function(response){
                $scope.gategorys=response.data;
            }, function(error) {
                messageSrvc.growlIt("e103", 'types '+domain);            
            }
        );
	}

	$scope.getTypes=function(domain) {
		dataSrvc.getCollection2('types',{domain:domain},0,20).then(
            function(response){
                $scope.paraTypes=response.data;
            }, function(error) {
                messageSrvc.growlIt("e103", 'types '+domain);            
            }
        );
	}

	$scope.load=function() {
		var url2=url; // TODO calculate the right date, taht will come in nex 30 days and older
		$http.get(url2).then(
			function(response) {
				$scope.rows=response.data;
			},
			function(error) {
				messageSrvc.growlIt('e103','competitors for competitor '+$scope.eventCode);
			}			
		);	
	}

	$scope.loadOptions=function(){
		$http.get('/trailos/types?domain=cmpStatus').then(
			function(response) {
				$scope.cmpStatus=response.data;
			},
			function(error) {
				messageSrvc.growlIt('e103','types for domain cmpStatus');
			}			
		);	
		$scope.getCategorys();
		$scope.getTypes('paraType');
	}
	$scope.loadOptions();

	
	
	$scope.getClass = function(path) {
		if ($location.path().indexOf(path) >= 0) {
	      return "active";
	    } else {
	      return "";
	    }
	}
	
	$(function() {
	    $( ".resizable tr th" ).resizable({
	      handles: 'e'
	    });
	  });
	
	
	$scope.searchOptions=
		[ {"value":"competitor",  "label":"Competitor", "typeahead":"competitors"},
		  {"value":"category",  "label":"Category", "typeahead":"categorys"},
		  {"value":"country", "label":"Country", "typeahead":"countrys"},
		   {"value":"group", "label":"Group"},
		   {"value":"startNo", "label":"Start no."}
		];	

	$scope.emptyPair={"key":$scope.searchOptions[0], "value":""}; // key == value from searchOptions
	
	$scope.searchPairs =
		[{"key":$scope.searchOptions[1], "value":""}, 
		{"key": $scope.searchOptions[0], "value":""},
		{"key": $scope.searchOptions[4], "value":""}];

	$scope.addPair = function() {
		var id=$scope.searchPairs.length+1;
		$scope.searchPairs.push({id:id,"key":{"value":"null",  "label":" "},"value":"" });
	};
	
	    
	$scope.deletePair = function(pairId){
		$scope.searchPairs.splice(pairId,1);
	    };
	        
	
	$(function() {
		$( ".resizable tr th" ).resizable({
			handles: 'e'
		});
	  });
	
	
	$scope.open = function($event, name) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    $scope[name]=true;
	};	
	

	$scope.addCmrCmp=function() {
		var method="POST";
		var urlForId=url;
		if ($scope.editRow._id) { // means old obejct
			method="PUT";
			urlForId=url+'/'+$scope.editRow._id;			
		} 
		// $scope.editRow.competition=$scope.com;
		$http({url:urlForId, method:method, data:$scope.editRow}).then(
					function (response){
						messageSrvc.growlIt("i300"); // TODO messageSrvc
						delete $scope.editRow;
						$scope.load();
					},
					function (error) {
						messageSrvc.growlIt('e198',error.data.error);
					});				
	}	
	
	$scope.setEditRow=function(row){
		var url2=url;
		$http.get(url2+"/"+row.competitor._id).then(
			function(response) {
				$scope.editRow=response.data[0];
				if ($scope.editRow.paraType) {
					$scope.editRow.paraType=$scope.editRow.paraType._id;
				}
			},
			function(error) {
				messageSrvc.growlIt('e103','competitors for competitor ');
			}			
		);	
/*
		dataSrvc.getCollection2('cmpCmrs',{_id:row._id}).then(
			function(response) {
				$scope.editRow=response.data;
				$scope.$apply();
				//$scope.editRow.category=$scope.editRow.category._id; // depopulate
				if ($scope.editRow.paraType) {
					$scope.editRow.paraType=$scope.editRow.paraType._id;
				}
			}, function(error) {
				messageSrvc.growlIt("e103", "competition competitors")
			}
		)
*/
	};


	$scope.buildUrlParams=function(){
		var url2="";
		for (var int = 0; int < $scope.searchPairs.length; int++) {
			if ($scope.searchPairs[int].value!=="") {
				var value=$scope.searchPairs[int].value;
				if (value.code) {
					value=value.code;
				}
				else if (value._id) {
					value=value._id;
				}
				url2= url2 + "&" + $scope.searchPairs[int].key.value + "=" + value;				
			};
		}
		for (key in $scope.printQuery) {
			url2=url2 + "&" +key+"="+$scope.printQuery[key];
		}
		url2=url2.replace("&", "?");
		return url2;
	}

	$scope.openModalSolution= function(forRow) {
	   	var modalInstance = $modal.open({
	   	    templateUrl: 'modalSolutionsEdit.html',
	   	 	controller: 'modalEditSolutionCtrl',
	   	 	backdrop:"static",
	   	 	size:"lg",
	   	      resolve: {
	   	        run: function () {
	   	        	return $scope.$parent.getMarkedItem('markedRun');
	   	        },
	   	        cmpCmr: function () {
		   	        return forRow;
	   	        }
	   	    }
	   	});

	   	modalInstance.result.then(function (result) {
			if (result=='OK') {
				if ($scope.editRow) { // paranoic check
					var params=$scope.buildUrlParams();
					// FIX for faster entrys, no need to enter category in search conditions
					if (params.indexOf("category")<0) params=params+"&category="+$scope.editRow.category.code;
					if (params.indexOf("competitor")<0) params=params+"&competitor="+$scope.editRow.competitor._id;
					if (params.indexOf("&")===0) params=params.replace("&","?");
					var runCode=$scope.$parent.getMarkedItem('markedRun').code;
					$http.get("/pub/trailos/events/"+$scope.eventCode+"/competitions/"+$scope.competitionCode+"/runs/"+runCode+"/ranking"+params).then(
						function(response){
							//$scope.search();
							var fr0=underscore.find(response.data, function(r){
								return r.competitor._id===$scope.editRow.competitor._id;
							})
							
							var fr=underscore.find($scope.rows, function(r){
								return r.competitor._id===fr0.competitor._id;
							})
							fr.info="Pos: "+response.data[0].rank+" P: "+response.data[0].points+" T: "+response.data[0].time;
							$http.get("/pub/trailos/events/"+$scope.eventCode+"/competitions/"+$scope.competitionCode+"/ranking"+params).then(
								function(response){
									//$scope.search();
									var fr0=underscore.find(response.data, function(r){
										return r.competitor._id===$scope.editRow.competitor._id;
									})
									
									var fr=underscore.find($scope.rows, function(r){
										return r.competitor._id===fr0.competitor._id;
									})
									fr.info=fr.info+"Tot: "+response.data[0].points+":"+response.data[0].time;
									
								}, function (error) {

								}
							);
						}, function (error) {

						}
					);
				} 
			}
		}, function () { // dialog dissmised
			$scope.search();
		});
	};

	$scope.openModalTimes= function(forRow) {
	   	var modalInstance = $modal.open({
	   	    templateUrl: 'modalTimesEdit.html',
	   	 	controller: 'modalEditTimesCtrl',
	   	 	backdrop:"static",
	   	 	size:"lg",
	   	      resolve: {
	   	        run: function () {
	   	        	return $scope.$parent.getMarkedItem('markedRun');
	   	        },
	   	        cmpCmr: function () {
		   	        return forRow;
	   	        }
	   	    }
	   	});

	   	modalInstance.result.then(function (result) {
			if (result=='OK') {
				if ($scope.editRow) { // paranoic check
					// $scope.save(); // TODO do it in modal controler
				} 
			}
		}, function () { // dialog dissmised
			$scope.search();
		});
	};
	

	$scope.selected=[];
	$scope.select=function(id) {
		var index=$scope.selected.indexOf(id);
		if (index<0) {
			$scope.selected.push(id);
		} else {
			$scope.selected.splice(index,1);
		};
	}

	$scope.save=function(editRow) {
		var method="POST";
		var urlForId=url;
		if (editRow._id) { // means old obejct
			method="PUT";
			urlForId=url+'/'+editRow._id;			
		} 
		$http({url:urlForId, method:method, data:editRow}).then(
					function (response){
						messageSrvc.growlIt("i300"); // TODO messageSrvc
						// delete $scope.editRow;
						$scope.load();
					},
					function (error) {
						messageSrvc.growlIt("i199",error.data.error); // TODO server will respons duplicate key
					});				
	}
		
	
	$scope.search=function() {
		// TODO put this in some service or factory
		var url2="";
		for (var int = 0; int < $scope.searchPairs.length; int++) {
			if ($scope.searchPairs[int].value!=="") {
				var value=$scope.searchPairs[int].value;
				if (value._id) {
					value=value._id;
				}
				url2= url2 + "&" + $scope.searchPairs[int].key.value + "=" + value;				
			};
		}
		url2=url2.replace("&", "?");
		url2=url+url2;
		$http.get(url2).then(
				function (response){
					$scope.rows=response.data;
				},
				function (error) {
					console.error(error);
				});
	}

	$scope.keyPressed=function($event){
		var int=0;
		int++;
	}

	$document.ready(function() {
    $('#addCmpCmr').formValidation({
			framework: 'bootstrap',
	        	err: {
	            	container: 'tooltip'
	        	},
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
            fields: {
                competitor: {
                    validators: {
                        notEmpty: {
                            message: 'Cmpetitor is mandatory!'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                category: {
                    validators: {
                        notEmpty: {
                            message: 'Category is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                }            
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
            $scope.addCmrCmp();
        });
    });

})