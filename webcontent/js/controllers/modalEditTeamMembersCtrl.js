/**
 * 
 */

smipdMain.controller("modalEditTeamMembersCtrl", function ($scope, $modalInstance, $http, $document, row, underscore,  messageSrvc, dataSrvc ) {
	
    $scope.team=row;
	$scope.editRow; //=row;

    $scope.loadOptions=function(){
        dataSrvc.getStates('gender').then(
            function(response) {
                $scope.statesGender=response.data;
            },
            function(error) {
                messageSrvc.growlIt('e103','genders');
            }           
        );  
        dataSrvc.getStates('competitorTypes').then(
            function(response) {
                $scope.statesCompetitorType=response.data;
            },
            function(error) {
                messageSrvc.growlIt('e103','competitor types');
            }           
        );  
    }

    $scope.addMember=function() {
        $scope.editRow.competitor.team=$scope.team._id;
        var f=underscore.find($scope.team.members2, function(tm){
            return tm.competitor._id===$scope.editRow.competitor._id
        })
        if (!$scope.team.members2) $scope.team.members2=[];
        if (!f) $scope.team.members2.push({no:$scope.team.members2.length+1, competitor: $scope.editRow.competitor});
        $scope.editRow={};
    }

    $scope.removeMember=function(row) {
        $scope.team.members2=underscore.filter($scope.team.members2, function(tm){
            return (tm._id!==row._id)  
        });
    }

    $scope.getCountrys=function($viewValue){
        if (!$viewValue) {
            return [];
        }
        return dataSrvc.getCollection2('countrys',{nameLocal:$viewValue},0,25,{nameLocal:{wildCard:true}}).then(
            function(response) {
                return response.data;
            },
            function(error) {
                messageSrvc.growlIt('e103','countrys');
            }           
        );  
    }

    $scope.getCompetitors=function($viewValue){
        if (!$viewValue) {
            return [];
        }
        return dataSrvc.getCollection2('competitors',{fullName:$viewValue},0,25,{fullName:{wildCard:true}}).then(
            function(response) {
                return response.data;
            },
            function(error) {
                messageSrvc.growlIt('e103','countrys');
            }           
        );  
    }


    $scope.loadOptions();

	$document.ready(function() {
    $('#teamMembersEditForm').formValidation({
			framework: 'bootstrap',
	        	err: {
	            	container: 'tooltip'
	        	},
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
            fields: {
                competitor: {
                    validators: {
                        notEmpty: {
                            message: 'Competitor is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
            $scope.ok();
        });
});

        

// This thre functions are common to all modalas...	  
	$scope.open = function($event, name) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope[name]=true;
	};		

	$scope.validate = function () {
		$modalInstance.close('OK');
		return true;
	};
	
	$scope.ok = function () {
        $modalInstance.close('OK');
	};

	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};

});