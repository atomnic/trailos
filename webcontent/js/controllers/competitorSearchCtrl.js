/**
 * 
 */

smipdMain.controller("competitorSearchCtrl",function($document, $route, $rootScope, $scope, $http, $window, $location, $modal, authenticationFactory, messageSrvc, underscore, dataSrvc) {
	// init, load from server


	var storageUrl=""; // test always loads data from node.js server
	var endPoint="/trailos/competitors";
	var url=storageUrl+endPoint;	

	$scope.$parent.setHideMenu(false);
	$scope.$parent.setHideTools(false);

	$scope.paging={"offset":0, "limit":20};

	$scope.init=function() {
		if (authenticationFactory.check()) {
			$scope.load();
		} else {
			$location.path("login");
		}
	}
	
	$scope.load=function() {
		var url2=url; 
		$http.get(url2).then(
			function(response) {
				$scope.rows=response.data;
				$scope.loadOptions();
			},
			function(error) {
				messageSrvc.growlIt('e103','competitors for event '+$scope.eventCode);
			}			
		);	
	}

	$scope.loadOptions=function(){
		$http.get('/trailos/types?domain=cmpStatus').then(
			function(response) {
				$scope.cmpStatus=response.data;
			},
			function(error) {
				messageSrvc.growlIt('e103','types for domain cmpStatus');
			}			
		);	
	}

	
	$scope.search=function() {
		// TODO put this in some service or factory
		var url2="";
		for (var int = 0; int < $scope.searchPairs.length; int++) {
			if ($scope.searchPairs[int].value!=="") {
				var value=$scope.searchPairs[int].value;
				if (value._id) {
					value=value._id;
				}
				url2= url2 + "&" + $scope.searchPairs[int].key.value + "=" + value;				
			};
		}
		url2=url2+"&offset="+$scope.paging.offset+"&"+"limit="+$scope.paging.limit;
		url2=url2.replace("&", "?");
		url2=url+url2;
		$http.get(url2).then(
				function (response){
					$scope.rows=response.data;
					$scope.$parent.parseContentRange(response.headers('Content-Range'), $scope.paging);
				},
				function (error) {
					messageSrvc.growlIt("e103","competitors")
				});
	}


	// TODO only for teams, finish it
	$scope.openCompetitor=function(forRow) {
		var child={};
		child.id=forRow._id;
		child.category=forRow.name;
		child.url=forRow._id+"/runSearch";
		child.children=[];
		$scope.$parent.selectedNode.children.push(child);
		$scope.$parent.selectedNode=child;
		$location.path(child.url);
	}

	$scope.openModalCompetitor= function(forRow) {
	   	var modalInstance = $modal.open({
	   	    templateUrl: 'modalCompetitorEdit.html',
	   	 	controller: 'modalEditCompetitorCtrl',
	   	      resolve: {
	   	        row: function () {
	   	        	if (forRow) {
	   	        		$scope.editRow=forRow;
	   	        		return $scope.editRow;
	   	        	}
	   	        	else {
	   	        		$scope.editRow={};
	   	        		return $scope.editRow;
	   	        	}
	   	        }
	   	      }
	   	    });

	   	modalInstance.result.then(function (result) {
			if (result=='OK') {
				if ($scope.editRow) { // paranoic check
					$scope.save();
				} 
			}
		}, function () { // dialog dissmised
			$scope.search();
		});
	};
	
	$scope.openModalTeamMembers= function(forRow) {
	   	var modalInstance = $modal.open({
	   	    templateUrl: 'modalTeamMembersEdit.html',
	   	 	controller: 'modalEditTeamMembersCtrl',
	   	      resolve: {
	   	        row: function () {
	   	        	if (forRow) {
	   	        		$scope.editRow=forRow;
	   	        		return forRow;
	   	        	}
	   	        }
	   	      }
	   	    });

	   	modalInstance.result.then(function (result) {
			if (result=='OK') {
				if ($scope.editRow) { // paranoic check
					$scope.save();
				} 
			}
		}, function () { // dialog dissmised
			$scope.search();
		});
	};
	
	
	$scope.getClass = function(path) {
		if ($location.path().indexOf(path) >= 0) {
	      return "active";
	    } else {
	      return "";
	    }
	}
	
	// Resizable
	$(function() {
	    $( ".resizable tr th" ).resizable({
	      handles: 'e'
	    });
	});
	
	// Search options
	$scope.searchOptions=
		[ {"value":"type",  "label":"Type"},
		  {"value":"firstName",  "label":"First name"},
		  {"value":"lastName", "label":"Last name"},
		  {"value":"gender", "label":"Gender"},
		  {"value":"siChipNo", "label":"SI"},
		  {"value":"country", "label":"Country", "typeahead":"countrys"}
		];	

	$scope.emptyPair={"key":$scope.searchOptions[0], "value":""}; // key == value from searchOptions
	
	$scope.searchPairs = [{"key":$scope.searchOptions[1], "value":""},{"key":$scope.searchOptions[2], "value":""},
						 {"key":$scope.searchOptions[3], "value":""},{"key":$scope.searchOptions[4], "value":""}];

	$scope.addPair = function() {
		var id=$scope.searchPairs.length+1;
		$scope.searchPairs.push({id:id,"key":{"value":"null",  "label":" "},"value":"" });
	};
	
	    
	$scope.deletePair = function(pairId){
		$scope.searchPairs.splice(pairId,1);
	};
	
	// Open dataPicker
	$scope.openDataPicker = function($event, name) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    $scope[name]=true;
	};	
	
	$scope.teamMembers=function(forRow) {

	}

	$scope.deleteConfirm=function(forRow) {
		bootbox.confirm("Are you sure? Action: delete competitor"+ forRow.firstName+" "+forRow.lastName+"!", function(result) {
		      if (result) {
				var urlForId=url+'/'+forRow._id;
				$http.delete(urlForId).then(
					function(response) {
						for (idx in $scope.rows) {
						    if ($scope.rows[idx]._id==forRow._id) {
						    	$scope.rows.splice(parseInt(idx),1);
						    	break;
						    }
						};
					},
					function(error){
						messageSrvc.growlIt("e105");
					}
				);
		      };
		});
	}

	$scope.save=function() {
		var method="POST";
		var urlForId=url;
		if ($scope.editRow._id) { // means old obejct
			method="PUT";
			urlForId=url+'/'+$scope.editRow._id;			
		} 
		$scope.editRow.event=$scope.eventId;
		$http({url:urlForId, method:method, data:$scope.editRow}).then(
					function (response){
						messageSrvc.silentInfo("Saved ..."); // TODO messageSrvc
						delete $scope.editRow;
						$scope.search();
					},
					function (error) {
						messageSrvc.growlIt("e104"); // TODO server will respons duplicate key
					});				
	}	
	

	$scope.selected=[];
	$scope.select=function(id) {
		var index=$scope.selected.indexOf(id);
		if (index<0) {
			$scope.selected.push(id);
		} else {
			$scope.selected.splice(index,1);
		};
	}

})