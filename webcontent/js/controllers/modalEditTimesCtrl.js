/**
 * 
 */

var modalEditRowCtrl=smipdMain.controller("modalEditTimesCtrl", function ($scope, $modalInstance, $http, $document, run, cmpCmr, underscore,  messageSrvc, dataSrvc) {
	
	$scope.editRow={};
    $scope.run=run;
    $scope.cmpCmr=cmpCmr;

    var storageUrl=""; // test always loads data from node.js server
    var endPoint="/trailos/runs/"+$scope.run._id+"/competitors";
    var url=storageUrl+endPoint;

    $scope.init=function(){
        $scope.load();
    }

    $scope.load=function() {
        var url2=url; 
        $http.get(url2+"/"+$scope.cmpCmr.competitor._id).then(
            function(response) {
                if (response.data.length==1) {
                    $scope.editRow=response.data[0];
                    if ($scope.editRow.solutions.length==0) {
                        $scope.initSolutions();
                    }
                    if (!$scope.editRow.breaks || $scope.editRow.breaks.length==0) {
                        $scope.editRow.breaks=[];
                        var numOfBreaks=$scope.editRow.run.breaks;
                        if (!numOfBreaks) numOfBreaks=0;
                        for (var idx=0;idx<numOfBreaks+1; idx++) {
                            $scope.editRow.breaks.push({no:idx+1, startHrs:0,startMin:0,startSec:0,finishHrs:0,finishMin:0,finishSec:0});
                        }
                    }
                } else {
                    messageSrvc.growlIt("e103", 'competitor for stage '+$scope.editRow.run.code)
                }
            },
            function(error) {
                messageSrvc.growlIt('e103','competitor for stage '+$scope.editRow.run.code);
            }           
        );  
    }

    $scope.save=function(){
        var method="POST";
        var urlForId=url;
        if ($scope.editRow._id) { // means old obejct
            method="PUT";
            urlForId=url+'/'+$scope.editRow._id+"/times";            
        } 
        $http({url:urlForId, method:method, data:$scope.editRow}).then(
                    function (response){
                        messageSrvc.growlIt("i300"); // TODO messageSrvc
                        // $scope.load();
                    },
                    function (error) {
                       messageSrvc.growlIt(error.data.error); // TODO server will respons duplicate key
                    });             
    }

    
    
	$document.ready(function() {
    $('#timesEditForm').formValidation({
		framework: 'bootstrap',
	       	err: {
	         	container: 'tooltip'
        	},
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        live:"disabled",
            fields: {
                /*code: {
                    validators: {
                        notEmpty: {
                            message: 'Short name is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Name is mandatory'	// TODO rethink, use messageSrvc, generic reuqired field
                        }
                    }
                },
                startDate: {
                    validators: {
                        notEmpty: {
                            message: 'Start date is mandatory'
                        }
                    }
                }*/
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            // for (var idx=0; idx<) // TODO do some validation if all tasks are entered if not warning and X for not entered
            // TODO check if times from break and from statiosn are entered to
            e.preventDefault();
            $scope.ok();
        }); 
    });

        

// This thre functions are common to all modalas...	  
	$scope.open = function($event, name) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope[name]=true;
	};		


	
	$scope.ok = function () {
        $scope.save(); 
		$modalInstance.close('OK');
	};

	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};

});