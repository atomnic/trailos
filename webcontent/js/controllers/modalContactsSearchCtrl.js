/**
 * 
 */

var modalEditRowCtrl=smipdMain.controller("modalContactsSearchCtrl", function ($scope, $modalInstance, row ) {
	
	$scope.editRow=row;
	
	
	
	$scope.searchOptions=
		[ {"value":"null",  "label":" "},
		  {"value":"type",  "label":"tip klijenta"},
		  {"value":"clientName", "label":"ime klijenta"},
		  {"value":"companyName", "label":"tvrtka"},
		  {"value":"department", "label":"odjel"},
		  {"value":"person", "label":"osoba"},
		  {"value":"reference", "label":"referenca"},
		];	

	$scope.emptyPair={"key":$scope.searchOptions[0], "value":""}; // key == value from searchOptions
	
	$scope.searchPairs =
		[ {id:1, "key":$scope.searchOptions[1], "value":""},
		 {id:2,"key":$scope.searchOptions[2], "value":""},
		 {id:3,"key":$scope.searchOptions[3], "value":""},
		 {id:4,"key":$scope.searchOptions[4], "value":""}
		];

	$scope.addPair = function() {
		var id=$scope.searchPairs.length+1;
		$scope.searchPairs.push({id:id,"key":{"value":"null",  "label":" "},"value":"" });
	};
	
	    
	$scope.deletePair = function(pairId){
		$scope.searchPairs.splice(pairId,1);
	    }; 
	  
	$scope.search=function() {
			var url2="";
			for (var int = 0; int < $scope.searchPairs.length; int++) {
				if ($scope.searchPairs[int].value!=="") {
					url2= url2 + "&" + $scope.searchPairs[int].key.value + "=" + $scope.searchPairs[int].value;				
				};
			}
			url2=url2.replace("&", "?");
			url2=url+url2;
			$http.get(url2).then(
					function (response){
						$scope.clients=response.data;
					},
					function (error) {
						console.error(error);
					});
		}
	
	$(function() {
	    $( ".resizable tr th" ).resizable({
	      handles: 'e'
	    });
	  });

// This thre functions are common to all modalas...	  
	$scope.open = function($event, name) {
		    $event.preventDefault();
		    $event.stopPropagation();
		    $scope[name]=true;
		  };		
	
	$scope.ok = function () {
	  	$modalInstance.close('OK');
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});