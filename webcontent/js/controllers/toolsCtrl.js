/**
 * 
 */

smipdMain.controller("toolsCtrl",function($rootScope, $scope, $http, $window, $location, $route) {
	// init, load from server
	var storageUrl=""; // test always loads data from node.js server this should be part of some global config
	var endPoint="/producta/subjects";  // 
	var url=storageUrl+endPoint;
	

	$scope.getClass = function(path) {
		if ($location.path().indexOf(path) >= 0) {
	      return "active";
	    } else {
	      return "";
	    }
	}	
	
	$scope.treeOptions = {
		    nodeChildren: "children",
		    dirSelectable: true,
		    injectClasses: {
		        ul: "a1",
		        li: "a2",
		        liSelected: "a7",
		        iExpanded: "a3",
		        iCollapsed: "a4",
		        iLeaf: "a5",
		        label: "a6",
		        labelSelected: "a8"
		    }
		}
		
	//$scope.dataForTheTree =$scope.$parent.dataForTheTree; Inherited from main controler

	$scope.openByNode=function(node) {
		//$scope.$parent.selectedNode=$scope.selectedNode;  
		if (node.openIn==="newTab" && node.url) {
			$window.open(node.url,'_blank');	
		} else if (node.openIn==="" && node.url) {
			$location.path(node.url);
		} else if (node.broadcast && node.signal) {
			$rootScope.$broadcast(node.signal, node);
		}
	}		
				
})