/**
 * 
 */

smipdMain.controller("operationsSearch",function($rootScope, $scope, $http, $window, $location, $modal) {
	// init, load from server
	var storageUrl=""; // test always loads data from node.js server
	var endPoint="/producta/operations";
	var url=storageUrl+endPoint;	
	
	$scope.treeOptions = {
		    nodeChildren: "children",
		    dirSelectable: true,
		    injectClasses: {
		        ul: "a1",
		        li: "a2",
		        liSelected: "a7",
		        iExpanded: "a3",
		        iCollapsed: "a4",
		        iLeaf: "a5",
		        label: "a6",
		        labelSelected: "a8"
		    }
		}
		
		$scope.dataForTheTree =
		[
		    { "id":1, "category" : "PREDMETI", "url":"" , "children" : [
		                                                    {"id":1.1, "category" : "PATENTI", "children" : [] },
															{"id":1.2, "category" : "ŽIGOVI", "children" : [] },] },
		    { "id":2, "category" : "OSOBE", "url":"#/clientsSearch", "openIn":"newTab", "children" : [] },	                                                      
		    { "id":3, "category" : "PREDLOŠCI", "children" : [] },
		    { "id":4, "category" : "OSTALO", "children" : [
		                                                {"id":4.1, "category" : "INSTRUKCIJE", "url":"#/operationsSearch", "openIn":"newTab", "children" : [] },
														{"id":4.2, "category" : "ULOGE OSOBA", "children" : [] },] },
		];
				
	
	$scope.init=function() {
		$scope.load();
	}
	
	$scope.load=function() {
		$http.get(url).then(
				function (response){
					$scope.operations=response.data;			
				},
				function (error) {
					console.error(error);
				});		
	}
	
	
	$scope.remove = function(){ 
		$scope.operations.splice(this.$index, 1); 
	}
	
	
	$scope.getClass = function(path) {
		if ($location.path().indexOf(path) >= 0) {
	      return "active";
	    } else {
	      return "";
	    }
	}
	
	$(function() {
	    $( ".resizable tr th" ).resizable({
	      handles: 'e'
	    });
	  });
	
	
	$scope.searchOptions=
		[ {"value":"null",  "label":" "},
		  {"value":"name",  "label":"naziv"},
		  {"value":"fullName", "label":"puni naziv"},
		  {"value":"iso2", "label":"ISO2"},
		  {"value":"iso3", "label":"ISO3"},
		];	

	$scope.emptyPair={"key":$scope.searchOptions[0], "value":""}; // key == value from searchOptions
	
	$scope.searchPairs =
		[];

	$scope.addPair = function() {
		var id=$scope.searchPairs.length+1;
		$scope.searchPairs.push({id:id,"key":{"value":"null",  "label":" "},"value":"" });
	};
	
	    
	$scope.deletePair = function(pairId){
		$scope.searchPairs.splice(pairId,1);
	    };
	        
	
	$(function() {
	    $( "#table-css-border-1 tr th" ).resizable({
	      handles: 'e'
	    });
	  });
	
	$( "button[type=save]" ).button({icons: {primary: "ui-icon ui-icon-disk"}});
	$( "button[type=plus]" ).button({icons: {primary: "ui-icon ui-icon-plus"}}); 
	$( "button[type=search]" ).button({icons: {primary: "ui-icon ui-icon-search"}}); 
	$( "button[type=new]" ).button({icons: {primary: "ui-icon ui-icon-folder-open"}}); 
	$( "button[type=delete]" ).button({icons: {primary: "ui-icon ui-icon-trash"}});
	
	
	$scope.delete=function(id) {
		$http.delete(url+"/"+id).then(
				function (response){
					$scope.operationsRows=response.data;
				},
				function (error) {
					console.error(error);
				});		
	}
	
	$scope.new=function() {
		$http.put(url).then(
				function (response){
					$scope.operationsRows=response.data;
				},
				function (error) {
					console.error(error);
				});		
	}
	
	
	$scope.save=function() {
		var method="POST";
		var urlForId=url;
		if ($scope.id) { // means old obejct
			method="PUT";
			urlForId=url+'/'+$scope.id;			
		} 
		$http({url:urlForId, method:method, data:$scope.editRow}).then(
					function (response){
						console.info(saved);
					},
					function (error) {
						console.error(error);
					});				
	}
	
	
	
	$scope.openModalOperations= function(templateUrl, forRow) {
	   	 var modalInstance = $modal.open({
	   	      templateUrl: templateUrl,
	   	      controller: 'modalEditRowCtrl',
	   	      resolve: {
	   	        row: function () {
	   	        	if (forRow) {
	   	        		return forRow;
	   	        	}
	   	        	else {
	   	        		$scope.newRow={};
	   	        		return $scope.newRow;
	   	        	}
	   	        }
	   	      }
	   	    });

	   	    modalInstance.result.then(function (result) {
	   	      if (result=='OK') {
	   	    	  if ($scope.newRow) {
	   	    		  $scope.operations.push($scope.newRow);
	   	    		  $scope.save();
	   	    		  delete $scope.newRow;
	   	    	  } else {
	   	    		  $scope.save();
	   	    	  }
	   	      }
	   	    }, function () { // dialog dissmised
	   	    	$scope.load();
	   	    });
	   	  };
	
	
	
	
	
	
	
	$scope.selected=[];
	$scope.select=function(id) {
		var index=$scope.selected.indexOf(id);
		if (index<0) {
			$scope.selected.push(id);
		} else {
			$scope.selected.splice(index,1);
		};
	}
		
	$scope.newSubject=function() {
		$window.open('/#/operationsEdit/', '_blank');
	}
	
	// #/subjectsEdit/{{row.id}}
	$scope.openSubject=function(rowId) {
		$window.open('/#/operationsEdit/'+rowId, '_blank');
	}
	
	$scope.pathUrl = function(path) {
		if (path === "OSOBE") {
			$window.open('/#/clientsSearch/');
	    }
		if (path === "INSTRUKCIJE") {
			$window.open('/#/operationsSearch/');
	    }
	}
	
	
	$scope.search=function() {
		var url2="";
		for (var int = 0; int < $scope.searchPairs.length; int++) {
			if ($scope.searchPairs[int].value!=="") {
				url2= url2 + "&" + $scope.searchPairs[int].key.value + "=" + $scope.searchPairs[int].value;				
			};
		}
		url2=url2.replace("&", "?");
		url2=url+url2;
		$http.get(url2).then(
				function (response){
					$scope.operations=response.data;
				},
				function (error) {
					console.error(error);
				});
	}

})