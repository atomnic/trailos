
smipdMain.controller("mainCtrl",function($rootScope, $scope, $http, $window, $location, $route, dataSrvc, underscore) {
	$scope.hideMenu=false;
	$scope.hideTools=false;

	$scope.markedItems={};

	$scope.getActive = function(arg) {
		var paths=arg;
		var active="";
		if (!underscore.isArray(arg)) {
			paths=[arg];
		} 
		for (var idx=0;idx<paths.length; idx++) {
			var path=paths[idx];
			if ($location.path().indexOf(path) && $location.path().indexOf(path, $location.path().length - path.length) === -1) {
		      	// active= "";
		    } else {
				active= "active";
				break;
			}
		}
		return active;
	}

	$scope.markItem=function(key, value){
		if ($scope.isMarked(key, value)) {
			$scope.unMarkItem(key);
		} else {
			$scope.markedItems[key]=value;
		}
	}

	$scope.unMarkItem=function(key){
		delete $scope.markedItems[key]; 
	}

	$scope.getMarkedItemLabel=function(key, basedOn, suffix){
		var mi=$scope.markedItems[key];
		if (mi && mi[basedOn]) {
			return mi[basedOn]+" "+suffix;
		}
		return "";
	}

	$scope.getMarkedItem=function(key) {
		return $scope.markedItems[key];
	}

	$scope.getMarkedItem2=function(key) {
		if  (typeof $scope.markedItems[key]!=='undefined') {
			return "/"+$scope.markedItems[key];
		} else {
			return "";
		}
	}

	$scope.isMarked=function(key, value) {
		var marked= $scope.markedItems[key];
		if (marked && marked._id==value._id) {
			return true;
		} else {
			return false;
		}
	}

	$scope.getMarkedClass=function(key, value) {
		if ($scope.isMarked(key, value)) {
			return "btn-primary";
		} else {
			return "btn-default";
		}
	}

	$scope.dataForTheTree =
		[
		    { "id":1, "category" : "Main event", "url":"eventSearch" , "openIn":"", "children" : [] },
			{ "id":2, "category" : "Competitors", "url":"competitorSearch" , "openIn":"", "children" : [] },
			{ "id":3, "category" : "Administration", "url":"" , "children" : [			                                                
			                                                {"id":3.1, "category" : "Countrys","url":"countriesSearch", "openIn":"", "children" : [] },
			                                                {"id":3.2, "category" : "Clubs","url":"clubSearch", "openIn":"", "children" : [] },
			                                                {"id":3.3, "category" : "Categorys","url":"categorySearch", "openIn":"", "children" : [] },
			                                                {"id":3.4, "category" : "Types","url":"typeSearch", "openIn":"", "children" : [] },
			                                                {"id":3.5, "category" : "Users","url":"usersSearch", "openIn":"", "children" : [] },]},
            { "id":4, "category" : "Documents", "url":"" , "children" : [
   			                                                {"id":4.1, "category" : "Tempaltes", "url":"templatesSearch", "openIn":"", "children" : [], "broadcast":true, signal:"showFiles" },
   			                                                {"id":4.2, "category" : "Reports", "url":"", "openIn":"", "children" : [] },] },
   			                                                ];

	//$scope.selectedNode=$scope.dataForTheTree[0];

	$scope.treeOptions = {
			multiSelection:false,
		    nodeChildren: "children",
		    dirSelectable: true,
		    injectClasses: {
		        ul: "a1",
		        li: "a2",
		        liSelected: "a7",
		        iExpanded: "a3",
		        iCollapsed: "a4",
		        iLeaf: "a5",
		        label: "a6",
		        labelSelected: "a8"
		    }
		}
		
	//$scope.dataForTheTree =$scope.$parent.dataForTheTree; Inherited from main controler

	$scope.openByNode=function(node) {
		//$scope.$parent.selectedNode=$scope.selectedNode;  
		if (node.openIn==="newTab" && node.url) {
			$window.open(node.url,'_blank');	
		} else if (node.openIn==="" && node.url) {
			$location.path(node.url);
		} else if (node.broadcast && node.signal) {
			$rootScope.$broadcast(node.signal, node);
		}
	}	

	$scope.setHideMenu=function(value) {
		$scope.hideMenu=value;
	}

	$scope.setHideTools=function(value) {
		$scope.hideTools=value;
	}	

	$scope.clientPageInfo=function(list, paging){
		if (list) {
			var infoLimit=paging.offset+paging.limit;
			if (infoLimit>list.length) {
				infoLimit=list.length;
			}
			return (paging.offset+1)+"-"+infoLimit+"/"+list.length;
		}
		return "";
	}

	$scope.clientPage=function(list, paging){
		if (list) {
			paging.size=list.length;
			return list.slice(paging.offset, paging.offset+paging.limit);
		}
		return [];
	}

	$scope.nextPage=function(paging){
		if (!paging.limit) paging.limit=20;	// TODO put limit in parameters
		if (!paging.last) paging.last=paging.limit;
		if (paging.limit>paging.last) return;
		var to=paging.offset+paging.limit;
		if (paging.size && to>paging.size) return;
		paging.offset=to;
	}

	$scope.prevPage=function(paging){
		if (!paging.limit) paging.limit=20;	// TODO put limit in parameters
		paging.offset=paging.offset-paging.limit;
		if (paging.offset<0) paging.offset=0;
	}

	$scope.parseContentRange=function(cr, paging){
		var crs=cr.match(/([0-9]+)-([0-9]+)\/([0-9]+|\*)/);
		paging.last=parseInt(crs[2])-parseInt(crs[1]);
	}

	$scope.deleteConfirm=function(message, collection, id, clientCollection, index, callback) {
		bootbox.confirm(message, function(result) {
		    if (result) {
				if (id)	{		
					dataSrvc.deleteCollection(collection, id).then(
						function(response) {
							if (clientCollection) {
								for (var idx=0; idx < clientCollection.length; idx++) {
								    if (clientCollection[idx]._id==id) {
								    	clientCollection.splice(parseInt(idx),1);
								    	$scope.$apply();
								    	if (callback)
								    		callback();
								    	break;
								    }
								};
							}
						},
						function(error){
							messageSrvc.growlIt("e105", collection);
						}
					);
				} else {
					if (clientCollection && (index || index==0)) {
					 	clientCollection=clientCollection.splice(parseInt(index),1);
					 	$scope.$apply();
						if (callback)
				    		callback();
					}
				}
		    };
		});
	}

    $scope.getCollection=function(collection, field, $viewValue){
        if (!$viewValue) {
            return [];
        }
        var query={};
        query[field]=$viewValue+"*";
        return dataSrvc.getCollection2(collection,query,0,15).then(
            function(response) {
                return response.data;
            },
            function(error) {
                messageSrvc.growlIt('e103',collection);
            }           
        );  
    }

	$scope.getStates=function(type) {
		return dataSrvc.getStates(type).then(
			function(response) {
				return reponse.data;
			}, function(error) {
				return [];
				// TODO just log 
			}
		)
	}
})