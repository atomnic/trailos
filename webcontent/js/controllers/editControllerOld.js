/**
 * 
 */

smipdMain.controller("editController",function($rootScope, $scope, $http, $window, $location, $route) {
	// init, load from server
	var storageUrl=""; // test always loads data from node.js server this should be part of some global config
	var endPoint="/producta/test";  // 
	var url=storageUrl+endPoint;
	
	if ($route.current.params.id) {
        $scope.id = $route.current.params.id;
      };
	
	
	$scope.init=function() {
		$scope.load();
	}
	
	$scope.load=function() {
		if ($scope.id) {
			var urlForId=url+'/'+$scope.id;
			$http.get(urlForId).then(
					function (response){
						$scope.editRow=response.data;
					},
					function (error) {
						console.error(error);
					});		
	     }
	}
	
	$scope.save=function() {
		var method="POST";
		var urlForId=url;
		if ($scope.id) {
			method="PUT";
			urlForId=url+'/'+$scope.id;			
		}
		$http({url:urlForId, method:method, data:$scope.editRow}).then(
					function (response){
						console.info(saved);
					},
					function (error) {
						console.error(error);
					});				
	}
})