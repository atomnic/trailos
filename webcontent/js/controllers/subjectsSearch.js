/**
 * 
 */

smipdMain.controller("subjectsSearch",function($rootScope, $scope, $http, $window, $location, $route) {
	// init, load from server
	var storageUrl=""; // test always loads data from node.js server
	var endPoint="/producta/subjects";
	var url=storageUrl+endPoint;	

	if ($route.current.params.subjectTypeAlias) {
        $scope.subjectTypeAlias = $route.current.params.subjectTypeAlias;
    };

    $scope.subjectTypeName="Sve vrste predmeta";
	
	$scope.treeOptions = {
		    nodeChildren: "children",
		    dirSelectable: true,
		    injectClasses: {
		        ul: "a1",
		        li: "a2",
		        liSelected: "a7",
		        iExpanded: "a3",
		        iCollapsed: "a4",
		        iLeaf: "a5",
		        label: "a6",
		        labelSelected: "a8"
		    }
		}
		
	$scope.dataForTheTree =
		[
		    { "id":1, "category" : "PREDMETI", "url":"" , "children" : [
		                                                    {"id":1.1, "category" : "PATENTI","url":"subjectsSearch/P", "children" : [] },
															{"id":1.2, "category" : "ŽIGOVI","url":"subjectsSearch/T", "openIn":"", "children" : [] },
															{"id":1.3, "category" : "MODELI","url":"subjectsSearch/M", "openIn":"", "children" : [] },] },
		    { "id":2, "category" : "OSOBE", "url":"#/clientsSearch", "openIn":"newTab", "children" : [] },	                                                      
		    { "id":3, "category" : "PREDLOŠCI", "children" : [], "url":"#/templatesSearch", "openIn":"newTab" },
		    { "id":4, "category" : "OSTALO", "children" : [
		                                                {"id":4.1, "category" : "INSTRUKCIJE", "url":"#/operationsSearch", "openIn":"newTab", "children" : [] },
														{"id":4.2, "category" : "ULOGE OSOBA", "children" : [] },
														{"id":4.3, "category" : "DRŽAVE","url":"#/countriesSearch", "openIn":"newTab", "children" : [] },] },
			
		];
				
	
	$scope.init=function() {
		//$scope.load();
		$scope.search();
	}
	
	$scope.load=function() {
		$http.get(url).then(
				function (response){
					$scope.subjects=response.data;			
				},
				function (error) {
					console.error(error);
				});		
	}
	
	
	$scope.remove = function(){ 
		$scope.subjects.splice(this.$index, 1); 
	}
	
	
	$scope.getClass = function(path) {
		if ($location.path().indexOf(path) >= 0) {
	      return "active";
	    } else {
	      return "";
	    }
	}
	
	$(function() {
	    $( ".resizable tr th" ).resizable({
	      handles: 'e'
	    });
	  });
	
	
	$scope.searchOptions=
		[ {"value":"null",  "label":" "},
		  {"value":"caseNo",  "label":"broj predmeta"},
		  {"value":"caseName", "label":"naziv predmeta"},
		  {"value":"caseNameEn", "label":"naziv predmeta en"},
		  {"value":"applyNo", "label":"broj prijave"},
		  {"value":"client2", "label":"naziv protustranke"},
		];	

	$scope.emptyPair={"key":$scope.searchOptions[0], "value":""}; // key == value from searchOptions
	
	$scope.searchPairs =
		[ {id:1, "key":$scope.searchOptions[1], "value":""},
		 {id:2,"key":$scope.searchOptions[2], "value":""},
		 {id:3,"key":$scope.searchOptions[3], "value":""},
		 {id:4,"key":$scope.searchOptions[4], "value":""}
		];

	$scope.addPair = function() {
		var id=$scope.searchPairs.length+1;
		$scope.searchPairs.push({id:id,"key":{"value":"null",  "label":" "},"value":"" });
	};
	
	    
	$scope.deletePair = function(pairId){
		$scope.searchPairs.splice(pairId,1);
	    };
	        
	
	$(function() {
	    $( "#table-css-border-1 tr th" ).resizable({
	      handles: 'e'
	    });
	  });
	
	$( "button[type=save]" ).button({icons: {primary: "ui-icon ui-icon-disk"}});
	$( "button[type=plus]" ).button({icons: {primary: "ui-icon ui-icon-plus"}}); 
	$( "button[type=search]" ).button({icons: {primary: "ui-icon ui-icon-search"}}); 
	$( "button[type=new]" ).button({icons: {primary: "ui-icon ui-icon-folder-open"}}); 
	$( "button[type=delete]" ).button({icons: {primary: "ui-icon ui-icon-trash"}});
	
	
	$scope.delete=function(id) {
		$http.delete(url+"/"+id).then(
				function (response){
					$scope.subjectsRows=response.data;
				},
				function (error) {
					console.error(error);
				});		
	}
	
	$scope.new=function() {
		$http.put(url).then(
				function (response){
					$scope.subjectsRows=response.data;
				},
				function (error) {
					console.error(error);
				});		
	}
	
	$scope.selected=[];
	$scope.select=function(id) {
		var index=$scope.selected.indexOf(id);
		if (index<0) {
			$scope.selected.push(id);
		} else {
			$scope.selected.splice(index,1);
		};
	}
		
	$scope.newSubject=function() {
		if ($scope.subjectTypeAlias==='P') {
			$window.open('/#/subjectsEdit', '_blank');		
		} if ($scope.subjectTypeAlias==='T') {
			$window.open('/#/trademarksEdit', '_blank');		
		} else  if ($scope.subjectTypeAlias==='M') {
			$window.open('/#/modelsEdit', '_blank');		

		}
	}
	
	// #/subjectsEdit/{{row.id}}
	$scope.openSubject=function(rowId) {
		$window.open('/#/subjectsEdit/'+rowId, '_blank');
	}
	

	$scope.openByNode=function(node) {
		if (node.openIn==="newTab" && node.url) {
			$window.open(node.url,'_blank');	
		} else {
			$location.path(node.url);
		}
	}
	
	
	$scope.search=function() {
		var url2="";
		if ($scope.subjectTypeAlias) {
			url2="&type="+$scope.subjectTypeAlias;
		}
		for (var int = 0; int < $scope.searchPairs.length; int++) {
			if ($scope.searchPairs[int].value!=="") {
				url2= url2 + "&" + $scope.searchPairs[int].key.value + "=" + $scope.searchPairs[int].value;				
			};
		}
		url2=url2.replace("&", "?");
		url2=url+url2;
		$http.get(url2).then(
				function (response){
					$scope.subjects=response.data;
				},
				function (error) {
					console.error(error);
				});
	}

})