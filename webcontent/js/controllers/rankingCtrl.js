/**
 * 
 */

smipdMain.controller("rankingCtrl",function($route, $rootScope, $scope, $http, $window, $document, $location, $modal, authenticationFactory, messageSrvc, dataSrvc, underscore) {
	// init, load from server
	if ($route.current.params.eventCode) {
        $scope.eventCode = $route.current.params.eventCode;
    };
    if ($route.current.params.competitionCode) {
        $scope.competitionCode = $route.current.params.competitionCode;
    };
    if ($route.current.params.runCode) {
        $scope.runCode = $route.current.params.runCode;
    };

    $scope.final=false;
    if ($route.current.params._final) {
        $scope.final = true;
    };

    $scope.now=new Date();

    $scope.printQuery={}
    if ($route.current.params.category) {
		// Only for print
		$scope.printQuery.category=$route.current.params.category;
	    $scope.team=false;
    	if ($route.current.params._team) {
        	$scope.team = $route.current.params._team;
        	$scope.printQuery._team=$route.current.params._team;
    	};
	}

	if ($location.path().indexOf('rankingPrint')>0) {
		$scope.$parent.setHideMenu(true);
		$scope.$parent.setHideTools(true);
	} else {
		$scope.$parent.setHideMenu(false);
		$scope.$parent.setHideTools(false);
	}

	$scope.searchOptions=
		[ {"value":"competitor",  "label":"Competitor", "typeahead":"competitors"},
		  {"value":"category",  "label":"Category", "typeahead":"categorys", "required":true},
		  {"value":"countrys", "label":"Country"}
		];	

	$scope.emptyPair={"key":$scope.searchOptions[0], "value":""}; // key == value from searchOptions
	
	$scope.searchPairs =
		[{"key":$scope.searchOptions[1], "value":""}];

	$scope.addPair = function() {
		var id=$scope.searchPairs.length+1;
		$scope.searchPairs.push({id:id,"key":{"value":"null",  "label":" "},"value":"" });
	};

	$scope.processed=function(row) {
		var r=underscore.reduce(row.solutions, function(s,l){
			if (l.task.type==="CP" && typeof l.solution!=="undefined") {
				s=s+1;
			}
			// TODO count TCs if (row.type==="ST" && typeof l.solution!=="undefined") {
			//	s=s+1;
			//}
			return s;
		}, 0);
		return r;
	}
	
	    
	$scope.deletePair = function(pairId){
		$scope.searchPairs.splice(pairId,1);
	};


	var storageUrl=""; // test always loads data from node.js server
	var endPoint;
	if ($scope.runCode) {
		endPoint="/pub/trailos/events/"+$scope.eventCode+"/competitions/"+$scope.competitionCode+"/runs/"+$scope.runCode+"/ranking";
	} else {
		endPoint="/pub/trailos/events/"+$scope.eventCode+"/competitions/"+$scope.competitionCode+"/ranking";
	}
	var url=storageUrl+endPoint;	

	$scope.paraTypes=[];
	$scope.gategorys=[];

	$scope.buildUrlParams=function(){
		var url2="";
		for (var int = 0; int < $scope.searchPairs.length; int++) {
			if ($scope.searchPairs[int].value!=="") {
				var value=$scope.searchPairs[int].value;
				if (value.code) {
					value=value.code;
				}
				else if (value._id) {
					value=value._id;
				}
				url2= url2 + "&" + $scope.searchPairs[int].key.value + "=" + value;				
			};
		}
		for (key in $scope.printQuery) {
			url2=url2 + "&" +key+"="+$scope.printQuery[key];
		}
		url2=url2.replace("&", "?");
		return url2;
	}

	$scope.getCompetitors=function($viewValue) {
		if ($viewValue=="") return [];
		return dataSrvc.getCollection3('competitors',{fullName:$viewValue+"*"},0,20);
	}

	$scope.getCategorys=function() {
		return dataSrvc.getCollection2('categorys',{},0,20).then(
            function(response){
                $scope.gategorys=response.data;
            }, function(error) {
                messageSrvc.growlIt("e103", 'types '+domain);            
            }
        );
	}

	$scope.getTypes=function(domain) {
		dataSrvc.getCollection2('types',{domain:domain},0,20).then(
            function(response){
                $scope.paraTypes=response.data;
            }, function(error) {
                messageSrvc.growlIt("e103", 'types '+domain);            
            }
        );
	}

	$scope.init=function() {
		$scope.search();
		$scope.load();
	}

	$scope.load=function(){
		var url="/trailos/events/"+$scope.eventCode+"/competitions/"+$scope.competitionCode;
		if ($scope.runCode) {
			url=url+"/runs/"+$scope.runCode;
		}
		$http.get(url).then(
			function(response){
				$scope.run=response.data[0];
			}, function(error){
				messageSrvc.growlIt("e103", 'competition or run/stage '+$scope.runCode); 
		});
	}

	$scope.print=function() {
		var url2=$scope.buildUrlParams();
		var openUrl='/#/'+$scope.eventCode+"/"+$scope.competitionCode;
		if ($scope.runCode) {
			openUrl=openUrl+"/"+$scope.runCode;
		}
		var openUrl=openUrl+'/rankingPrint'+url2;
		$window.open(openUrl, '_blank');	// TODO dodaj sve prams i query parametre, pa u npr. rankingPrint koristi isti controler 
		// TODO rankingPrint.html je isti kao i rankings.html samo bez kerefeka tabova i gumbiju i slično i po potrebi prilagođavan za ispis 
		// nađi emajlove o teome kako treba izgledati ispis
	}

	$scope.send=function(detailed) {
		var url2=$scope.buildUrlParams();
		var openUrl=$scope.eventCode+"/"+$scope.competitionCode;
		var what="reports/";
		if ($scope.runCode) {
			openUrl=openUrl+"/"+$scope.runCode;
			if (detailed) {
				what=what+"detailed/";
			} 
		}
		openUrl=what+openUrl+url2;

		$http.get(openUrl).then(function(response){
			if (response.status===200) {
				messageSrvc.growlIt("i301");
			} else {
				messageSrvc.growlIt("e199", 'Unable to send results'); 
			}
		}, function (error){
			messageSrvc.growlIt("e199", error.data.error);
		})
	}

	$scope.loadOptions=function(){
		$http.get('/trailos/types?domain=cmpStatus').then(
			function(response) {
				$scope.cmpStatus=response.data;
			},
			function(error) {
				messageSrvc.growlIt('e103','types for domain cmpStatus');
			}			
		);	
		$scope.getCategorys();
		$scope.getTypes('paraType');
	}
	$scope.loadOptions();

	
	
	$scope.getClass = function(path) {
		if ($location.path().indexOf(path) >= 0) {
	      return "active";
	    } else {
	      return "";
	    }
	}
		
	$(function() {
		$( ".resizable tr th" ).resizable({
			handles: 'e'
		});
	  });
	
	
	$scope.search=function() {
		// TODO put this in some service or factory
		var url2=$scope.buildUrlParams();
		url2=url+url2;
		$http.get(url2).then(
				function (response){
					$scope.rows=response.data;
					for (var idx=0; idx<$scope.rows.length; idx++) {
						var r=$scope.rows[idx];
						if (r.runCmrs[0].run>r.runCmrs[1].run) {
							var tmp;
							tmp=r.runCmrs[0];
							r.runCmrs[0]=r.runCmrs[1];
							r.runCmrs[1]=tmp;
						}
					}
				},
				function (error) {
					console.error(error);
				});
	}

})