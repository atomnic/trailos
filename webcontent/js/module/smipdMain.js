/**
 * 
 */
//angular.module('myApp', ['treeControl']);
//'fileUpload', [ 'ngFileUpload' ]

var smipdMain=angular.module('smipdMain', ["ngResource", "ngRoute","treeControl", "angularFileUpload","angular-growl","ui.bootstrap","ngUnderscore","ui.select", "ngSanitize"  ]);

smipdMain.config(['growlProvider', function(growlProvider) {
	  growlProvider.globalReversedOrder(true);
	  growlProvider.globalTimeToLive(8000);
}]);

smipdMain.service('cacheService', function() {
	// TODO: write or find better approach, maybe something like cookie store
	  return {
	    set: function(key, value, expireTimeInSeconds) {
	      sessionStorage.setItem(key+'-ts', new Date().getTime());
	      sessionStorage.setItem(key+'-et', expireTimeInSeconds);
	      return sessionStorage.setItem(key, value);
	    },
	    get: function(key) {
	        var item=sessionStorage.getItem(key);
	        if(!item) {
	          return null;
	        } else {
	        	var timestamp=sessionStorage.getItem(key+'-ts');
	        	var expireTimeInMilliseconds=sessionStorage.getItem(key+'-et');
	        	if (timestamp && expireTimeInMilliseconds) {
	        		if (new Date().getTime() > (timestamp + expireTimeInMilliseconds)) {
	        			return null
	        		};
	        	}
	          return item;
	        }	     
	    },
	    remove: function(key) {
		      sessionStorage.removeItem(key);	    	
		      sessionStorage.removeItem(key+'-ts');
		      sessionStorage.removeItem(key+'-et');
		}	    
	  };
	});

smipdMain.service('securityService', function($http) {
		this.path='oauth/token';
	
		this.login = function (credentials) {
			var uri=this.path+"?grant_type=password&username="+credentials.username+"&password="+credentials.password;
			return $http.post(uri,{},{headers: {'Authorization': 'Basic MzUzYjMwMmM0NDU3NGY1NjUwNDU2ODdlNTM0ZTdkNmE6Mjg2OTI0Njk3ZTYxNWE2NzJhNjQ2YTQ5MzU0NTY0NmM='},
			});
		};	
		// TODO delete user
	});



	/**
	 * Factory for edit controllers, non parallel controllers otherwise if parallel broadcast
	*/
smipdMain.factory('editFactory',function(){
		return {
			data: {
				user: '',
			    run: ''
			},
			    updateUser: function(user) {
			      this.data.user = user;
			    },
			    updateRun: function(run) {
			    	this.data.run = run;
			  }
		  };
	});
		

	/**
	 * Route configuration is global and session data too
	 */ 
	/**
	 * Route data
	 */
smipdMain.config(function($routeProvider, $sceDelegateProvider,$locationProvider) {
		// $http.defaults.useXDomain = true;
		// $locationProvider.html5Mode(true);
		routeTable={'/search': {url:'views/subjectsSearch.html', controller: 'subjectsSearch', loginRequired:false} ,
					'/clientsSearch': {url:'views/clientsSearch.html', controller: 'clientsSearch', loginRequired:false} ,
					'/contactsSearch': {url:'views/contactsSearch.html', controller: 'clientsEdit', loginRequired:false} ,
					'/contactsSearch/:id': {url:'views/contactsSearch.html', controller: 'clientsEdit', loginRequired:false} ,
					'/clientsEdit2/': {url:'views/clientsEdit2.html', controller: 'clientsEdit', loginRequired:false} ,
					'/clientsEdit2/:id': {url:'views/clientsEdit2.html', controller: 'clientsEdit', loginRequired:false} ,
					'/subjectsSearch/:subjectTypeAlias': {url:'views/subjectsSearch.html', controller: 'subjectsSearch', loginRequired:false} ,					
					'/subjectsEdit/:id': {url:'views/subjectsEdit.html', controller: 'subjectsEdit', loginRequired:false} ,
					'/subjectsEdit/': {url:'views/subjectsEdit.html', controller: 'subjectsEdit', loginRequired:false} ,
					'/instructionsEdit/:id': {url:'views/instructionsEdit.html', controller: 'subjectsEdit', loginRequired:false} ,
					'/instructionsEdit/': {url:'views/instructionsEdit.html', controller: 'subjectsEdit', loginRequired:false} ,
					'/clientsEdit/:id': {url:'views/clientsEdit.html', controller: 'subjectsEdit', loginRequired:false} ,
					'/clientsEdit/': {url:'views/clientsEdit.html', controller: 'subjectsEdit', loginRequired:false} ,
					'/listsEdit/:id': {url:'views/listsEdit.html', controller: 'subjectsEdit', loginRequired:false} ,
					'/listsEdit/': {url:'views/listsEdit.html', controller: 'subjectsEdit', loginRequired:false} ,
			        '/' :{ url:'views/eventSearch.html', controller: 'eventSearchCtrl', loginRequired:false} ,
			        '/eventSearch' :{ url:'views/eventSearch.html', controller: 'eventSearchCtrl', loginRequired:false} ,
			        '/:eventCode/competitionSearch' :{ url:'views/competitionSearch.html', controller: 'competitionSearchCtrl', loginRequired:false} ,
			        '/competitorSearch':{ url:'views/competitorSearch.html', controller: 'competitorSearchCtrl', loginRequired:false} ,
			        '/clubSearch':{ url:'views/clubSearch.html', controller: 'clubSearchCtrl', loginRequired:false} ,
			        '/:eventCode/:competitionCode/runSearch':{ url:'views/runSearch.html', controller: 'runSearchCtrl', loginRequired:false} ,
			        '/:eventCode/:competitionCode/cmpCmrEdit':{ url:'views/cmpCmrEdit.html', controller: 'cmpCmrEditCtrl', loginRequired:false} ,

			        '/:eventCode/:competitionCode/:runCode/tasks':{ url:'views/taskSearch.html', controller: 'taskSearchCtrl', loginRequired:false} ,
			        '/:eventCode/:competitionCode/:runCode/startTimes':{ url:'views/startTimes.html', controller: 'startTimesCtrl', loginRequired:false} ,
			        '/:eventCode/:competitionCode/:runCode/ranking':{ url:'views/rankings.html', controller: 'rankingCtrl', loginRequired:false} ,
			        '/:eventCode/:competitionCode/:runCode/rankingPrint':{ url:'reports/rankingsPrint.html', controller: 'rankingCtrl', loginRequired:false} ,
					'/:eventCode/:competitionCode/:runCode/startTimesPrintStart':{ url:'reports/startTimesPrintStart.html', controller: 'startTimesCtrl', loginRequired:false} ,
					'/:eventCode/:competitionCode/:runCode/startTimesPrintFinish':{ url:'reports/startTimesPrintFinish.html', controller: 'startTimesCtrl', loginRequired:false} ,
				
					'/:eventCode/:competitionCode/:runCode/startTimesPrint':{ url:'reports/startTimesPrint.html', controller: 'startTimesCtrl', loginRequired:false} ,

					'/:eventCode/:competitionCode/ranking':{ url:'views/rankingsCmp.html', controller: 'rankingCtrl', loginRequired:false} ,
			        '/:eventCode/:competitionCode/rankingPrint':{ url:'reports/rankingCmpPrint.html', controller: 'rankingCtrl', loginRequired:false} ,


			        '/login' :{ url:'views/login.html', controller: 'loginCtrl', loginRequired:false} ,
					'/tools' :{ url:'views/tools.html', controller: 'toolsController', loginRequired:false} ,
					'/tabs' :{ url:'views/tabs.html', controller: 'tabsController', loginRequired:false},
					'/templatesSearch': {url:'views/templatesSearch.html', controller: 'templatesSearch', loginRequired:false},
					'/operationsSearch': {url:'views/operationsSearch.html', controller: 'operationsSearch', loginRequired:false},
					'/subjectOperationsSearch': {url:'views/subjectOperationsSearch.html', controller: 'subjectOperationsSearch', loginRequired:false},
					'/countriesSearch': {url:'views/countriesSearch.html', controller: 'countriesSearch', loginRequired:false},
					'/usersSearch': {url:'views/usersSearch.html', controller: 'usersSearchCtrl', loginRequired:false},
					'/trademarksClientsEdit/': {url:'views/trademarksClientsEdit.html', controller: 'subjectsEdit', loginRequired:false},
					'/trademarksInstructionsEdit/': {url:'views/trademarksInstructionsEdit.html', controller: 'subjectsEdit', loginRequired:false},
					'/trademarksListsEdit/': {url:'views/trademarksListsEdit.html', controller: 'subjectsEdit', loginRequired:false},					
					'/trademarksEdit/': {url:'views/trademarksEdit.html', controller: 'subjectsEdit', loginRequired:false} ,
					'/trademarksClientsEdit/:id': {url:'views/trademarksClientsEdit.html', controller: 'subjectsEdit', loginRequired:false},
					'/trademarksInstructionsEdit/:id': {url:'views/trademarksInstructionsEdit.html', controller: 'subjectsEdit', loginRequired:false},
					'/trademarksListsEdit/:id': {url:'views/trademarksListsEdit.html', controller: 'subjectsEdit', loginRequired:false},					
					'/trademarksEdit/:id': {url:'views/trademarksEdit.html', controller: 'subjectsEdit', loginRequired:false} ,					
					'/modelsSearch': {url:'views/modelsSearch.html', controller: 'subjectsSearch', loginRequired:false} ,
					'/modelsClientsEdit/': {url:'views/modelsClientsEdit.html', controller: 'subjectsEdit', loginRequired:false},
					'/modelsInstructionsEdit/': {url:'views/modelsInstructionsEdit.html', controller: 'subjectsEdit', loginRequired:false},
					'/modelsListsEdit/': {url:'views/modelsListsEdit.html', controller: 'subjectsEdit', loginRequired:false},
					'/modelsEdit/': {url:'views/modelsEdit.html', controller: 'subjectsEdit', loginRequired:false},
					'/modelsClientsEdit/:id': {url:'views/modelsClientsEdit.html', controller: 'subjectsEdit', loginRequired:false},
					'/modelsInstructionsEdit/:id': {url:'views/modelsInstructionsEdit.html', controller: 'subjectsEdit', loginRequired:false},
					'/modelsListsEdit/:id': {url:'views/modelsListsEdit.html', controller: 'subjectsEdit', loginRequired:false},
					'/modelsEdit/id': {url:'views/modelsEdit.html', controller: 'subjectsEdit', loginRequired:false} ,

					'/competitorsReport': {url:'reports/competitors.html', controller: 'competitorReportCtrl', loginRequired:false} 

				};					
					 
		for (var route in routeTable) {
			$routeProvider
				.when(route, {
					templateUrl : routeTable[route].url,
					controller  : routeTable[route].controller
				});
		}
		 /**
		  * Some global functions to override problems with date manipulation between JSON and HTML input[data] and ISO format
		  * TODO: check if this is still needed in angular 1.3.10
		  */
	}).config(function(){
		milisecToIsoDate=function (milisec) {
			function pad(width, padding, string) { 
				  return (width <= string.length) ? string : pad(width, padding + string, padding);
				};
			var date=new Date(milisec);
			return date.getFullYear()+'-'+pad(2,'0',(date.getMonth()+1).toString())+'-'+pad(2,'0',date.getDate().toString());
		} ;
		
		isoDateToMilisec=function (str) {
			if (str==null) return '';
			var strs=str.split('-');
			var date= new Date(parseInt(strs[0]),parseInt(strs[1])-1,parseInt(strs[2]));
			return date.getTime();
		} ;
		
	}).config(function($httpProvider){
		$httpProvider.interceptors.push('tokenInterceptor');
	});


	
		/**
		 * Event listener when routing, if user is not authenticated, don't allow routes to specific pages given in route table
		 */
	smipdMain.run(function ($rootScope, $http, cacheService) {
			  $rootScope.$on('$locationChangeStart', function (event, next, current) {
				  if (routeTable['/'+next.split("/").pop()].loginRequired && cacheService.get('access_token')==null) {
					  event.preventDefault();
					  $rootScope.$broadcast("MESSAGE","You are not logged in. Please sign up or login first!");
					  return;
				  } 
			});			  
		});
